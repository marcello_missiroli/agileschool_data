/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package delucabiagini;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author grand
 */
public class GameTest {

    Game game;

    @Before
    public void perTuttiITest() {
        game = new Game("Pippo", "Pluto");
    }
    //questa è una partita di Tennis
    @Test
    public void zeroPunti() {
        assertEquals(game.getPunteggio(), "zero, zero");
    }
    //quindi un punto ne vale 15
    @Test
    public void unPunto() {
        game.puntoPer("Pluto");
        assertEquals(game.getPunteggio(), "zero, quindici");
    }
    //due punti ne valgono 30
    @Test
    public void duePunti() {
        game.puntoPer("Pippo");
        game.puntoPer("Pippo");
        game.puntoPer("Pluto");
        assertEquals(game.getPunteggio(), "trenta, quindici");
    }
    //e tre ne valgono 40
    @Test
    public void trePunti() {
        game.puntoPer("Pippo");game.puntoPer("Pippo");game.puntoPer("Pippo");
        assertEquals(game.getPunteggio(), "quaranta, zero");
    }
    @Test 
    public void robustezza() {
        game.puntoPer("Pluto");game.puntoPer("Pluto");
        assertEquals(game.getPunteggio(), "zero, trenta");
        game.puntoPer("Pippo");game.puntoPer("Pippo");
        assertEquals(game.getPunteggio(), "trenta, trenta");
        game.puntoPer("Paperino");game.puntoPer("Paperoga");
        assertEquals(game.getPunteggio(), "trenta, trenta");
        game=new Game("Paperino","Paperoga");
        game.puntoPer("Paperino");game.puntoPer("Paperoga");
        assertEquals(game.getPunteggio(), "quindici, quindici");
    }
    @Test
    public void vantaggio() {
        game.puntoPer("Pippo");game.puntoPer("Pippo");game.puntoPer("Pippo");
        game.puntoPer("Pluto");game.puntoPer("Pluto");game.puntoPer("Pluto");game.puntoPer("Pluto");
        assertEquals(game.getPunteggio(), "vantaggio Pluto");
    }
    @Test
    public void parità() {
        game.puntoPer("Pippo");game.puntoPer("Pippo");game.puntoPer("Pippo");
        game.puntoPer("Pluto");game.puntoPer("Pluto");game.puntoPer("Pluto");
        assertEquals(game.getPunteggio(), "parità");
        game.puntoPer("Pippo");
        assertFalse(game.getPunteggio().equals("parità"));
        game.puntoPer("Pluto");
        assertEquals(game.getPunteggio(), "parità");
    }
}
