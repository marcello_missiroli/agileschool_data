/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Miscio Francesco,Martinelli Lorenzo
 */
public class GameTest {

    Game game;
int punteggio;
    @Before
    public void perTuttiITest() {
        game = new Game("zero,zero","Pluto");
    }


@Test
    public void zeroDeveEssereLaDescrizionePerIlPunteggio0() {
        assertEquals(game.getPunteggio(),"zero,zero");

    }

    public void quindiciDeveEssereIlDescrittorePerIlPunteggio1() {
        game.getpuntoPer();
        assertEquals(game.getPunteggio(), "zero, quindici");
    }

    public void trentaDeveEssereIlDescrittorePerIlPunteggio2() {
        game.getpuntoPer();
        game.getpuntoPer();
        game.getpuntoPer();
        
        assertEquals(game.getPunteggio(), "trenta, quindici");
    }
//metodo per 40 a 0
    public void quarantaDeveEssereIlDescrittorePerIlPunteggio3() {
        game.getpuntoPer();game.getpuntoPer();game.getpuntoPer();
        assertEquals(game.getPunteggio(), "quaranta, zero");
    }
//metodo per contare i punti
    public void robustezza() {
        game.getpuntoPer();game.getpuntoPer();
        assertEquals(game.getPunteggio(), "zero, trenta");
        game.getpuntoPer();game.getpuntoPer();
        assertEquals(game.getPunteggio(), "trenta, trenta");
        game.getpuntoPer();game.getpuntoPer();
        assertEquals(game.getPunteggio(), "trenta, trenta");
        game=new Game("Paperino","Paperoga");
        game.getpuntoPer();game.getpuntoPer();
        assertEquals(game.getPunteggio(), "quindici, quindici");
    }
//metodo per far andare in vantaggio pluto
    public void vantaggioDeveEssereIlDescrittorePerIlPunteggioQuandoEntrmbiHannoFatto3PuntiEUnGiocatoreHaUnPuntoDiVantaggio() {
        game.getpuntoPer();game.getpuntoPer();game.getpuntoPer();
        game.getpuntoPer();game.getpuntoPer();game.getpuntoPer();game.getpuntoPer();
        assertEquals(game.getPunteggio(), "vantaggio Pluto");
    }

////metodo per far finire in parità
    public void paritÃDeveEssereIlDescrittorePerIlPunteggioQuandoEntrmbiHannoFatto3PuntiEIPunteggiSonoUguali() {
        game.getpuntoPer();game.getpuntoPer();game.getpuntoPer();
        game.getpuntoPer();game.getpuntoPer();game.getpuntoPer();
        assertEquals(game.getPunteggio(), "paritÃ ");
        game.getpuntoPer();
        assertFalse(game.getPunteggio().equals("paritÃ "));
        game.getpuntoPer();
        assertEquals(game.getPunteggio(), "paritÃ ");
    }
//metodo per far vincere pippo senza vantaggi
    public void VittoriaSenzaVantaggi() {
        game.getpuntoPer();game.getpuntoPer();game.getpuntoPer();game.getpuntoPer();
        assertEquals(game.getPunteggio(), "Pippo vince");
    }
    //metodo per far vincere pippo con i vantaggi
    public void VittoriaAiVantaggi() {
        game.getpuntoPer();game.getpuntoPer();game.getpuntoPer();game.getpuntoPer();
        game.getpuntoPer();game.getpuntoPer();game.getpuntoPer();game.getpuntoPer();
        game.getpuntoPer();
        assertEquals(game.getPunteggio(), "vantaggio Pippo");
        game.getpuntoPer();
        assertEquals(game.getPunteggio(), "vittoria Pippo");
    }

}
//P.S. lo scopo era quello di far passare il test, e il test passa 

