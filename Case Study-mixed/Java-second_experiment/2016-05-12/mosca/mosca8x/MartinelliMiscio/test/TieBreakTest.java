/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Miscio Francesco,Martinelli Lorenzo
 */
public class TieBreakTest {
    
    TieBreak tieBreak;

    @Before
    public void perTuttiITest() {
        tieBreak = new TieBreak("0", "0");
    }
    
    @Test
    public void iPunteggiSonoNumerici() {
        assertEquals(tieBreak.getPunteggio(), "0");
        tieBreak.getpuntoPer();
        assertEquals(tieBreak.getPunteggio(), "0");
    }

    public void VinciConAlmeno7puntieDueDiVantaggio() {
        tieBreak.getpuntoPer();tieBreak.getpuntoPer();tieBreak.getpuntoPer();
        tieBreak.getpuntoPer();tieBreak.getpuntoPer();tieBreak.getpuntoPer();
        assertEquals(tieBreak.getPunteggio(), "6, 0");
        tieBreak.getpuntoPer();
        assertEquals(tieBreak.getPunteggio(), "vince Pippo");
        tieBreak.getpuntoPer();tieBreak.getpuntoPer();tieBreak.getpuntoPer();
        tieBreak.getpuntoPer();tieBreak.getpuntoPer();tieBreak.getpuntoPer();
        assertEquals(tieBreak.getPunteggio(), "7, 6");
    }


}
