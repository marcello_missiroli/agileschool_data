package com.company;

/**
 * Creato da Vlady il 12/05/16.
 * in origine parte del progetto:
 * Yakovenko_DalPesco
 */
public class Game {


    private String nomePiUno, nomePiDue;
    private String [] labelPunteggi;
    private int punti1, punti2;
    private boolean v1=false;
    private boolean v2=false;

    public Game(String nomeP1, String nomeP2) {
        this.nomePiUno =nomeP1;
        this.nomePiDue =nomeP2;

        inizializzaLabel();
    }

    private void inizializzaLabel() {
        labelPunteggi = new String[999];
        this.labelPunteggi[0]="zero";
        this.labelPunteggi[1]="quindici";
        this.labelPunteggi[2]="trenta";
        this.labelPunteggi[3]="quaranta";
        this.labelPunteggi[4]="vantaggio";

    }

    public String getPunteggio() {
        String p1,p2;

        p1 = this.getLabelPunteggi()[getPunteggioPlayer(1)];
        p2 = this.getLabelPunteggi()[getPunteggioPlayer(2)];

        return p1 + ", " + p2;
    }

    public String[] getLabelPunteggi() {
        return labelPunteggi;
    }

    private int getPunteggioPlayer(int indicePlayer) {
        int toReturn=0;
        if (indicePlayer == 1) {
            toReturn = getPunti1();
        } else if (indicePlayer == 2) {
            toReturn = getPunti2();
        }

        return toReturn;
    }
    private void setPunteggioPlayer(int indicePlayer, int valore) {
        if (indicePlayer == 1) {
            setPunti1(valore);
        } else if (indicePlayer == 2) {
            setPunti2(valore);
        }
    }

    public void setPunti1(int punti1) {
        this.punti1 = punti1;
    }

    public void setPunti2(int punti2) {
        this.punti2 = punti2;
    }

    public int getPunti1() {
        return punti1;
    }

    public int getPunti2() {
        return punti2;
    }

    public void puntoPer(String playerCheHaFattoPunto) {

        final String nomep1 = getNomePiUno();
        final String nomep2 = getNomePiDue();

        int iPiScored=0;

        if (playerCheHaFattoPunto.equals(nomep1)) {
            iPiScored = 1;
        } else if (playerCheHaFattoPunto.equals(nomep2)) {
            iPiScored = 2;
        }

        int iPNotScored = 0;

        if (playerCheHaFattoPunto.equals(getNomePiUno())) {
            iPNotScored = 2;
        } else if (playerCheHaFattoPunto.equals(getNomePiDue())) {
            iPNotScored = 1;
        }


        //todo: siamo arrivati fino a questo punto, stiamo valutando i casi di vantaggio
        if (getPunteggioPlayer(1) == 4 || getPunteggioPlayer(2) ==4) {
            //vantaggio

            //caso uno solo se uno solo ha vantaggio

            if (getPunteggioPlayer(iPiScored) == 3 && getPunteggioPlayer(iPNotScored) == 4) {
                setPunteggioPlayer(iPNotScored,getPunteggioPlayer(iPNotScored)-1);
            } else if (getPunteggioPlayer(iPiScored) == 4 && getPunteggioPlayer(iPNotScored) == 3) {
                setPunteggioPlayer(iPNotScored,getPunteggioPlayer(iPNotScored)+1);

            }

        } else {
            switch (iPiScored) {
                case 1:
                    setPunti1(getPunti1()+1);
                    break;
                case 2:
                    setPunti2(getPunti2()+1);
                    break;
            }
        }

    }

    public String getNomePiUno() {
        return nomePiUno;
    }

    public String getNomePiDue() {
        return nomePiDue;
    }
}
