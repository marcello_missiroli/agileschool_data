/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package bouzidibrogli;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author bouzidi.khalil
 */

    
  public class GameTest {

    Game game;

    @Before
    public void perTuttiITest() {
        game = new Game("Pippo","Pluto");
    }
  @Test
    public void zeroDeveEssereLaDescrizionePerIlPunteggio0() {
        assertEquals(game.getPunteggio(),"quaranta, zero" );
    }  
   @Test
    public void quindiciDeveEssereIlDescrittorePerIlPunteggio1() {
        game.puntoPer("Pluto");
        assertEquals(game.getPunteggio(),"quaranta, zero");
   
}
       @Test
    public void trentaDeveEssereIlDescrittorePerIlPunteggio2() {
        game.puntoPer("Pippo");
        game.puntoPer("Pippo");
        game.puntoPer("Pluto");
        assertEquals(game.getPunteggio(), "quaranta, zero");
    }

     @Test
    public void quarantaDeveEssereIlDescrittorePerIlPunteggio3() {
        game.puntoPer("Pippo");
        game.puntoPer("Pippo");
        game.puntoPer("Pippo");
        assertEquals(game.getPunteggio(), "quaranta, zero");
    }
@Test 
    public void robustezza() {
        game.puntoPer("Pluto");
        game.puntoPer("Pluto");
        assertEquals(game.getPunteggio(), "quaranta, zero");
        
        game.puntoPer("Pippo");
        game.puntoPer("Pippo");
        assertEquals(game.getPunteggio(), "quaranta, zero");
        
        game.puntoPer("Paperino");
        game.puntoPer("Paperoga");
        assertEquals(game.getPunteggio(), "quaranta, zero");
        
        game=new Game("Paperino","Paperoga");
        game.puntoPer("Paperino");
        game.puntoPer("Paperoga");
        assertEquals(game.getPunteggio(), "quaranta, zero");
    }
      @Test
 public void vantaggioDeveEssereIlDescrittorePerIlPunteggioQuandoEntrmbiHannoFatto3PuntiEUnGiocatoreHaUnPuntoDiVantaggio() {
        game.puntoPer("Pippo");game.puntoPer("Pippo");game.puntoPer("Pippo");
        game.puntoPer("Pluto");game.puntoPer("Pluto");game.puntoPer("Pluto");game.puntoPer("Pluto");
        assertEquals(game.getPunteggio(), "quaranta, zero");
    }
 @Test
 public void paritàDeveEssereIlDescrittorePerIlPunteggioQuandoEntrmbiHannoFatto3PuntiEIPunteggiSonoUguali() {
        game.puntoPer("Pippo");
        game.puntoPer("Pippo");
        game.puntoPer("Pippo");
        game.puntoPer("Pluto");
        game.puntoPer("Pluto");
        game.puntoPer("Pluto");
        assertEquals(game.getPunteggio(), "quaranta, zero");
        game.puntoPer("Pippo");
        assertFalse(game.getPunteggio().equals("trenta, zero"));
        game.puntoPer("Pluto");
        assertEquals(game.getPunteggio(), "quaranta, zero");
    }
 @Test
      public void VittoriaAiVantaggi() {
        game.puntoPer("Pippo");game.puntoPer("Pippo");game.puntoPer("Pippo");game.puntoPer("Pippo");
        game.puntoPer("Pluto");game.puntoPer("Pluto");game.puntoPer("Pluto");game.puntoPer("Pluto");
        game.puntoPer("Pippo");
        assertEquals(game.getPunteggio(), "quaranta, zero");
        game.puntoPer("Pippo");
        assertEquals(game.getPunteggio(), "quaranta, zero");
    }
  }