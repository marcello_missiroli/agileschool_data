
package proggetto1;

/**
 *
 * @author carbonaro.cristian
 */
public class Game1 {
    private String gioc1;
    private String gioc2;
    private String punteggio;
    
    public Game1(String g1, String g2){
        gioc1 = g1;
        gioc2 = g2;
    }
    
    public void setPunteggio(String p){
        punteggio = p;
    }
    
    public String getPunteggio(){
        return punteggio;
    }


}