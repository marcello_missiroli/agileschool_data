/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package lugaririnaldi;

/**
 *
 * @author lugari.alessandro
 */
public class Game {
    private String nome; //pippo
    private String nome1; //pluto
    private String p; //pippo_punti
    private String p1; //pluto_punti
    private int c; //pippo_puntiInt
    private int c1; //pluto_puntiInt
    
    Game(String n, String n1) {
        nome = n;
        nome1 = n1;
        p = "zero";
        p1 = "zero";
        c = 0;
        c1 = 0;
    }
    
    public String getPunteggio() {
        if(c == c1 && c > 0 && c1 > 0) {
            return "parità";
        }
        if(c >= 3 && c1 >= 3) {
            if (c > c1) {
                return "vantaggio Pippo";
            } else {
                return "vantaggio Pluto";
            }
        } else {
            return p + ", " + p1;
        }
        
    }
    
    public void puntoPer(String n) {
        if(n.equals("Pippo")) {
            c++;
        } else {
            c1++;
        }
        p = this.toString(c);
        p1 = this.toString(c1);
    }
    
    //n è punteggio in numero che viene convertito in String 
    private String toString(int n) {
        String app;
        if(n == 0) {
            app = "zero";
        } else if(n == 1) {
            app = "quindici";
        } else if (n == 2) {
            app = "trenta";
        } else {
            app = "quaranta";
        }
        return app;
    }
    public static void main(String[] args) {
        // TODO code application logic here
    }
    
}
