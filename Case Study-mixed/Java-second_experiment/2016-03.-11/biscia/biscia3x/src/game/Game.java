/*
 * 
 */

package game;

/**
 *
 * @author ravaglia.alessandro
 */
public class Game {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
    }
    private String punteggio;
    private String pippo;
    private String pluto;
    private int contPippo;
    private int contPluto;

    Game(String pippo, String pluto) {
        this.pippo = pippo;
        this.pluto = pluto;
        punteggio = "zero, zero";
        contPippo = 0;
        contPluto = 0;
    }
    
    public String getPippo() {
        return pippo;
    }
    
    public String getPluto() {
        return pluto;
    }
    
    String getPunteggio() {
        return punteggio;
    }

    void puntoPer(String nome) {
        if(nome == "Pippo") {
            contPippo +=3;
        }else if(nome == "Pluto") {
            contPluto +=3;
        }else {
            punteggio = "error";
        }
        if(contPippo == 0 && contPluto == 3) {
            punteggio = "zero, quindici";
        }else if(contPippo == 6 && contPluto == 3) {
            punteggio = "trenta, quindici";
        }else if(contPippo == 8 && contPluto == 0) {
            punteggio = "quaranta, zero";
        }
    }
    
    
    
}
