package proggetto0;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
    public class TieBreakTest {
    
    TieBreak tieBreak;

    @Before
    public void perTuttiITest() {
        tieBreak = new TieBreak("Pippo", "Pluto");
    }
    
    @Test
    public void iPunteggiSonoNumerici() {
        assertEquals(tieBreak.getPunteggio(), "0, 0");
        tieBreak.puntoPer("Pippo");
        assertEquals(tieBreak.getPunteggio(), "1, 0");
    }
    @Test
    public void VinciConAlmeno7puntieDueDiVantaggio() {
        tieBreak.puntoPer("Pippo");tieBreak.puntoPer("Pippo");tieBreak.puntoPer("Pippo");
        tieBreak.puntoPer("Pippo");tieBreak.puntoPer("Pippo");tieBreak.puntoPer("Pippo");
        assertEquals(tieBreak.getPunteggio(), "6, 0");
        tieBreak.puntoPer("Pippo");
        assertEquals(tieBreak.getPunteggio(), "vince Pippo");
        tieBreak.puntoPer("Pluto");tieBreak.puntoPer("Pluto");tieBreak.puntoPer("Pluto");
        tieBreak.puntoPer("Pluto");tieBreak.puntoPer("Pluto");tieBreak.puntoPer("Pluto");
        assertEquals(tieBreak.getPunteggio(), "7, 6");
    }

    
}