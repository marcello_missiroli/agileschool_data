package proggetto0;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class GameTest {

    Game game;

    @Before
    public void perTuttiITest() {
        game = new Game("Pippo", "Pluto");
    }   
    @Test
    public void zeroDeveEssereLaDescrizionePerIlPunteggio0() {
        assertEquals(game.getPunteggio(), "zero, zero");
    }
    @Test
    public void quindiciDeveEssereIlDescrittorePerIlPunteggio1() {
        game.puntoPer("Pluto");
        assertEquals(game.getPunteggio(), "zero, quindici");
    }
    @Test
    public void trentaDeveEssereIlDescrittorePerIlPunteggio2() {
        game.puntoPer("Pippo");
        game.puntoPer("Pippo");
        game.puntoPer("Pluto");
        assertEquals(game.getPunteggio(), "trenta, quindici");
    }
    @Test
    public void quarantaDeveEssereIlDescrittorePerIlPunteggio3() {
        game.puntoPer("Pippo");
        game.puntoPer("Pippo");
        game.puntoPer("Pippo");
        assertEquals(game.getPunteggio(), "quaranta, zero");
    }
    @Test 
    public void robustezza() {
        game.puntoPer("Pluto");
        game.puntoPer("Pluto");
        assertEquals(game.getPunteggio(), "zero, trenta");
        game.puntoPer("Pippo");
        game.puntoPer("Pippo");
        assertEquals(game.getPunteggio(), "trenta, trenta");
        game.puntoPer("Paperino");
        game.puntoPer("Paperoga");
        assertEquals(game.getPunteggio(), "trenta, trenta");
        game = new Game("Paperino","Paperoga");
        game.puntoPer("Paperino");
        game.puntoPer("Paperoga");
        assertEquals(game.getPunteggio(), "quindici, quindici");
    }
    @Test
    public void vantaggioDeveEssereIlDescrittorePerIlPunteggioQuandoEntrmbiHannoFatto3PuntiEUnGiocatoreHaUnPuntoDiVantaggio() {
        game.puntoPer("Pippo");
        game.puntoPer("Pippo");
        game.puntoPer("Pippo");
        game.puntoPer("Pluto");
        game.puntoPer("Pluto");
        game.puntoPer("Pluto");
        game.puntoPer("Pluto");
        assertEquals(game.getPunteggio(), "vantaggio Pluto");
    }
    @Test
    public void paritaDeveEssereIlDescrittorePerIlPunteggioQuandoEntrmbiHannoFatto3PuntiEIPunteggiSonoUguali() {
        game.puntoPer("Pippo");game.puntoPer("Pippo");game.puntoPer("Pippo");
        game.puntoPer("Pluto");game.puntoPer("Pluto");game.puntoPer("Pluto");
        assertEquals(game.getPunteggio(), "parita");
        game.puntoPer("Pippo");
        assertFalse(game.getPunteggio().equals("parita"));
        game.puntoPer("Pluto");
        assertEquals(game.getPunteggio(), "parita");
    }
    @Test
    public void VittoriaSenzaVantaggi() {
        game.puntoPer("Pippo");
        game.puntoPer("Pippo");
        game.puntoPer("Pippo");
        game.puntoPer("Pippo");
        assertEquals(game.getPunteggio(), "Pippo vince");
    }
    @Test
    public void VittoriaAiVantaggi() {
        game.puntoPer("Pippo");
        game.puntoPer("Pluto");
        game.puntoPer("Pippo");
        game.puntoPer("Pluto");
        game.puntoPer("Pippo");
        game.puntoPer("Pluto");
        game.puntoPer("Pippo");
        game.puntoPer("Pluto");
        
        game.puntoPer("Pippo");
        assertEquals(game.getPunteggio(), "vantaggio Pippo");
        game.puntoPer("Pippo");
        assertEquals(game.getPunteggio(), "Pippo vince");
    }
}
