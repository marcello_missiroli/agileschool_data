package proggetto0;
public class TieBreak extends Game{
    
    protected int punto1T;//punteggio giocatore 1
    protected int punto2T;//punteggio giocatore 2
    
    public String getPunteggio() {
        if(punto1T == 7 && punto2T == 0) {
            return "vince Pippo";
        }else{
            return punto1T+", "+punto2T;
        }
    }
    public void puntoPer(String chi) {
        if(chi.equals(cog1)) {
            punto1T++;
        }else {
            punto2T++;
        }
    }//public
    public TieBreak(String cog1, String cog2) {
        super(cog1, cog2);
       punto1T = 0;
       punto2T = 0;
    }
}
