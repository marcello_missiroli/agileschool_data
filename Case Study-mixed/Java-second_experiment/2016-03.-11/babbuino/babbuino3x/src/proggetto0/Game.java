package proggetto0;

/**
 * @author lambertini.alessandr
 */

public class Game {
    protected String cog1;//cognome giocatore 1
    protected String cog2;//cognome giocatore 2
    protected String punto1;//punteggio giocatore 1
    protected String punto2;//punteggio giocatore 2
    protected String vit;//variabile che segnala la vittoria di un giocatore
    
    public Game(String pippo, String pluto) {
        cog1 = pippo;
        cog2 = pluto;
        punto1 = "zero";
        punto2 = "zero";
        vit = null;
    }
    public String getPunteggio() {
        if(vit != null)
            return vit;
        else if(punto1.equals(punto2) && (punto1 == "quaranta"))
            return "parita";
        else if(punto1 ==  "vantaggio Pippo")
            return punto1;
        else if(punto2 == "vantaggio Pluto")
            return punto2;
        return punto1 + ", " + punto2;
    }
    public void puntoPer(String chi) {
        if(chi.equals(cog1))
            switch(punto1) {
                case "zero": {
                    punto1 = "quindici";
                    break;
                }
                case "quindici": {
                    punto1 = "trenta";
                    break;
                }
                case "trenta": {
                    punto1 = "quaranta";
                    break;
                }
                case "quaranta": {
                    if(punto2 == "quaranta")
                        punto1 = "vantaggio Pippo";
                    else if(punto2 == "vantaggio Pluto") {
                        punto1 = "quaranta";
                        punto2 = "quaranta";
                    }else
                        vit = "Pippo vince";
                    break;
                }
                case "vantaggio Pippo": {
                    vit = "Pippo vince";
                    break;
                }
            }//switc
        else if(chi.equals(cog2)){
            switch(punto2) {
                case "zero": {
                    punto2 = "quindici";
                    break;
                }
                case "quindici": {
                    punto2 = "trenta";
                    break;
                }
                case "trenta": {
                    punto2 = "quaranta";
                    break;
                }
                case "quaranta": {
                    if(punto1 == "quaranta")
                        punto2 = "vantaggio Pluto";
                    else if(punto1 == "vantaggio Pippo") {
                        punto2 = "quaranta";
                        punto1 = "quaranta";
                    }else
                        vit = "Pluto vince";
                    break;
                }
                case "vantaggio Pluto": {
                    vit = "Pluto vince";
                    break;
                }
            }//switc
        }//else
    }//public
}//class
