/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package monarimautone;

/**
 *
 * @author g.mautone
 */
public class Game {

     private String uno;
     private String due;
     private String punteggio;
     
     
    public Game(String uno, String due, String punteggio) {
        this.uno = uno;
        this.due = due;
        this.punteggio= punteggio;
    }

    public String getPunteggio() {
        return punteggio;
    }
 
    public String puntoPer(String nome)
    {
        int c=0, c2=0;
        if (nome==uno)
        {
           c=15;
        }
        if (nome==due)
        {
            c2=15;
            punteggio="zero, quindici";
        }
        if (nome==uno && c==15)
        {
           c=30;
        }
        if (nome==due && c2==15)
        {
            c2=30;
            punteggio="trenta, quindici";
        }
        if (nome==uno && c==30)
        {
           c=40;
        }
        if (nome==due && c2==30)
        {
           c2=40;
        }
        return nome;
    }
    
}
