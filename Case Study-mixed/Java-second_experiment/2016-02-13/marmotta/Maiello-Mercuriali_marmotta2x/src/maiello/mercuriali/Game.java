/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package maiello.mercuriali;

import java.util.Objects;

/**
 *
 * @author p.maiello
 */
public class Game {
    private String pippo,pluto,punteggio;
    
    Game(String pippo, String pluto) {
        this.pippo=pippo;
        this.pluto=pluto;
        this.punteggio="zero, zero";
    }

    public String getPunteggio() {
        return punteggio;
    }

    public void setPunteggio(String punteggio) {
        this.punteggio = punteggio;
    }
    
    

    public void puntoPer(String Punto) {
        if (Punto=="Pluto")
        {
            switch (Punto)
                    {
                    case "zero, zero":
                        punteggio="zero, quindici";
                        break;
                    case "zero, quindici":
                        punteggio="zero, trenta";
                        break;
                    case "zero, trenta":
                        punteggio="zero, quaranta";
                        break;
                    case "zero, quaranta":
                        punteggio="zero, quaranta";
                        break;
                    case "quindici, zero":
                        punteggio="quindici, quindici";
                        break;
                    case "quindici, quindici":
                        punteggio="quindici, trenta";
                        break;
                    case "quindici, trenta":
                        punteggio="quindici, quaranta";
                        break;
                    case "quindici, quaranta":
                        punteggio="quindici, quaranta";
                        break;
                    case "trenta, zero":
                        punteggio="trenta, quindici";
                        break;
                    case "trenta, quindici":
                        punteggio="trenta, trenta";
                        break;
                    case "trenta, trenta":
                        punteggio="trenta, quaranta";
                        break;
                    case "trenta, quaranta":
                        punteggio="trenta, quaranta";
                        break;
                    case "quaranta, zero":
                        punteggio="quaranta, quindici";
                        break;
                    case "quaranta, quindici":
                        punteggio="quaranta, trenta";
                        break;
                    case "quaranta, trenta":
                        punteggio="quaranta, quaranta";
                        break;
                    case "quaranta, quaranta":
                        punteggio="quaranta, quaranta";
                        break;
            }               
        }
        else
        {
            switch (Punto)
                    {
                    case "zero, zero":
                        punteggio="quindici, zero";
                        break;
                    case "quindici, zero":
                        punteggio="trenta, zero";
                        break;
                    case "trenta, zero":
                        punteggio="quaranta, zero";
                        break;
                    case "quaranta, zero":
                        punteggio="quaranta, zero";
                        break;
                    case "zero, quindici":
                        punteggio="quindici, quindici";
                        break;
                    case "quindici, quindici":
                        punteggio="trenta, quindici";
                        break;
                    case "trenta, quindici":
                        punteggio="quaranta, quindici";
                        break;
                    case "quaranta, quindici":
                        punteggio="quaranta, quindici";
                        break;
                    case "zero, trenta":
                        punteggio="quindici, trenta";
                        break;
                    case "quindici, trenta":
                        punteggio="trenta, trenta";
                        break;
                    case "trenta, trenta":
                        punteggio="quaranta, trenta";
                        break;
                    case "quaranta, trenta":
                        punteggio="quaranta, trenta";
                        break;
                    case "zero, quaranta":
                        punteggio="quindici, quaranta";
                        break;
                    case "quindici, quaranta":
                        punteggio="trenta, quaranta";
                        break;
                    case "trenta, quaranta":
                        punteggio="quaranta, quaranta";
                        break;
                    case "quaranta, quaranta":
                        punteggio="quaranta, quaranta";
                        break;
            } 
        }
        
    }

    
    
}
