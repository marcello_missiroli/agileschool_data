/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package burinibarbani;

/**
 *
 * @author a.barbani
 */
public class Game {
    private String giocatore1;//giocatore 1
    private String giocatore2;//giocatore 2
    private String punteggiogiocatore1;//punteggio giocatore 1
    private String punteggiogiocatore2;//punteggio giocatore 2
    
    
    public Game(String n, String c)//inizializza le variabili
    {
        giocatore1=new String(n);
        giocatore2=new String(c);
        punteggiogiocatore1="zero";
        punteggiogiocatore2="zero";
    }
    
    public String getPunteggio() {
        if(punteggiogiocatore1.equals("vantaggio "+giocatore1))
            return punteggiogiocatore1;
        if(punteggiogiocatore2.equals("vantaggio "+giocatore2))
            return punteggiogiocatore2;
        if(punteggiogiocatore1.equals("parita ")&&punteggiogiocatore2.equals("parita "))
            return "parita";
        if(punteggiogiocatore2.equals("quaranta")&&punteggiogiocatore1.equals("quaranta"))
            return "parita ";
        if(punteggiogiocatore1.equals(giocatore1+" vince"))
            return punteggiogiocatore1;
        if(punteggiogiocatore2.equals(giocatore2+" vince"))
            return punteggiogiocatore2;
        return punteggiogiocatore1+", "+punteggiogiocatore2;
    }


    public void puntoPer(String temp)//per assegnare i punti
    {
    if(temp.equals(giocatore1))
    {
        if(punteggiogiocatore1.equals("zero"))
        {
            punteggiogiocatore1=new String("quindici");
        }
        else if(punteggiogiocatore1.equals("quindici"))
        {
            punteggiogiocatore1=new String("trenta");
        }
        else if(punteggiogiocatore1.equals("trenta"))
        {
            punteggiogiocatore1=new String("quaranta");
        }
        else if(punteggiogiocatore2.equals("quaranta")&&punteggiogiocatore1.equals("quaranta"))
        {
            punteggiogiocatore1=new String("vantaggio "+giocatore1);
        }
        else if(punteggiogiocatore1.equals("quaranta")&&punteggiogiocatore2.equals("vantaggio "+giocatore2))
        {
            punteggiogiocatore1=new String("parita ");
            punteggiogiocatore2=new String("parita ");
        }
        else if(punteggiogiocatore1.equals("quaranta"))
        {
            punteggiogiocatore1=new String(giocatore1+" vince");
        }
        else if(punteggiogiocatore1.equals("parita ")&&punteggiogiocatore2.equals("parita "))
        {
            punteggiogiocatore1=new String("vantaggio "+giocatore1);
        }
        else if(punteggiogiocatore1.equals("vantaggio "+giocatore1))
        {
            punteggiogiocatore1=new String(giocatore1+" vince");
        }
    }
    if(temp.equals(giocatore2))
    {
        if(punteggiogiocatore2.equals("zero"))
        {
            punteggiogiocatore2=new String("quindici");
        }
        else if(punteggiogiocatore2.equals("quindici"))
        {
            punteggiogiocatore2=new String("trenta");
        }
        else if(punteggiogiocatore2.equals("trenta"))
        {
            punteggiogiocatore2=new String("quaranta");
        }
        else if(punteggiogiocatore2.equals("quaranta")&&punteggiogiocatore1.equals("quaranta"))
        {
            punteggiogiocatore2=new String("vantaggio "+giocatore2);
        }
        else if(punteggiogiocatore2.equals("quaranta")&&punteggiogiocatore1.equals("vantaggio "+giocatore1))
        {
            punteggiogiocatore1=new String("parita ");
            punteggiogiocatore2=new String("parita ");
        }
        else if(punteggiogiocatore2.equals("quaranta"))
        {
            punteggiogiocatore2=new String(giocatore2+" vince");
        }
        else if(punteggiogiocatore1.equals("parita ")&&punteggiogiocatore2.equals("parita "))
        {
            punteggiogiocatore2=new String("vantaggio "+giocatore2);
        }
        else if(punteggiogiocatore2.equals("vantaggio "+giocatore2))
        {
            punteggiogiocatore2=new String(giocatore2+" vince");
        }
    }
    
    }
}
