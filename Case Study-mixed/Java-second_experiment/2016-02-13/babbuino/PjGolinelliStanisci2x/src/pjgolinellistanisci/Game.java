/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pjgolinellistanisci;

/**
 *
 * @author r.golinelli
 */
public class Game {
    private String nome1;
    private String nome2;
    private String punteggio;
    private int punti1;
    private int punti2;
    private int conta1;
    private int conta2;
    public Game (String s1,String s2){
        nome1=s1;
        nome2=s2;
        punteggio="zero, zero";
        punti1=0;
        punti2=0;
    }

    public String getPunteggio() {
        return this.punteggio;
    }
    public void puntoPer(String giocatore){
        if(giocatore==nome1)
        {
            if(punti1!=30)
                punti1+=15;
            else
                punti1+=10;
        }
        else if(giocatore==nome2)
        {
            if(punti2!=30)
                punti2+=15;
            else
                punti2+=10;
        }
        setPunteggio();
    }
    public void setPunteggio(){
    punteggio="";
        switch(punti1){
            case 0:
                punteggio+="zero, ";
                break;
            case 15:
                punteggio+="quindici, ";
                break;
            case 30:
                punteggio+="trenta, ";
                break;
            case 40:
                punteggio+="quaranta, ";
                break;
            default:
        }
        switch(punti2){
            case 0:
                punteggio+="zero";
                break;
            case 15:
                punteggio+="quindici";
                break;
            case 30:
                punteggio+="trenta";
                break;
            case 40:
                punteggio+="quaranta";
                break;
            default:
        }
        if(punti1>=40&&punti2>=40)
        {
            if(punti1==punti2)
                punteggio="parita ";
            else if(punti1>=punti2)
                 punteggio="vantaggio "+nome1;
            else
                 punteggio="vantaggio "+nome2;
        }
        
        
    }
    /*
    public boolean Equals(String s1, String s2)
    {
        if(s1==s2)
        {
            return true;
        }
        else
            return false;
    }*/
    
}
