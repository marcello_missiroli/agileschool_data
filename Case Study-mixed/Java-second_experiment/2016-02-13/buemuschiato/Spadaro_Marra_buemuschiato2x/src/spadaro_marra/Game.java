/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spadaro_marra;

/**
 *
 * @author a.marra
 */
public class Game {
    private String sGiocatore1, sGiocatore2, sPunto;
    private int contaPluto=0, contaPippo=0;
    public Game(String giocatore1, String giocatore2)
    {
       sGiocatore1 = giocatore1;
       sGiocatore2 = giocatore2;
    }
    
    public String getPunteggio()     
    {
        String punteggio1 = "";
        String punteggio2 = "";
        
        switch(contaPippo)          // switch che attribuisce valori string in base ai punti fatti da pippo
        {
          case 0:
              punteggio1 = "zero";
              break;
          case 1:
              punteggio1 = "quindici";
              break;
          case 2:
              punteggio1 = "trenta";
              break;
          case 3:
              punteggio1 = "quaranta";
              break;
        }
        switch(contaPluto)      // switch che attribuisce valori string in base ai punti fatti da pluto
        {
          case 0:
              punteggio2 = "zero";
              break;
          case 1:
              punteggio2 = "quindici";
              break;
          case 2:
              punteggio2 = "trenta";
              break;
          case 3:
              punteggio2 = "quaranta";
              break;
        }
            
        if(contaPippo == contaPluto)
           return "parità";   
        
       if(contaPluto==3&&contaPippo==4)
           return "vantaggio Pippo";
       else if(contaPippo==3&&contaPluto==4)
           return "vantaggio Pluto";
           
   
           
      return ""+punteggio1+", "+punteggio2;          //ritorna il punteggio di entrambi in string
    }
    
    public void puntoPer(String pp)         // conta quanti punti sono stati fatti da ognuno in cifra unitaria
    {
        if(pp==sGiocatore1)            
            contaPippo++;
        else if(pp==sGiocatore2)
            contaPluto++;
        sPunto=pp;
        
    }
    
}
