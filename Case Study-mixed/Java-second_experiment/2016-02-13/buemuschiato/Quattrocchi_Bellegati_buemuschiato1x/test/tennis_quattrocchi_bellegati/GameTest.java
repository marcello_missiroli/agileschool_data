/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tennis_quattrocchi_bellegati;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author a.quattrocchi
 */
public class GameTest {
    
        Game game;
        @Before
        public void perTuttiITest()
        {
             game = new Game("Pippo", "Pluto");       
        }
        
        @Test
        public void zeroDeveEssereLaDescrizionePerIlPunteggio0() {
            assertEquals("zero, zero",game.getPunteggio());
        }
        
        @Test
        public void quindiciDeveEssereIlDescrittorePerIlPunteggio1() {
            game.puntoPer("Pluto");
            assertEquals("zero, quindici", game.getPunteggio());
        }
        @Test
        public void trentaDeveEssereIlDescrittorePerIlPunteggio2() {
            game.puntoPer("Pippo");
            game.puntoPer("Pippo");
            game.puntoPer("Pluto");
            assertEquals("trenta, quindici",game.getPunteggio());
        }
        
    @Test
    public void quarantaDeveEssereIlDescrittorePerIlPunteggio3() {
        game.puntoPer("Pippo");
        game.puntoPer("Pippo");
        game.puntoPer("Pippo");
        assertEquals("quaranta, zero",game.getPunteggio());
    }
    @Test 
    public void robustezza() {
        game.puntoPer("Pluto");
        game.puntoPer("Pluto");
        assertEquals("zero, trenta",game.getPunteggio());
        game.puntoPer("Pippo");
        game.puntoPer("Pippo");
        assertEquals("trenta, trenta",game.getPunteggio());
        game.puntoPer("Paperino");
        game.puntoPer("Paperoga");
        assertEquals(game.getPunteggio(), "trenta, trenta");
        game=new Game("Paperino","Paperoga");
        game.puntoPer("Paperino");
        game.puntoPer("Paperoga");
        assertEquals(game.getPunteggio(), "quindici, quindici");
    }
    
    @Test
    public void vantaggioDeveEssereIlDescrittorePerIlPunteggioQuandoEntrmbiHannoFatto3PuntiEUnGiocatoreHaUnPuntoDiVantaggio() {
        game.puntoPer("Pippo");
        game.puntoPer("Pippo");
        game.puntoPer("Pippo");
        game.puntoPer("Pluto");
        game.puntoPer("Pluto");
        game.puntoPer("Pluto");
        game.puntoPer("Pluto");
        assertEquals("vantaggio Pluto",game.getPunteggio());
    }
    @Test
    public void paritaDeveEssereIlDescrittorePerIlPunteggioQuandoEntrmbiHannoFatto3PuntiEIPunteggiSonoUguali() {
        game.puntoPer("Pippo");
        game.puntoPer("Pippo");
        game.puntoPer("Pippo");
        game.puntoPer("Pluto");
        game.puntoPer("Pluto");
        game.puntoPer("Pluto");
        assertEquals(game.getPunteggio(), "parita ");
        game.puntoPer("Pippo");
        assertFalse(game.getPunteggio().equals("parita "));
        game.puntoPer("Pluto");
        assertEquals("parita ", game.getPunteggio());
    }


}
