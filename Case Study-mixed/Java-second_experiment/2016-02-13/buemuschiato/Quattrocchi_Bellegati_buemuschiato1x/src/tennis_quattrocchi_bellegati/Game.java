/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tennis_quattrocchi_bellegati;

/**
 *
 * @author a.quattrocchi
 */
public class Game {
    protected String nomePippo;
    protected String nomePluto;
    protected String nomePaperino;
    protected String nomePaperoga;
    protected int punteggio1;
    protected int punteggio2;
    protected int punteggio3;
    protected int punteggio4;
    protected String punteggi;
    
    public Game(String n1, String n2)
    {
        this.nomePippo = n1;
        this.nomePluto = n2;
        this.punteggio1 = 0;
        this.punteggio2 = 0;
        this.punteggio3 = 0;
        this.punteggio4 = 0;
        this.punteggi = "zero, zero";
    }
    public String getPunteggio()
    {
        String punteggi1="zero"; 
        String punteggi2="zero";
        //switch per definire i punteggi
        switch(punteggio1)
        {
            case 0:     
                punteggi1="zero";
                break;
            case 1:
                punteggi1="quindici";
                break;
            case 2:
                punteggi1="trenta";
                break;
            case 3:
                punteggi1="quaranta";
                break;
            case 4:
                punteggi1="vantaggio Pippo";
                break;
                
        }
        switch(punteggio2)
        {
            case 0:
                punteggi2="zero";
                break;
            case 1:
                punteggi2="quindici";
                break;
            case 2:
                punteggi2="trenta";
                break;
            case 3:
                punteggi2="quaranta";
                break;
            case 4:
                punteggi1="vantaggio Pluto";
                break;
        }
       
        //rinizializzazione
        if(punteggio1==4)
            return "vantaggio Pippo";
        else if(punteggio2==4)
            return "vantaggio Pluto";
        if((punteggio1==punteggio2 && punteggio1 == 3 && punteggio2 == 3) || (punteggio1==punteggio2 && punteggio1 == 4 && punteggio2 == 4))
            return "parita ";
        return punteggi1+", "+punteggi2;
    }
    public void puntoPer(String nome){
        //incrementiamo a seconda di chi ha fatto punto
        if(nome.equals(nomePippo)) //Pippo
        {
            punteggio1++;
        }
        else if(nome.equals(nomePluto)) //Pluto
        {
            punteggio2++;
        }
        else if(nome.equals(nomePaperino)) //Paperino
        {
            punteggio3++;
        }
        else if(nome.equals(nomePaperoga)) //Paperoga
        {
            punteggio4++;
        }
    }
    
}
