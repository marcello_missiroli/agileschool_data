/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package scialfacamara;

/**
 *
 * @author i.scialfa
 */
public class Game {
    private String giocatore1;
    private String giocatore2;
   
    private int puntiG1, puntiG2;

    public Game(String a, String b){
        this.giocatore1=a;
        this.giocatore2=b;
        puntiG1=0;
        puntiG2=0;
    }
    
    public String getPunteggio()
    {
        if (puntiG1<40 && puntiG2 <40){
              return TrasformaPunteggio();
        }
        else
        {  
        if (puntiG1>puntiG2)
        {
            return  "vantaggio "+giocatore1;
        }
          if (puntiG1<puntiG2)
        {
            return  "vantaggio "+giocatore2;
        }
          return "";
        }
    }
    public  void puntoPer(String nome )
    {
        if(nome.equals(giocatore1) == true)
        {
            puntiG1++;     
        
        }
        else 
         if(nome.equals(giocatore2) == true)
        {
            puntiG2++;
           
        }
    }
    private String TrasformaPunteggio() //trasformo in stringa in puntegio
    {
       
       String a=" ";
       switch(puntiG1)
       {
           case 2:
                a=new String("trenta, ");
              break;
           case 1:
               a=new String("quindici, ");
              break;
           case 0:
               a=new String("zero, ");
              break;
              case 3:
              a=new String("quaranta, ");
              break;     
       }
       switch(puntiG2)
       {
           case 2:
                a=new String(a+"trenta");
              break;
           case 1:
               a=new String(a+"quindici");
              break;
           case 0:
               a=new String(a+"zero");
              break;
           case 4:
              a=new String(a+"quaranta");
              break;                  
       }
       return a;
    }
    
}
