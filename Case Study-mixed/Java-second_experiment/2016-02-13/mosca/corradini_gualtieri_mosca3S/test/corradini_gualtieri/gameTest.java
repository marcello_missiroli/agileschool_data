/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package corradini_gualtieri;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author s.gualtieri
 */
public class gameTest {
    
    game game;

    @Before
    public void perTuttiITest() {
        game = new game("Pippo", "Pluto");
    }
    
    @Test
    public void zeroDeveEssereLaDescrizionePerIlPunteggio0() {
        assertEquals("zero, zero", game.getPunteggio());
    }

    @Test
    public void quindiciDeveEssereIlDescrittorePerIlPunteggio1() {
        game.puntoPer("Pluto");
        assertEquals("zero, quindici", game.getPunteggio());
    }

    @Test
    public void trentaDeveEssereIlDescrittorePerIlPunteggio2() {
        game.puntoPer("Pippo");
        game.puntoPer("Pippo");
        game.puntoPer("Pluto");
        assertEquals("trenta, quindici", game.getPunteggio());
    }

    
    @Test
    public void quarantaDeveEssereIlDescrittorePerIlPunteggio3() {
        game.puntoPer("Pippo");
        game.puntoPer("Pippo");
        game.puntoPer("Pippo");
        assertEquals("quaranta, zero", game.getPunteggio());
    }
    
    @Test 
    public void robustezza() {
        game.puntoPer("Pluto");
        game.puntoPer("Pluto");
        assertEquals(game.getPunteggio(), "zero, trenta");
        game.puntoPer("Pippo");
        game.puntoPer("Pippo");
        assertEquals(game.getPunteggio(), "trenta, trenta");
        game.puntoPer("Paperino");
        game.puntoPer("Paperoga");
        assertEquals(game.getPunteggio(), "trenta, trenta");
        
        game=new game("Paperino","Paperoga");
        game.puntoPer("Paperino");
        game.puntoPer("Paperoga");
        assertEquals(game.getPunteggio(), "quindici, quindici");
    }




    
    
}
