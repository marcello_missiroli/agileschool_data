/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package corradini_gualtieri;

/**
 *
 * @author s.gualtieri
 */
public class game {
    
    private String giocatore1;
    private String giocatore2;
    private int punteggio1;
    private int punteggio2;
    
    
    game(String pippo, String pluto) {
        this.giocatore1 = pippo;
        this.giocatore2 = pluto;
        this.punteggio1 = 0;
        this.punteggio2 = 0;
    }

    public String getPunteggio() {
        if (punteggio1 == 0 && punteggio2 == 0)
            return "zero, zero";
        if (punteggio1 == 0 && punteggio2 == 15)
            return "zero, quindici";
        if (punteggio1 == 15 && punteggio2 == 0)
            return "quindici, zero";
        if(punteggio1 == 15 && punteggio2 == 15)
            return "quindici, quindici";
        if(punteggio1 == 30 && punteggio2 == 0)
            return "trenta, zero";
        if (punteggio1 == 0 && punteggio2 == 30)
            return "zero, trenta";
        if (punteggio1 == 30 && punteggio2 == 15)
            return "trenta, quindici";
        if (punteggio1 == 15 && punteggio2 == 30)
            return "quindici, trenta";
         if(punteggio1 == 30 && punteggio2 == 30)
            return "trenta, trenta";
         if (punteggio1 == 40 && punteggio2 == 0)
            return "quaranta, zero";
         if (punteggio1 == 0 && punteggio2 == 40)
            return "zero, quaranta";
         if (punteggio1 == 40 && punteggio2 == 15)
            return "quaranta, quindici";
         if (punteggio1 == 15 && punteggio2 == 40)
            return "quindici, quaranta";
         if (punteggio1 == 40 && punteggio2 == 30)
            return "quaranta, trenta";
         if (punteggio1 == 30 && punteggio2 == 40)
            return "trenta, quaranta";
         if (punteggio1 == 40 && punteggio2 == 40)
            return "quaranta, quaranta";
        else
            return "";
    }

    public void puntoPer(String nome_giocatore) {
        switch(nome_giocatore)
        {
                case "Pippo": 
                    if (punteggio1 < 30)
                        punteggio1 += 15;
                    else
                        punteggio1 += 10;
                            break;
                case "Pluto":
                    if (punteggio2 < 30)
                        punteggio2 += 15;
                    else
                        punteggio2 += 10;
                            break;
                case "Paperino": 
                    if (punteggio2 < 30)
                        punteggio2 += 15;
                    else
                        punteggio2 += 10;
                            break;
                case "Paperoga": 
                    if (punteggio2 < 30)
                        punteggio2 += 15;
                    else
                        punteggio2 += 10;
                            break;
        }
    }
    
}
