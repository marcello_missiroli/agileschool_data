/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serpetti_ficociello;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author g.ficociello
 */
public class GameTest {
    
    Game game;

    @Before
    public void perTuttiITest() {
        game = new Game("Pippo", "Pluto");
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testSomeMethod() {
        
    }
    @Test
    public void zeroDeveEssereLaDescrizionePerIlPunteggio0() {
        assertEquals("zero, zero",game.getPunteggio());
    }
    
    @Test
    public void quindiciDeveEssereIlDescrittorePerIlPunteggio1() {
        game.puntoPer("Pluto");
        assertEquals(game.getPunteggio(), "zero, quindici");
    }
    
    @Test
    public void trentaDeveEssereIlDescrittorePerIlPunteggio2() {
        game.puntoPer("Pippo");
        game.puntoPer("Pippo");
        game.puntoPer("Pluto");
        assertEquals(game.getPunteggio(), "trenta, quindici");
    }
    
    @Test
    public void quarantaDeveEssereIlDescrittorePerIlPunteggio3() {
        game.puntoPer("Pippo");game.puntoPer("Pippo");game.puntoPer("Pippo");
        assertEquals(game.getPunteggio(), "quaranta, zero");
    }
    @Test 
    public void robustezza() {
        game.puntoPer("Pluto");
        game.puntoPer("Pluto");
        assertEquals(game.getPunteggio(), "zero, trenta");
        game.puntoPer("Pippo");
        game.puntoPer("Pippo");
        assertEquals(game.getPunteggio(), "trenta, trenta");
        game.puntoPer("Paperino");
        game.puntoPer("Paperoga");
        assertEquals(game.getPunteggio(), "trenta, trenta");
        game=new Game("Paperino","Paperoga");
        game.puntoPer("Paperino");
        game.puntoPer("Paperoga");
        assertEquals(game.getPunteggio(), "quindici, quindici");
    }
}
