/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package panini_diprima;

/**
 *
 * @author p.panini
 */
public class Game {

    private String punteggio;
    private String pippo;
    private String pluto;
    private int puntoPluto=0;
    private int puntoPippo=0;
    
    Game(String pippo, String pluto) {
        this.pippo=pippo;
        this.pluto=pluto;
    }

    public String getPunteggio() {
        String punteggio1="", punteggio2="";
        switch (puntoPippo){
            case 0: 
                punteggio1="zero";
                break;
            case 1:
                punteggio1="quindici";
                break;
            case 2:
                punteggio1="trenta";
                break;
            case 3:
                punteggio1="quaranta";                
        }
        switch (puntoPluto){
            case 0: 
                punteggio2="zero";
                break;
            case 1:
                punteggio2="quindici";
                break;
            case 2:
                punteggio2="trenta";
                break;
            case 3:
                punteggio2="quaranta";                
        }
        return punteggio1+", "+punteggio2;
    }    

    public void puntoPer(String pluto) {
        if (pluto.equals("Pippo")){
            puntoPippo++;
        }
        if (pluto.equals("Pluto")){
            puntoPluto++;
        }
    }
}
