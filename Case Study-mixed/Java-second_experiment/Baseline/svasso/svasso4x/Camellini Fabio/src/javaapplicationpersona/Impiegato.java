/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplicationpersona;

/**
 *
 * @author Utilizzatore
 */
public class Impiegato extends Persona {
    private double salario;
    
    public Impiegato(String nome, String cognome, int eta, double salario){
        super(nome, cognome, eta);
        this.salario=salario;
    }
    public String Dettagli(){
        
        String p="";
        p+=super.Dettagli()+" "+salario;
        return p;
    }
    public double getSalario(){return salario;}
    public void setSalario(double salario){this.salario=salario;}
    
    public void AumentaSalario(Impiegato i){
        double s;
        s=i.salario+(i.salario*0.05);
        i.setSalario(s);
    }
}
