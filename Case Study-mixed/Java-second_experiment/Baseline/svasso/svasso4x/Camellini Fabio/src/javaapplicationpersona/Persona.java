/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplicationpersona;

/**
 *
 * @author Utilizzatore
 */
public class Persona {
    protected String nome;
    protected String cognome;
    protected int eta;
    
    public Persona(String nome, String cognome, int eta){
        this.nome=nome;
        this.cognome=cognome;
        this.eta=eta;
    }
    public String getNome(){return nome;}
    public String getCognome(){return cognome;}
    public int getEta(){return eta;}
    public void setNome(String nome){this.nome=nome;}
    public void setCognome(String cognome){this.cognome=cognome;}
    public void setEta(int eta){this.eta=eta;}
    public String Dettagli(){
        String l="";
        l+=nome+" "+cognome+" "+eta;
        return l;
    }
}
