package esimpiegati_scialfa_monari;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author i.scialfa
 */
public class Impiegato {
   private int matricola;
   private int nominativo;
   private int annoAssunzione;
   private int livelloRetribuito;
   private int anzianita;
   private static int base=800;
  public Impiegato(int matricola,int nominativo,int annoAssunzione,int livelloRetribuito)
  {
      this.matricola=matricola;
      this.nominativo=nominativo;
      this.annoAssunzione=annoAssunzione;
      setlivelloRetribuito(livelloRetribuito);
      
  }
  public int getmatricola()
  {
      return matricola;
  }
  public int getnominativo()
  {
      return nominativo;
  }
  public int getannoAssunzione()
  {
      return annoAssunzione;
  }
  public int getlivelloRetribuito()
  {
      return livelloRetribuito;
  }
  public void setnominativo(int nominativo)
  {
      this.nominativo=nominativo;
  }
  public void setmatricola(int matricola)
  {
      this.matricola=matricola;
  }
  public void setannoAssunzione(int annoAssunzione)
  {
      this.annoAssunzione=annoAssunzione;
  }
  public void setlivelloRetribuito(int livelloRetribuito)
  {
      if(livelloRetribuito<6&&livelloRetribuito>0)
      {
      this.livelloRetribuito=livelloRetribuito;
      }
      else
      {
          if(livelloRetribuito<0)
          {
              this.livelloRetribuito=1;  
          }
          else if(livelloRetribuito>6)
          {
              this.livelloRetribuito=6;        
          }
        
      }
  }
  public int calcolaStipendio()
  {
      int stipendio;
      stipendio=base+(50*this.anzianita)+(25*this.livelloRetribuito);
      return stipendio;
  }
   @Override
  public String toString()
  {
     return "NOME: "+getNominativo()+"ANZIANITA: "+"STIPENDIO: ";
     
  }
}
