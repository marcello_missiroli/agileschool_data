/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pjtreno;

/**
 *
 * @author a.solombrino
 */
public class VagoneMerci extends Vagone{
    private double VolumeCarico;
    private double PesoMaxCarico;
    private double PesoEffettivoReale;
    
    public VagoneMerci()
    {
        super();
        this.VolumeCarico=0;
        this.PesoMaxCarico=0;
        this.PesoEffettivoReale=0;
    }
    public VagoneMerci(double VolumeCarico,double PesoMaxCarico, double PesoEffettivoReale,String codice,double pesoavuoto,String NomeAzienda,int AnnoCostruzione)
    {
        super(codice,pesoavuoto,NomeAzienda,AnnoCostruzione);
        this.VolumeCarico=VolumeCarico;
        this.PesoMaxCarico=PesoMaxCarico;
        this.PesoEffettivoReale=PesoEffettivoReale;
    }
    public double calcolopeso()
    {
        return PesoEffettivoReale;
    }
    
}
