/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pjtreno;

/**
 *
 * @author a.solombrino
 */
public class VagonePasseggeri extends Vagone{
    private int classe;
    private int PostiDisponibili;
    private int PostiOccupati;
    
    public VagonePasseggeri()
    {
        super();
        this.classe=0;
        this.PostiDisponibili=0;
        this.PostiOccupati=0;
    }
     public VagonePasseggeri(int classe, int PostiDisponibili, int PostiOccupati,String codice,double pesoavuoto,String NomeAzienda,int AnnoCostruzione)
     {
         super(codice,pesoavuoto,NomeAzienda,AnnoCostruzione);
         this.classe=classe;
         this.PostiDisponibili=PostiDisponibili;
         this.PostiOccupati=PostiOccupati;
     }
     public double calcolopeso()
     {
         return PostiOccupati*65;
     }
    
}
