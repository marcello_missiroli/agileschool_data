/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pjtreno;

/**
 *
 * @author a.solombrino
 */
abstract public class Vagone {
    protected String codice;
    protected double pesoavuoto;
    protected String NomeAzienda;
    protected int AnnoCostruzione;
    
    public Vagone() // COSTRUTTORE DEFAULT
    {
        this.codice="";
        this.pesoavuoto=0;
        this.NomeAzienda="";
        this.AnnoCostruzione=0;
    }
    public Vagone(String codice,double pesoavuoto,String NomeAzienda,int AnnoCostruzione)    //COSTRUTTORE CON PARAMETRI
    {
        this.codice=codice;
        this.pesoavuoto=pesoavuoto;
        this.NomeAzienda=NomeAzienda;
        this.AnnoCostruzione=AnnoCostruzione;
    }
    public abstract double calcolopeso();
}
