/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package garage;

/**
 *
 * @author y.kilic
 */
public class Furgone extends Veicolo{
    protected float CapacitaCarico;
    protected String tipo;

    public Furgone(float CapacitaCarico, int annoImmatricolazione, String marca, String modello, String tipoAlimentazione, int cilindrata) {
        super(annoImmatricolazione, marca, modello, tipoAlimentazione, cilindrata);
        this.CapacitaCarico = CapacitaCarico;
        this.tipo=tipo;
    }

    public float getCapacitaCarico() {
        return CapacitaCarico;
    }

    public void setCapacitaCarico(float CapacitaCarico) {
        this.CapacitaCarico = CapacitaCarico;
    }
    
    
    
}
