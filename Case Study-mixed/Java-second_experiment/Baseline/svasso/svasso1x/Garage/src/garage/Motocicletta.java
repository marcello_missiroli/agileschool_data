/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package garage;

/**
 *
 * @author y.kilic
 */
public class Motocicletta extends Veicolo{
    protected int tempiMotore;
    protected String tipo;

    public Motocicletta(int tempiMotore, int annoImmatricolazione, String marca, String modello, String tipoAlimentazione, int cilindrata) {
        super(annoImmatricolazione, marca, modello, tipoAlimentazione, cilindrata);
        this.tempiMotore = tempiMotore;
        this.tipo=tipo;
    }

    public int getTempiMotore() {
        return tempiMotore;
    }

    public void setTempiMotore(int tempiMotore) {
        this.tempiMotore = tempiMotore;
    }
    
    
}
