/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package garage;

/**
 *
 * @author y.kilic
 */
public class Autovettura extends Veicolo{
    static final double COSTO_ORARIO=1.5;
protected int NumeroPosti;
protected String tipo;

    public Autovettura(int NumeroPosti, String tipo, 
            int annoImmatricolazione, String marca, String modello, 
            String tipoAlimentazione, int cilindrata) {
        super(annoImmatricolazione, marca, modello, 
                tipoAlimentazione, cilindrata);
        this.NumeroPosti = NumeroPosti;
        this.tipo = tipo;
    }

    public int getNumeroPosti() {
        return NumeroPosti;
    }

    public Autovettura clone(){
        Autovettura tmp = new Autovettura(NumeroPosti, annoImmatricolazione,marca, modello, tipoAlimentazione, cilindrata);
        return tmp;
    }
    
    double costoParcheggio (int ore, int minuti){
        double costo= ore*COSTO_ORARIO;
        if (minuti != 0){
            costo += COSTO_ORARIO;
            return costo;
        }
    }

    
    
}
