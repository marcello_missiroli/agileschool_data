/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package garage;

/**
 *
 * @author y.kilic
 */
abstract public class Veicolo {
    protected int annoImmatricolazione;
    protected String marca;
    protected String modello;
    protected String tipoAlimentazione;
    protected int cilindrata;
    
    public Veicolo(){
        this.annoImmatricolazione=0;
        this.marca=" ";
        this.modello=" ";
        this.tipoAlimentazione=" ";
        this.cilindrata=0;
        }
    
    public Veicolo (int annoImmatricolazione, String marca, String modello, String tipoAlimentazione, int cilindrata){
        this.annoImmatricolazione=annoImmatricolazione;
        this.marca=marca;
        this.modello=modello;
        this.tipoAlimentazione=tipoAlimentazione;
        this.cilindrata=cilindrata;
    }

    public int getAnnoImmatricolazione() {
        return annoImmatricolazione;
    }

    public void setAnnoImmatricolazione(int annoImmatricolazione) {
        this.annoImmatricolazione = annoImmatricolazione;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModello() {
        return modello;
    }

    public void setModello(String modello) {
        this.modello = modello;
    }

    public String getTipoAlimentazione() {
        return tipoAlimentazione;
    }

    public void setTipoAlimentazione(String tipoAlimentazione) {
        this.tipoAlimentazione = tipoAlimentazione;
    }

    public int getCilindrata() {
        return cilindrata;
    }

    public void setCilindrata(int cilindrata) {
        this.cilindrata = cilindrata;
    }

    @Override
    public String toString() {
        return "Veicolo con " + "annoImmatricolazione= " + annoImmatricolazione + ", marca=" + marca + ", modello=" + modello + ", tipoAlimentazione=" + tipoAlimentazione + ", cilindrata=" + cilindrata;
    }
    
    
}
