/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package garage;

/**
 *
 * @author y.kilic
 */
public class Garage2 {
    //attributi
    private static final int NumPosti=25;
    private VeicoloInGarage posti[];

    public Garage2() {
        this.posti = new VeicoloInGarage[NumPosti];
    }

    public int ingressoVeicolo(Veicolo veicolo, int ora, int posizione){
        int ogg1=new VeicoloInGarage(ora, veicolo);
        if ((posizione < 0|| posizione >= NumPosti))
            return -1; //posizione non valida
        if (posti[posizione]!=null)
            return -2;  //posizione occupata
        if (posti[posizione]=null)
            return veicolo;
        
        
    }
    public float uscitaVeicolo(int posizione, int ora){
        if ((posizione <0) || (posizione > NumPosti))
            return -1; //posizione non corretta
        if (posti[posizione]== null)
            return -2;//posizione vuota nessun veicolo
        posti[posizione]=null;// rimozione libro in posizione
        return posizione;//restituisce la posizione liberata
        
        
        
    }

    
    
    
}
