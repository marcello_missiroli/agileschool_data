/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package persona;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author carbonaro.cristian
 */
public class TestImpiegato {
    public static void main(String[] args){
        Impiegato P1 = new Impiegato();
        P1.setCognome("Asdrubale");
        P1.setNome("Paolo");
        P1.setEtà(27);
        P1.setSalario(1000);
        
        System.out.println("I dettagli della persona sono: " + P1.dettagli());
        
        int aum = 0;
        
        
try {
        
        InputStreamReader input = new InputStreamReader(System.in);
        BufferedReader keyboard = new BufferedReader(input);
    
        System.out.println("Inserisci 1 per aumentare il salario del 20%:");
        aum = Integer.parseInt(keyboard.readLine());
        
        if(aum == 1){
         P1.salario = P1.salario + ((P1.salario * 20) / 100);
         System.out.println("I dettagli della persona sono: " + P1.dettagli());
        }
        
        
            
}
   catch(IOException | NumberFormatException exception) { 
       System.out.println("Errore!");
    }
    }  
}
