/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package persona;

/**
 *
 * @author carbonaro.cristian
 */
public class Persona {

    protected String nome;
    protected String cognome;
    protected int età;
    
    public Persona(){
        nome = null;
        cognome = null;
        età = 0;
    }
    public Persona(String n, String c, int e){
        nome = n;
        cognome = c;
        età = e;
    }
    
    public void setNome(String n){
    nome = n;
    }
    public void setCognome(String c){
        cognome = c;
    }
    public void setEtà(int e){
        età = e;
    }
    
    public String getNome(){
        return nome;
    }
    public String getCognome(){
        return cognome;
    }
    public int getEtà(){
        return età;
    }
    
    public String dettagli(){
        String dettagli = "";
        return dettagli + nome+" "+cognome+""+età;
    }
         
    
}
