/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package persona;

/**
 *
 * @author carbonaro.cristian
 */
public class Impiegato extends Persona {
    protected int salario;
    
    public Impiegato(){
        salario = 0;
    }
    
    public Impiegato(String n, String c, int e, int s){
        nome = n;
        cognome = c;
        età = e;
        salario = s;
    }
    
    public void setSalario(int s){
        salario = s;
    }
    
    public int getSalario(){
        return salario;
    }
    
    public void aumentoSalario(int s){
        salario = salario + salario * s / 100; 
    }
    
    public int getAumentasalario(){
        return salario;
    }
    
    public String dettagli(){
        String dettagli = "";
        return dettagli + nome + cognome + età + salario;
    }
    
    

}
