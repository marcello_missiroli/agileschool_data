/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;
import adt.*;

/**
 *
 * @author Aiello Matteo, Di Cristo Salvatore, Marchi Gabriele
 */
public class Traghetto {
    //attributi
    int lunghezza;
    Pila <Auto> p;
    int spazioDisp;
    
    //costruttore
    public Traghetto() {
        lunghezza = 50;
        p=new <Auto> Pila(); 
        spazioDisp=lunghezza;
    }
    
    public void inserisciAuto(Auto a){
        p.push(a);
    }

    public int getLunghezza() {
        return lunghezza;
    }

    public int getSpazioDisp() {
        return spazioDisp;
    }
    
    public Auto leggiUltimaAuto(){
        Auto a;
        a=p.top();
        return a;
    }
    
    public Auto rimuoviAuto(){
        Auto a;
        a=p.top();
        try{
            p.pop();
            
        }
        catch (Exception e){
           a=null; 
            
        }
        return a;
        
    }
    
    public boolean isEmpty(){
        return p.isEmpty();
    }
    
}
