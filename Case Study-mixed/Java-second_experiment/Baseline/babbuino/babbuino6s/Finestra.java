import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by harda on 29/01/2016.
 */
public class Finestra extends JFrame {
    private JPanel rootPanel;
    private JButton inserisciLaMacchinaButton;
    private JButton estraiLaMacchinaButton;
    private JTextArea areaEntrata;
    private JTextArea areaUscita;
    private JPanel panelCenter;
    private JPanel rightPanel;
    private JPanel leftPanel;
    private JTextField targaField;
    private JTextField lunghField;
    private JLabel targalbl;
    private JLabel lunglbl;
    private JLabel uscitaLbl;
    private JLabel entrataLbl;
    private JProgressBar progressBar;
    private JLabel lunghLbl;
    private JButton resetButton;
    private JLabel icon1;
    private JLabel allertaTarga;
    Traghetto <Macchina> traghetto=new Traghetto();
    Macchina macchina;
    Frame frame=new Frame();
    public Finestra(final int largh, final int lungh){
        super("Caronte 2.0| by Harda Chadi");
        setVisible(true);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setContentPane(rootPanel);
        setSize(largh,lungh);
        setLocationRelativeTo(null);
        String s = (String) JOptionPane.showInputDialog(frame, "Quant'è la larghezza del traghetto?");
        while(s==(null) || s.equals("") || String.valueOf(s).equals("0") || Integer.parseInt(s)<=0){
            s = (String) JOptionPane.showInputDialog(frame, "Quant'è la larghezza del traghetto?");
        }
        lunghLbl.setText("Lunghezza traghetto: "+s);
        traghetto.setSpazioDisponibile(Integer.parseInt(s));
        progressBar.setValue(100);
        progressBar.setForeground(Color.green);
        inserisciLaMacchinaButton.addActionListener(new ActionListener() {
            int a=1;
            int b=1;
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if(targaField.getText().equals("") && lunghField.getText().equals("")){
                    JOptionPane.showMessageDialog(frame,"Inserisci la targa e la lunghezza","Errore",JOptionPane.WARNING_MESSAGE);
                }
                else if (targaField.getText().length()<7){
                    JOptionPane.showMessageDialog(frame,"Inserisci una targa di almeno 7 caratteri.","Errore",JOptionPane.WARNING_MESSAGE);
                }
                else if (lunghField.getText()==null || Integer.parseInt(lunghField.getText())==0 || Integer.parseInt(lunghField.getText())<0){
                    JOptionPane.showMessageDialog(frame,"Inserisci una lunghezza valida","Errore",JOptionPane.WARNING_MESSAGE);
                }
                else if(Integer.parseInt(lunghField.getText())==0){
                    JOptionPane.showMessageDialog(frame,"Lunghezza impossibile","Errore",JOptionPane.WARNING_MESSAGE);
                }
                else if (Integer.parseInt(lunghField.getText())>traghetto.getSpazioDisponibile()&&traghetto.getSpazioDisponibile()>0){
                    JOptionPane.showMessageDialog(frame,"Hai superato la lunghezza del traghetto"+"\n"+"Lunghezza="+traghetto.getSpazioDisponibile(),"Errore",JOptionPane.WARNING_MESSAGE);
                }
                else if (traghetto.getSpazioDisponibile()==0){
                    progressBar.setForeground(Color.RED);
                    progressBar.setValue(100);
                    JOptionPane.showMessageDialog(frame,"Non c'è più spazio","Errore",JOptionPane.WARNING_MESSAGE);
                }
                else{
                        int lunghTrag=traghetto.getSpazioDisponibile();
                        String targa=targaField.getText();
                        int lungh=Integer.parseInt(lunghField.getText());
                        macchina=new Macchina(lungh,targa,b);
                        traghetto.inserisciMacchina(macchina);
                        areaEntrata.append("Macchina "+a+"|"+"Targa:"+targa+" "+"Lunghezza:"+lungh+"\n");
                        lunghTrag=lunghTrag-lungh;
                        traghetto.setSpazioDisponibile(lunghTrag);
                        lunghLbl.setText("Lunghezza traghetto: "+lunghTrag);
                        targaField.setText("");
                        lunghField.setText("");
                        a++;
                        b++;

                }
        }
        });

        estraiLaMacchinaButton.addActionListener(new ActionListener() {
            int a=1;
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if(traghetto.tragettoVuoto()){
                    progressBar.setForeground(Color.green);
                    JOptionPane.showMessageDialog(frame,"Il traghetto è vuoto","Errore",JOptionPane.WARNING_MESSAGE);
                    inserisciLaMacchinaButton.setEnabled(true);
                }
                    else{
                    progressBar.setForeground(Color.red);
                    inserisciLaMacchinaButton.setEnabled(false);
                    int indice=traghetto.leggiUltimaMacchina().getIndice();
                    String targa=traghetto.leggiUltimaMacchina().getTarga();
                    int lunghezza=traghetto.leggiUltimaMacchina().getLunghezza();
                    areaUscita.append("Macchina"+indice+"|"+" Targa: "+targa+" Lunghezza:"+lunghezza+"\n");
                    traghetto.estraiUltimaMacchina();
                }
                }
        });
        resetButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                int n = JOptionPane.showConfirmDialog(
                        frame,
                        "Vuoi cambiare la lunghezza del traghetto?",
                        "",
                        JOptionPane.YES_NO_OPTION);
                if(n==0){
                    String s = (String) JOptionPane.showInputDialog(frame, "Quant'è la larghezza del traghetto?");
                    while(s==(null) || s.equals("") || String.valueOf(s).equals("0") || Integer.parseInt(s)<=0){
                        s = (String) JOptionPane.showInputDialog(frame, "Quant'è la larghezza del traghetto?");
                    }
                    lunghLbl.setText("Lunghezza traghetto: "+s);
                }
                areaEntrata.setText("");
                areaUscita.setText("");
                targaField.setText("");
                lunghField.setText("");
                while(!traghetto.tragettoVuoto()){
                    traghetto.estraiUltimaMacchina();
                }
            }
        });

    }
    public static void main(String[] args){
        Finestra f=new Finestra(700,300);
    }
}
