/**
 * Created by harda on 28/01/2016.
 */
public class Macchina {
    String targa;
    int lunghezza;
    int indice;

    public Macchina(int lunghezza, String targa,int indice) {
        this.lunghezza = lunghezza;
        this.targa = targa;
        this.indice=indice;
    }

    public int getLunghezza() {
        return lunghezza;
    }

    public String getTarga() {
        return targa;
    }

    public void setIndice(int indice) {
        this.indice = indice;
    }

    public int getIndice() {

        return indice;
    }

    public void setTarga(String targa) {
        this.targa = targa;
    }

    public void setLunghezza(int lunghezza) {
        this.lunghezza = lunghezza;
    }
}
