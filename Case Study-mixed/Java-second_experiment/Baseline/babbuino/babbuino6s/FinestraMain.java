import javax.swing.*;
import java.awt.*;

/**
 * Created by harda on 29/01/2016.
 */
public class FinestraMain extends JFrame {
    JButton submit=new JButton("Aggiungi macchina");
    JButton exit=new JButton("Esci (la) macchina");

    JTextField targa=new JTextField("",15);
    JTextField lunghezza=new JTextField("",15);
    JTextArea cronologia=new JTextArea(15,20);

    JPanel panel1=new JPanel();
    JPanel panel2=new JPanel();
    JPanel panel3=new JPanel();
    JPanel panel4=new JPanel();

    JLabel targhetta=new JLabel("Targa");
    JLabel lungo=new JLabel("Lunghezza");

    public FinestraMain(){
        super("TRAGHETTO WORLD|By Chado inc.");
        this.setSize(500,500);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setVisible(true);
        Container c=this.getContentPane();
        panel1.add(targa);
        panel1.add(lunghezza);
        c.add(panel1,BorderLayout.NORTH);

        panel2.add(submit);
        panel3.add(cronologia);
        panel2.add(panel3);
        c.add(panel2,BorderLayout.CENTER);
    }
    public static void main(String[] args){
        FinestraMain f=new FinestraMain();
    }
}
