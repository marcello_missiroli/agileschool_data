import adt.Pila;

import javax.swing.*;

/**
 * Created by harda on 28/01/2016.
 */
public class Traghetto <T> {
    Pila <T> pila;
    boolean semaforo;
    int spazioDisponibile;

    public Traghetto() {
        pila=new Pila<>();
        semaforo=true;
    }

    public void inserisciMacchina(T Macchina){
        pila.push(Macchina);
    }
    public T leggiUltimaMacchina(){
        T Macchina;
        Macchina=pila.top();
        return Macchina;
    }
    public String estraiUltimaMacchina(){
        String verifica="";
        try {
            pila.pop();
        }
        catch (Exception e){
            verifica=e.getMessage();
        }
        return verifica;
    }


    public Pila<T> getPila() {
        return pila;
    }

    public int getSpazioDisponibile() {
        return spazioDisponibile;
    }
    public boolean tragettoVuoto(){
        return pila.isEmpty();
    }

    public void setSpazioDisponibile(int spazioDisponibile) {
        this.spazioDisponibile = spazioDisponibile;
    }
}

