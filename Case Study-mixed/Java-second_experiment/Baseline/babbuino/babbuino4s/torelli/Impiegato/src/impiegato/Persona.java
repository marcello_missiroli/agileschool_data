package impiegato;

public class Persona {
    protected String nome;
    protected String cognome;
    protected int età;
    protected Persona(String n, String c, int e) {
        nome = n;
        cognome = c;
        età = e;
    }
    protected Persona() {
        nome = " ";
        cognome = " ";
        età = 0;
    }
    public void setNome(String n) {
        nome = n;
    }
    public void setCognome(String c) {
        cognome = c;
    }
    public void setEtà(int e) {
        età = e;
    }
    
    public String getNome() {
        return nome;
    }
    public String getCognome() {
        return cognome;
    }
    public int getEtà() {
        return età;
    }
    public void dettagli() {
        System.out.println("Nome: " + nome);
        System.out.println("Cognome: " + cognome);
        System.out.println("Età: " + età);
    }
}