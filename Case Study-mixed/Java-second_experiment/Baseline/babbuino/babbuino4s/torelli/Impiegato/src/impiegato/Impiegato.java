package impiegato;

public class Impiegato extends Persona {
    private float salario;
    public Impiegato(String n, String c, int e, float s) {
        super(n, c, e);
        salario = s;
    }
    public Impiegato() {
        super(" ", " ", 0);
        salario = 0;
    }
    public void setSalario(float s) {
        salario = s;
    }
    public float getSalario() {
        return salario;
    }
    public float aumentaSalario(float s, int p) {
        System.out.print("Aumento salario: ");
        System.out.println(salario = s + (s * p / 100));
        return 0;
    }
    public void dettagli() {
        System.out.println("Nome: " + nome);
        System.out.println("Cognome: " + cognome);
        System.out.println("Età: " + età);
        System.out.println("Salario: " + salario);
    }
}