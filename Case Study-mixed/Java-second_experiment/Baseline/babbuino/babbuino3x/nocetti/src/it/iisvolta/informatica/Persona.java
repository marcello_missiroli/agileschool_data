/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package it.iisvolta.informatica;

/**
 *
 * @author nocetti.gianluca
 */
public class Persona {
    protected String nome;
    protected String cognome;
    protected int eta;
    
    public Persona() {
        nome = "nome";
        cognome = "cognome";
        eta = 18;
    }
    
    public Persona(String nome, String cognome, int eta) {
        this.nome = nome;
        this.cognome = cognome;
        this.eta = eta;
    }
    
    public String getNome() {
        return nome;
    }
    
    public String getCognome() {
        return cognome;
    }
    
    public int getEta() {
        return eta;
    }
    
    public void setNome(String nome) {
        this.nome = nome;
    }
    
    public void setCognome(String cg) {
        this.cognome = cognome;
    }
    
    public void setEta(int et) {
        this.eta = eta;
    }
    
    public String dettagli() {
        return "Nome : " + nome + " Cognome : " + cognome + " Eta : " + eta;
    }
}
