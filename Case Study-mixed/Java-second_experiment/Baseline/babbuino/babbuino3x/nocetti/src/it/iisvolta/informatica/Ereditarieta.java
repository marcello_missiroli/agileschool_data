/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package it.iisvolta.informatica;

/**
 *
 * @author nocetti.gianluca
 */
public class Ereditarieta {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Impiegato imp = new Impiegato("Pino", "Esposito", 22, 12);
        System.out.println(imp.dettagli());
        imp.aumentaSalario(10);
        System.out.println(imp.dettagli());
        
    }
    
}
