/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package it.iisvolta.informatica;

/**
 *
 * @author nocetti.gianluca
 */
public class Impiegato extends Persona {
    float salario;
    
    public Impiegato() {
        nome = "nome";
        cognome = "cognome";
        eta = 18;
        salario = 1;
    }
    
    public Impiegato(String nome, String cognome, int eta, float salario) {
        this.nome = nome;
        this.cognome = cognome;
        this.eta = eta;
        this.salario = salario;
    }
    
    public String dettagli() {
        return super.dettagli() + " Salario : " + salario;
    }
    
    public void aumentaSalario(int pr) {
        salario = salario + (salario / 100) * pr;
    }
    
    public float getSalario() {
        return salario;
    }
}
