/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package euclaide;

/**
 *
 * @author f.di casola
 */
public class franzione {
    private int num;
    private int den;  

    public franzione()
    {
        num=0;
        den=0;
    }
    
        public franzione(int num, int den)
    {
        this.num=num;
        this.den=den;
    }

    public int getNum() {
        return num;
    }

    public int getDen() {
        return den;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public void setDen(int den) {
        this.den = den;
    }
        
       public franzione somma(franzione f)
    {        
        franzione r;
        int a=den*f.den;
        int b=num*f.den+ f.num*den;
        r = new franzione(b, a);
        return r;
    }
       public franzione sottrazione(franzione f)
       {
            franzione r;
        int a=den*f.den;
        int b=num*f.den- f.num*den;

        r = new franzione(b, a);
        return r;
       }
       
        public franzione divisione(franzione f)
       {
            franzione r;
        int a=den*f.num;
        int b=num*f.den;

        r = new franzione(b, a);
        return r;
       }
        public franzione moltiplicazione(franzione f)
       {
            franzione r;
        int a=den*f.den;
        int b=num*f.num;

        r = new franzione(b, a);
        return r;
       }

    @Override
    public String toString() {
        return "{" + "num=" + num + ", den=" + den + '}';
    }
        
        






}