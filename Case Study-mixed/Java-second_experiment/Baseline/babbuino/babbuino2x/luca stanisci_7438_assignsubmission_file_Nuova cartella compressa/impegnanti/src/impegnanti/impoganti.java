/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package impegnanti;

import java.util.*;


/**
 *
 * @author f.di casola
 */
public class impoganti {
    private int matricola;
    private int nominativo;
    private int anno_assunzione;
    private int livello_retributivo;
    private int stipendio;
    
    public impoganti(){
    matricola=0;
    nominativo=0;
    anno_assunzione=0;
    livello_retributivo=1; 
    }
    
     public impoganti(int matricola, int nominativo, int anno_assunzione, int livello_retributivo){
     this.matricola=matricola;
     this.nominativo=nominativo;
     this.anno_assunzione=anno_assunzione;
     this.livello_retributivo=livello_retributivo;        
    }
     
     public void calcolo()
     {
         int base=800;
         GregorianCalendar gc = new GregorianCalendar();
         int anno = gc.get(Calendar.YEAR);
         base+=(anno-this.anno_assunzione)*50;
         base+=25*this.livello_retributivo;
         stipendio=base;
     }    
    

    public void setMatricola(int matricola) {
        this.matricola = matricola;
    }

    public void setNominativo(int nominativo) {
        this.nominativo = nominativo;
    }

    public void setAnno_assunzione(int anno_assunzione) {
        this.anno_assunzione = anno_assunzione;
    }

    public void setLivello_retributivo(int livello_retributivo) {
            this.livello_retributivo = livello_retributivo;        
    }
    
    public void setStipendio(int stipendio) {
            this.stipendio = stipendio;        
    }

    public int getMatricola() {
        return matricola;
    }
    
    public int getStipendio() {
        return stipendio;
    }

    public int getNominativo() {
        return nominativo;
    }

    public int getAnno_assunzione() {
        return anno_assunzione;
    }

    public int getLivello_retributivo() {
        return livello_retributivo;
    }

    @Override
    public String toString() {
        return "impoganti{" + "matricola=" + matricola + ", nominativo=" + nominativo + ", anno_assunzione=" + anno_assunzione + ", livello_retributivo=" + livello_retributivo + ", stipendio=" + stipendio+ '}';
    }
    
    
}
