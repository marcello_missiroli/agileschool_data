/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pjgigi;

import java.util.Scanner;

/**
 *
 * @author f.di casola
 */
public class punto {
    private float x;
    private float y;
    private float distanza;
       
    public punto()
    {      
        x=0;
        y=0;
    }
    public punto(float x, float y)
    {
      this.x=x;
      this.y=y;
    }
    
      public punto(float x)
    {
      this.x=x;
      this.y=x;
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public void setX(float x) {
        this.x = x;
    }

    public void setY(float y) {
        this.y = y;
    }

    @Override
    public String toString() {
        return " punto{" + "x=" + x + ", y=" + y + '}';
    }


    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final punto other = (punto) obj;
        if (Float.floatToIntBits(this.x) != Float.floatToIntBits(other.x)) {
            return false;
        }
        if (Float.floatToIntBits(this.y) != Float.floatToIntBits(other.y)) {
            return false;
        }
        return true;
    }
    
    public float distanza(float x, float y)
    {
     this.distanza=((Math.pow(this.x-0))+(Math.pow(this.y-0)));
     
     
    }
    
}
