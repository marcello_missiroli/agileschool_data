/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lampadaina.luminaisa;

/**
 *
 * @author f.di casola
 */
public class lampiadina {
    private int colore;
    private int luminosità;
    private boolean stato;    


    public lampiadina(int colore)
    {   
        this.colore=colore;
        this.luminosità=50;
        this.stato=false;
    }
    
    public int aumento()
    {
        if(luminosità < 50 )
            setLuminosità(luminosità+1);
        return getLuminosità();
    }
    
      public int diminimento()
    {
        if(luminosità > 1 )
        {
            if(this.luminosità>=1)                    
                setLuminosità(luminosità-1);
            else
              {
                setStato(false);
                setLuminosità(1);
              }
        }
        return getLuminosità();
    }
      public boolean accensione()
      {
          if(this.stato==false)
              setStato(true);
          setLuminosità(1);
          return isStato();
          
      }
      public boolean spegnimento()
      {
          if(this.stato==true)
              setStato(false);
          setLuminosità(1);
          return isStato();
      }
      public int restituzionecolore()
      {
          return getColore(); 
      }
            public int restituzionelum()
      {
          if(stato==true)
                return getLuminosità(); 
          else
                return 0;                  
      }
      
      public int bancacolore()
      {
          int conta=0;
          
      }
    
    
    
    
    
    

    public int getColore() {
        return colore;
    }

    public int getLuminosità() {
        return luminosità;
    }

    public boolean isStato() {
        return stato;
    }

    public void setColore(int colore) {
        this.colore = colore;
    }

    public void setLuminosità(int luminosità) {
        this.luminosità = luminosità;
    }

    public void setStato(boolean stato) {
        this.stato = stato;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    }


    
    
    
    
    
    
    
    
}
