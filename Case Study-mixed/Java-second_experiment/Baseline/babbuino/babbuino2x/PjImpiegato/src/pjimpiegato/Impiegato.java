/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pjimpiegato;

import java.util.*;

/**
 *
 * @author r.golinelli
 */
public class Impiegato {
   private int matricola;
   private String nominativo;
   private int anno_assunzione;
   private int livello_retributivo;
   public Impiegato()
    {
        matricola=0;
        nominativo="";
        anno_assunzione=0;
        livello_retributivo=1;
    }
   
   
    public int getMatricola(){return matricola;}
    public void setMatricola(int m){matricola=m;}
    
    public String getNominativo(){return nominativo;}
    public void setNominativo(String n){nominativo=n;}
    
    public int getAAssunzione(){return anno_assunzione;}
    public void setAAssunzione(int a){anno_assunzione=a;}
    
    public int getRetribuzione(){return livello_retributivo;}
    public void setRetribuzione(int lr){livello_retributivo=lr;}
    
    public void menoLivello(){if(livello_retributivo>1){livello_retributivo--;}}
    public void piuLivello(){if(livello_retributivo<6){livello_retributivo++;}}
    
    public int Anzianita()
    {
        GregorianCalendar gc = new GregorianCalendar();
        int anno = gc.get(Calendar.YEAR);
        anno=anno-this.anno_assunzione;
        return anno;
    }
    
    public int CalcoloStipendio(){
        int s=800;
        s=s+this.livello_retributivo*25+this.Anzianita()*50;
        return s;
    }
    
    @Override
    public String toString()
    {
        return "l'impiegato: "+this.nominativo+"\n"+"Matricola: "+
                this.matricola+"\n"+"l'anno di assunzione: "+this.anno_assunzione+"\n"+
                "livello retributivo: "+this.livello_retributivo+"\n"+
                "stipendio: "+this.CalcoloStipendio()+"\n";
    }
}
/*
Si implementi il metodo main 
(ambiente console) che preveda 
la creazione di un oggetto di
tipo Impiegato e l'utilizzo su
questo dei vari metodi della classe.*/