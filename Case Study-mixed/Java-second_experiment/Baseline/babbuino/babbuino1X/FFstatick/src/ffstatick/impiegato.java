/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ffstatick;

import java.util.*;

/**
 *
 * @author a.barbani
 */
public class impiegato {
    private int matricola;
    private String nominativo;
    private int annoass;
    private int nfeeder;
    private int nliv; //valore retributivo
    private static int fissa=800;
    private static int ffeeder=50;
    private static int liv=25;
    
    public impiegato ()
    {
        GregorianCalendar gc = new GregorianCalendar();

        int anno = gc.get(Calendar.YEAR);

        annoass=anno;
        nliv=1;
        matricola=69;
        nominativo = "Nome Cognome";
        nfeeder=0;
        
    }
    public impiegato(int matricola, String nominativo, int annoass, int nliv )
    {
        this.matricola=matricola;
        this.nominativo=nominativo;
        this.annoass=annoass;
        this.nliv=nliv;
        GregorianCalendar gc = new GregorianCalendar();

        int anno = gc.get(Calendar.YEAR);
        nfeeder=anno-annoass;
    }

    public int getMatricola() {
        return matricola;
    }

    public String getNominativo() {
        return nominativo;
    }

    public void setNfeeder(int nfeeder) {
        this.nfeeder = nfeeder;
    }

    public void setNliv(int nliv) {
        if (nliv<0)
            nliv=1;
        if (nliv>6)
            nliv=6;
        this.nliv = nliv;
    }

    @Override
    public String toString() {
        return "impiegato{" + "matricola=" + matricola + ", nominativo=" + nominativo + ", annoass=" + annoass + ",lavora da " + nfeeder + "anni"+", è nel livello retributivo " + nliv + '}'+"il suo stipendio è "+stipendio();
    }

    public static void setFissa(int fissa) {
        impiegato.fissa = fissa;
    }

    public static void setFfeeder(int ffeeder) {
        impiegato.ffeeder = ffeeder;
    }

    public static void setLiv(int liv) {
        impiegato.liv = liv;
    }

    public int getNfeeder() {
        return nfeeder;
    }

    public int getNliv() {
        return nliv;
    }

    public static int getFissa() {
        return fissa;
    }

    public static int getFfeeder() {
        return ffeeder;
    }

    public static int getLiv() {
        return liv;
    }

    public int getAnnoass() {
        return annoass;
    }


    
    public void setMatricola(int matricola) {
        this.matricola = matricola;
    }

    public void setNominativo(String nominativo) {
        this.nominativo = nominativo;
    }

    public void setAnnoass(int annoass) {
        this.annoass = annoass;
    }
public boolean equals(impiegato x)
{
    boolean a=false;
    if (nliv==x.getNliv()&&nfeeder==x.getNfeeder()&&x.getMatricola()==matricola&&x.getAnnoass()==annoass&&x.getNominativo()==nominativo);
        a=true;
    return a;
}
public int stipendio()
{
    int s;
    s=nliv*liv+fissa+ffeeder*nfeeder;
    return s;
}

            
}
