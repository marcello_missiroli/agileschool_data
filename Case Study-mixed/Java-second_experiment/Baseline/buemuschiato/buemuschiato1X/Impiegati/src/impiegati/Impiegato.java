/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package impiegati;

/**
 *
 * @author a.quattrocchi
 */
public class Impiegato {
    private int matricola;
    private int anno_assunzione;
    private int livello_retributivo;
    private String nominativo;
    private static int base = 800;
    private int anzianita;
    public Impiegato()
    {
        this.matricola = 0;
        this.nominativo = "";
        this.anno_assunzione = 0;
        this.livello_retributivo = 0;
        this.anzianita = 0;
    }

    public Impiegato(int matricola, String nominativo, int anno_assunzione, int livello_retributivo, int anzianita)
    {
        this.matricola = matricola;
        this.nominativo = nominativo;
        this.anno_assunzione = anno_assunzione;
        this.livello_retributivo = livello_retributivo;
        this.anzianita = anzianita;
    }
    
    public int getMatricola() {
        return matricola;
    }

    public void setMatricola(int matricola) {
        this.matricola = matricola;
    }

    public int getAnno_assunzione() {
        return anno_assunzione;
    }

    public void setAnno_assunzione(int anno_assunzione) {
        this.anno_assunzione = anno_assunzione;
    }

    public int getLivello_retributivo() {
        return livello_retributivo;
    }

    public void setLivello_retributivo(int livello_retributivo) {
        this.livello_retributivo = livello_retributivo;
    }

    public String getNominativo() {
        return nominativo;
    }

    public void setNominativo(String nominativo) {
        this.nominativo = nominativo;
    }

    public int getAnzianita() {
        return anzianita;
    }

    public void setAnzianita(int anzianita) {
        this.anzianita = anzianita;
    }
    
    public int Stipendio(int anno)
    {
       int stipendio;
       anzianita = anno - anno_assunzione;
       stipendio = base + (50*anzianita) + (25*livello_retributivo);
       return stipendio;
    }
}
