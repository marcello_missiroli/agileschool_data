/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package impiegati;
import java.util.*;
/**
 *
 * @author a.quattrocchi
 */
public class Main_Impiegati {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        GregorianCalendar gc = new GregorianCalendar();
        int anno = gc.get(Calendar.YEAR);
        String nominativo;
        int matricola, anno_assunzione, livello_retributivo, anzianita;
        System.out.print("Inserire il nominativo dell'Impiegato: ");
        Scanner in = new Scanner(System.in);
        nominativo = in.next();
        System.out.print("Inserire la matricola dell'Impiegato: ");
        matricola = in.nextInt();
        System.out.print("Inserire l'anno d'assunzione dell'Impiegato: ");
        anno_assunzione = in.nextInt();
        System.out.print("Inserire il livello retributivo dell'Impiegato: ");
        livello_retributivo = in.nextInt();
        System.out.print("Inserire l'eta' dell'Impiegato: ");
        anzianita = in.nextInt();
        Impiegato I1 = new Impiegato(matricola, nominativo, anno_assunzione, livello_retributivo, anzianita);
        System.out.println(I1.Stipendio(anno));
        
    }
    
}
