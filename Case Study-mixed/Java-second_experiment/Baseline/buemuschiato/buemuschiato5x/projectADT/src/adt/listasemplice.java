package adt;
/**
 * Esempio semplificato di lista semplice concatenata
 * formata da nodi legati tra loro da un riferimento
 * Il nodo ha una struttura fissa e contiene solo una stringa
 * @author peroni
 */
public class listasemplice {
    /**
     * riferimento all'elemento di testa della lista
     */
    private nodo head;
    /**
     * costruttore di default: genera una lista vuota che contiene
     * un null nella testa
     */
    public listasemplice(){
        head=null;
    }
    /**
     * Aggiunge un nodo in testa alla lista:
     * crea un nuovo nodo a partire dall'informazione passata come parametro
     * fa puntare il suo riferimento al nodo che era precedentemente in testa
     * e assegna il riferimento di questo nodo alla testa facendolo diventare
     * il nuovo primo nodo
     * @param info informazione da mettere in testa
     */
    public void addHead(String info){
        /* */
        nodo n=new nodo();
        n.setInfo(info);
        n.setNext(head);
        head=n;
        /* */
        /* Alternativa: in un'unica operazione:
        head=new nodo(info,head);
        */
    }
    /**
     * Aggiunge un nodo in coda alla lista
     * Se la lista è vuota l'operazione diventa un inserimento in testa
     * Se la lista non è vuota il metodo naviga attaverso la lista
     * fino a trovare l'ultimo nodo che contiene null nel riferimento
     * a next.
     * crea un nuovo nodo a partire dall'informazione passata come parametro
     * e con riferimento nullo
     * assegna il riferimento di questo nodo al precedente ultimo nodo
     * facendolo diventare il penultimo
     * @param info informazione da mettere in coda
     */
    public void addTail(String info){
        /* */
        nodo current=head;
        //controlla se la lista è vuota
        if (current==null) { //se lista vuota diventa un inserimento in testa
            addHead(info);
        }
        else { //se lista non vuota naviga fino all'ultimo elemento ed inserisce
            while (current.getNext()!=null){
                current=current.getNext();
            }
            //crea un nuovo ultimo nodo con info e appende in coda
            nodo nuovo=new nodo(info,null);
            current.setNext(nuovo);
        }
        /* */
        /* Alternativa: usando un metodo privato di navigazione
         nodo tail=findTail();
         nodo nuovo=new nodo(info,null);
         tail.setNext(nuovo);
         */
        /* Altra alternativa: in una sola riga usando un metodo privato di navigazione
         findTail().setNext(new nodo(info,null));
        */
    }
    /**
     * Inserisce un nuovo nodo di valore passato come parametro
     * nella posizione passata come parametro
     * Se la posizione è un intero positivo o nullo ed è
     * compresa nella dimensione attuale della lista
     * naviga fino a raggiungere il nodo precedente alla posizione 
     * richiesta inserisce un nuovo nodo contenente info come successivo
     * al nodo trovato e ricollega il nuovo nodo al resto della lista
     * casi particolari:
     * se la posizione è 0 diventa un inserimento in testa
     * se la posizione è negativa o eccede le dimensioni attuali della lista
     * genera una eccezione Index out of Bounds
     * @param info informazione da inserire nella lista
     * @param pos posizione di inserimento
     * @throws IndexOutOfBoundsException posizione fuori della lista
     */
    public void insert(String info, int pos) throws IndexOutOfBoundsException {
        /* */
        //verifica che la posizione sia compresa tra 0 e la dimensione attuale
        if (pos < size()&&(pos>=0)){ //posizione compresa tra 0 e la dimensione attule
            //verifica se la lista è vuota
            if (pos==0){// posizione 0diventa un inserimento in testo
                addHead(info);
            }
            else {  //lista non vuota naviga fino a trovare il nodo precedente
                nodo current=head;
                int i=0;
                while ((i<pos-1)&&(current.getNext()!=null)){
                    current=current.getNext();
                    i++;
                }
                //trovato il precedente aggiunge dopo e ricollega
                nodo nuovo=new nodo(info,current.getNext());
                current.setNext(nuovo);
                //nodo precedente=findNodo(pos-1);
                //nodo nuovo=new nodo(info,precedente.getNext());
                //precedente.setNext(nuovo);
            }
        }
        else { //posizione negativa o maggiore delle dimensioni
            throw(new IndexOutOfBoundsException());
        }
        /* */
        /* Alternativa: usando un metodo privato di navigazione 
        //verifica se è la posizione 0
        if (pos==0) { //posizione 0 diventa un inserimento in testa
            addHead(info);
        }
        else { //posizione non 0 cerca nodo
            nodo precedente=findNodo(pos-1);
            nodo nuovo=new nodo(info,precedente.getNext());
            precedente.setNext(nuovo);
            / * Alternativa: in una sola riga * /
             findNodo(pos-1).setNext(new nodo(info,findNodo(pos-1).getNext()));
            / * * /
        }
        / * */
    }
    /**
     * Cancellazione dalla testa
     */
    public void deleteHead(){
        head=head.getNext();
    }
    /**
     * Rimuove un nodo dalla lista nella posizione specificata come parametro
     * Se la pozione di cancellazione è entro i limiti (0..dimensione) naviga
     * nella lista fino a trovare il riferimento al nodo da cancellare
     * ricordando anche il riferimento al nodo precedente
     * assegna al next del nodo precedente il next del nodo trovato
     * eliminando il nodo trovato dalla lista (viene rilasciato dal garbage collector
     * e ricollegando la lista
     * @param pos posizione del nodo da cancellare
     * @throws IndexOutOfBoundsException posizione fuori della lista
     */
    public void delete(int pos) throws  IndexOutOfBoundsException{
        /* */
        //verifica se la posizione è entro i limiti
        if (pos < size()&&(pos>=0)){ //posizione compresa tra 0 e dimensione
            //verifica se la posizione è 0
            if (pos==0){ //posizione 0: diventa una cancellazione dalla testa
                deleteHead();
            }
            else { //non è in testa: naviga per cercare il nodo e cancella
                int i=0;
                nodo current=head;
                nodo previous=null;
                //cerca la posizione ma ricorda anche la precedente
                while((i<pos)&&(current.getNext()!=null)){
                    previous=current;
                    current=current.getNext();
                    i++;
                }
                //collega il successivo con il precedente
                nodo next=current.getNext();
                previous.setNext(next);
            }
        }
        else { //posizione fuori dai limiti
            throw(new IndexOutOfBoundsException());
        }
        /* */
        /* Alternativa: usando un metodo privato di navigazione * /
        //verifica se la posizione è 0
        if (pos==0){ //posizione 0: diventa una cancellazione in testa
            deleteHead();
        }
        else { //naviga usando il metodo privato di navigazione
            /* * /
            nodo precedente=findNodo(pos-1);
            nodo corrente=precedente.getNext();
            nodo successivo=corrente.getNext();
            precedente.setNext(successivo);
            / * */
            /* Altra alternativa: in un'unica istruzione * /
            findNodo(pos-1).setNext(findNodo(pos).getNext());
            /* * /
        }
        / * */
    }
    /**
     * Svuota la lista rilasciando tutti i nodi al garbage collector
     */
    public void clear(){
        head=null;
    }
    /**
     * Restituisce il primo elemento della lista
     * Se la lista è vuota genera una eccezione
     * @return contenuto del primo elemento della lista
     * @throws NullPointerException lista vuota
     */
    public String getFirst() throws NullPointerException{
        String ris="";
        //verifica se la lista è vuota
        if (head!=null){ //lista non vuota estre il primo elemento
            ris=head.getInfo();
        }
        else { //lsita vuota lancia una eccezione
            throw(new NullPointerException("index out of bounds"));
        }
        return ris;
    }
    /**
     * Restituisce l'ultimo elemento della lista
     * @return contenuto dell'ultimo elemento della lista
     * @throws NullPointerException
     */
    public String getLast()throws NullPointerException{
        String ris="";
        /* */
        //verifica se la lista è vuota
        if (head!=null){ //lista non vuota: naviga
            nodo current=head;
            //cerca l'ultimo elemento
            while (current.getNext()!=null){
                current=current.getNext();
            }
            ris=current.getInfo();
        }
        else { //lista vuota: eccezione
            throw(new NullPointerException("index out of bounds"));
        }
        /* */
        /* Alternativa: usa un metodo privato di navigazione * /
        nodo n=findTail();
        ris=n.getInfo();
        / * */
        /* Altra alternativa: un'unica istruzione * /
        ris=findTail().getInfo();
        / * */
        return ris;

    }
    /**
     * Resituisce un elemento della lista che si trova nella posizione
     * indicata dal parametro pos
     * @param pos posizione dell'elemento
     * @return contenuto dell'elemento
     * @throws IndexOutOfBoundsException posizione fuori dei limiti
     * @throws NullPointerException lista vuota
     */
    public String get(int pos) throws IndexOutOfBoundsException,NullPointerException{
        String ris="";
        /* */
        //verifica se la lista è vuota
        if (head!=null){ //lista non vuota controlla posizione
            //verifica se posizione entro i limit
            if (pos<size()&&(pos>=0)){ //posizione entro i limiti
                //verifica se è una estrazione dalla testa
                if (pos==0) { //estrazione dalla testa
                    ris=getFirst();
                }
                else { //posizione intermedia: naviga
                    nodo current=head;
                    int i=0;
                    while ((i<pos)&&(current.getNext()!=null)){
                        current=current.getNext();
                        i++;
                    }
                    ris=current.getInfo();
                }
            }
            else { //pos fuori dei limiti
                throw(new IndexOutOfBoundsException());
            }
        }
        else { //lista vuota
            throw(new NullPointerException());
        }
        /* */
        /* Alternativa: usa il metodo privato di navigazione * /
        nodo n=findNodo(pos);
        ris=n.getInfo();
        / * */
        /* Alternativa: in un'unica istruzione * /
        ris=findNodo(pos).getInfo();
        / * */
        return ris;        
    }
    /**
     * verifica se un elemento è contenuto nella lista
     * In quest caso semplificato l'elemento in lista è un oggetto
     * di tipo String.
     * Essendo le String oggetti immutabili il confronto deve essere fatto
     * sul contenuto perchè un confronto tra riferimenti fallisce sempre
     * ma in generale quando un elemento è un oggetto non immutabile
     * il confronto si fa sui riferimenti.
     * Per individuare se l'elemento esiste si deve navigare nella lista
     * se la lista è vuota si restituisce subito false
     * @param s Elemento da verificare
     * @return vero se la lista contiene l'elemento falso altrimenti
     */
    public boolean contains(String s){
        boolean ris=false;
        nodo current=head;
        String s1="";
        //verifica se la lista è vuota
        if (head!=null) { //lista non vuota: naviga
            while((current!=null)&&(ris==false)){
                /* */
                s1=current.getInfo();
                if (s1.compareTo(s)==0) ris=true;
                /* */
                /* Alternativa * /
                if (current.getInfo().compareTo(s)==0) ris=true;
                / * */
                current=current.getNext();
            }
        }
        return ris;
    }
    /**
     * Effettua un ordinamento per selezione usando il metodo di comparazione
     * definito nel nodo
     * L'ordinamento per selezione prevede l'effettuazione di un ciclo su tutti
     * gli elementi della struttura fino al penultimo.
     * Ad ogni passo di questo ciclo viene effettuato un ulteriore ciclo
     * dall'elemento successivo al corrente fino all'ultimo e per ogni coppia
     * (corrente e successivo) viene effettuato il confronto. Se i due elementi non
     * sono ordinati viene effettuato lo scambio.
     */
    public void sort(){
        //percorre tutti i nodi dal primo al penultimo
        for (int i=0;i<size()-1;i++){
            //per ogni nodo percorre tutti i nodi dal successivo all'ultimo
            for(int j=i+1;j<size();j++){
                //estrae il nodo i-esimo e j-esimo
                nodo corrente=findNodo(i);
                nodo successivo=findNodo(j);
                //verifica se è richiesto un ordinamento
                if(corrente.compareTo(successivo)>0){ //corrente segue il successivo: scambia
                    String appoggio=corrente.getInfo();
                    corrente.setInfo(successivo.getInfo());
                    successivo.setInfo(appoggio);
                }
            }
        }
    }
    /**
     * Restituisce la dimensione della lista navigando sui nodi e contandoli
     * Se la lista è vuota restituice 0
     * @return numero di nodi
     */
    public int size(){
        int n=0;
        nodo current=head;
        //verifica se la lista è vuota
        if (head!=null){ //lista non vuota: conta
            n++;
            //naviga su tutta la lista incrementando
            while(current.getNext()!=null){
                current=current.getNext();
                n++;
            }
        }
        return n;
    }
/**
 * Scavalca il metodo toString fornendo una rappresentazione testuale
 * della lista
 * Naviga nella lista estraendo la rappresentazioen testuale di ciascun nodo
 * costruendo una stringa in cui ogni descrizione di nodo è separata
 * da un caporiga.
 * Se la lista è vuota restituisce una stringa vuota
 * @return rappresentazione testuale della lista
 */
    @Override
    public String toString(){
        String s="";
        nodo current=head;
        //verifica se la lista è vuota
        if (head!=null){
            s=current.toString();
            //naviga su tutti gli elementi
            while (current.getNext()!=null){
                current=current.getNext();
                s=s+current.toString();
             }
        }
        return s;
    }
    /**
     * Metodo privato per la ricerca della coda
     * Naviga dalla testa fino a trovare il riferimento all'ultimo elemento
     * ( elemento il cui riferimento punta a null)
     * @return riferimento all'ultimo elemento
     */
    protected nodo findTail(){
        //parte dalla testa
        nodo current=head;
        //naviga cercando il successivo fino a riferirsi all'ultimo elemento
        while (current.getNext()!=null){
             current=current.getNext();
        }
        //restituisce il riferimento all'ultimo elemento
        return current;
    }
    /**
     * Cerca nella lista il riferimento ad un nodo in posizione pos
     * Se la listan non è vuota e se il valore di pos
     * è entro i limiti della lista (0..dimensione attuale) restituisce il
     * riferimento all'elemento in posizione pos
     * altrimenti genera una eccezione
     * @param pos posizione del nodo richiesto
     * @return riferimento al nodo in posizione pos
     * @throws IndexOutOfBoundsException posizione fuori dei limiti
     * @throws NullPointerException lista vuota
     */
    protected nodo findNodo(int pos) throws IndexOutOfBoundsException,NullPointerException{
        //parte dalla testa
        nodo ris=head;
        //verifica se la lista è vuota
        if (ris!=null){ //lista non vuota verifica se entro i limiti (0..size)
            if (pos<size()&&(pos>=0)){ //posizione entro i limiti: cerca
                int i=0;
                //naviga fino alla posizone richiesta
                while ((i<pos)&&(ris.getNext()!=null)){
                    ris=ris.getNext();
                    i++;
                }
            }
            else { //posizione fuori dai limiti
                throw(new IndexOutOfBoundsException());
            }
        }
        else { //lista vuota: eccezione di puntatore a null
            throw(new NullPointerException());
        }
        return ris;
    }
}
