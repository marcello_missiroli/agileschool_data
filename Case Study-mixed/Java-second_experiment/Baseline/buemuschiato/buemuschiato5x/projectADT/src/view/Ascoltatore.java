/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import adt.listasemplice;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author studente
 */
public class Ascoltatore implements ActionListener{
     JTextField numDec;
    JTextField numBin;
    public Ascoltatore(JTextField numDec, JTextField numBin) {
        this.numDec=numDec;
        this.numBin=numBin;
    }
    
    @Override
    public void actionPerformed(ActionEvent event){
        int numero;
        int e = Integer.parseInt(numDec.getText());
        numero = e;
        listasemplice l = new listasemplice();
        int r;
        while(numero != 0){
            r = numero%2;
            numero = numero / 2;
            l.addHead(Integer.toString(r));
        }
        numBin.setText(l.toString());
}
}
