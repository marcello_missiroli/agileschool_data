/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.awt.Container;
import java.awt.FlowLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author Lorenzo
 */
public class Finestra extends JFrame{
    public JTextField numDec;
    public JTextField numBin;
    public JButton calc;
    Ascoltatore listener;
    
    public Finestra() {
            super("Prima finestra");
            Container c = this.getContentPane();
            c.setLayout(new FlowLayout());        
            this.setSize(400,400);
            this.setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE);
            JLabel decimale = new JLabel("Numero Decimale");
            JLabel binario = new JLabel("Numero Binario");
            JPanel nord = new JPanel();
            JPanel centro = new JPanel();
            JPanel sud = new JPanel();
            numDec = new JTextField("",15);
            numBin = new JTextField("",15);
            calc = new JButton("converti");
            c.add(nord);
            c.add(centro);
            c.add(sud);
            nord.add(decimale);
            nord.add(binario);
            centro.add(numDec);
            centro.add(numBin);
            listener=new Ascoltatore(numDec,numBin);
            calc.addActionListener(listener);
            sud.add(calc);
            this.setVisible(true);
        }    
}
