/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Carpanelli Finelli Fontana
 */
public class Auto {
    //dichiaro le caratteristiche che dovrò passare all'auto(in input)
    int lunghezza;
    String targa;
    
    //costruttore
    public Auto(int lunghezza, String targa) {
        this.lunghezza = lunghezza;
        this.targa = targa;
    }
    
   //metodi getter
    public int getLunghezza() {
        return lunghezza;
    }

    public String getTarga() {
        return targa;
    }
    
}
