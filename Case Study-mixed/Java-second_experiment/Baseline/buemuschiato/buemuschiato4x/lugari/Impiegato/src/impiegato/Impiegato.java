package impiegato;

/**
 *
 * @author lugari.alessandro
 */
public class Impiegato extends Persona{
    private int salario;
    public Impiegato(String n, String c, int e, int s) {
        super(n, c, e);
        salario = s;
    }
    public Impiegato() {
        super(" ", " ", 30);
        salario = 0;
    }
    public Impiegato(Impiegato i) {
        this.nome = i.nome;
        this.cognome = i.cognome;
        this.eta = i.eta;
    }
    public void sets(int s) {
        salario = s;
    }
    public int gets() {
        return salario;
    }
    public int aumentasalario(int percentuale) {
        salario = salario + ((salario*percentuale)/100);
        return salario;
    }
    public void dettagliImpiegato() {
        System.out.println("dettagli impiegato:");
        System.out.println(super.dettagli());
        System.out.println(" salario: " + salario);
    }
}
