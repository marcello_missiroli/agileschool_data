package impiegato;

/**
 *
 * @author lugari.alessandro
 */
public class Persona {
    protected String nome;
    protected String cognome;
    protected int eta;
    
    public Persona() {
        nome = " ";
        cognome = " ";
        eta = 30;
    }
    
    public Persona(String n, String c, int e) {
        nome = n;
        cognome = c;
        eta = e;
    }
    public void setN(String n) {
        nome = n;
    }
    public void setC(String c) {
        cognome = c;
    }
    public void setE(int e) {
        eta = e;
    }
    public String getN() {
        return nome;
    }
    public String getC() {
        return cognome;
    }
    public int getE() {
        return eta;
    }
    public String dettagli() {
        return " nome: " + nome + " cognome: " + cognome + " età: " + eta;
    }
}
