/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ereditarietàpolimorfismo;

public class Persona {
    protected String nome;
    protected String cognome;
    protected int età;
    
    public Persona(){
        nome = null;
        cognome = null;
        età = 0;
    }
    public Persona(String n, String c, int e){
        nome = n;
        cognome = c;
        età = e;
    }
    
    public void setN(String n){nome = n;}
    public void setC(String c){cognome = c;}
    public void setE(int e){età = e;}
    
    public String getN(){return nome;}
    public String getC(){return cognome;}
    public int getE(){return età;}
    
    public String dettagli(){
        return età + " " + cognome + " " + nome;
    }
    

}
