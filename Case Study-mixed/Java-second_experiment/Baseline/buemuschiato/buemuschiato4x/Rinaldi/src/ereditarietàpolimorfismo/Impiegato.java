/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ereditarietàpolimorfismo;

public class Impiegato extends Persona {
    protected int salario;
    
    public Impiegato(){
        nome = null;
        cognome = null;
        età = 0;
        salario = 0;
    }
    public Impiegato(String n, String c, int e,int s){
        nome = n;
        cognome = c;
        età = e;
        salario = s;
    }
    public void setS(int s){salario = s;}
    public int getS(){return salario;}
    
    public String dettagli(){
        return nome + " " + cognome + " " + età + " " + salario;
    }
    
    public int aumento(int p){
        return salario = salario + ((p / 100) * salario);
    }
    



    
}
