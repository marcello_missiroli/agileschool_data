/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ereditarietàpolimorfismo;

/**
 *
 * @author rinaldi.jaqueline
 */
public class TestImpiegato {
    
    public static void main(String [] args){
        Impiegato Alberto = new Impiegato();
        Alberto.setN("Alberto");
        Alberto.setC("Rossi");
        Alberto.setE(35);
        Alberto.setS(1500);
        
        
        System.out.println("I dettagli dell'impiegato sono: " + Alberto.dettagli() + " " + Alberto.aumento(10));
    }
    
}
