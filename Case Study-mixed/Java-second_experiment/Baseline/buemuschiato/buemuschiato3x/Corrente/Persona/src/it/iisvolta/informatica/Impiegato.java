/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package it.iisvolta.informatica;


public class Impiegato extends Persona {
    private float salario;
    
    public Impiegato(String n, String c, int e, int s)
    {
        nome = n;
        cognome = c;
        eta = e;
        salario = s;
    }
    
    public Impiegato()
    {
        nome = " ";
        cognome = " ";
        eta = 0;
        salario = 0;
    }
    
    public String dettagli()
    {
        String dettagli;
        dettagli = super.dettagli();
        dettagli = dettagli + "\nSalario: " + salario;
        System.out.println(dettagli);
        return dettagli;
    }
    
    public void aumentasalario()
    {
        float percentuale = 0;
        percentuale = salario * (20.0f / 100.0f);
        System.out.println("Percentuale: " + percentuale);
        salario = salario + percentuale;
    }
}
