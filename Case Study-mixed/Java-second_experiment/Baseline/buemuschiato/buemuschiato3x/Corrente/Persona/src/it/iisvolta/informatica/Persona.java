/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package it.iisvolta.informatica;


public class Persona {
    
    protected String nome;
    protected String cognome;
    protected int eta;
    
    public Persona(String n, String c, int e)
    {
        nome = n;
        cognome = c;
        eta = e;
    }
    
     public Persona()
     {
         nome = " ";
         cognome = " ";
         eta = 0;
     }
    
    public void setNome(String n){nome = n;}
    public void setCognome(String c){cognome = c;}
    public void setEta(int e){eta = e;}
    
    public String getNome() {return nome;}
    public String getCognome(){return cognome;}
    public int getEta(){return eta;}
   
    public String dettagli()
    {
        String dettagli = new String();
        dettagli = nome + " " + cognome + "\nEtà: " + eta;
        return dettagli;
    }
   
}


