/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package impiegati;

/**
 *
 * @author a.marra
 */
public class Impiegato {
    private int matricola;
    private String nominativo;
    private int anno_assunzione;
    private int livello_retributivo;
    static private int baseStipendio=800;
    public void setMatricola(int matricola)
    {
        this.matricola=matricola;
    }
    public void setNominativo(String nominativo)
    {
        this.nominativo=nominativo;
    }
    public void setAnnoAssunzione(int anno_assunzione)
    {
        this.anno_assunzione=anno_assunzione;
    }
    public void setLivelloretributivo(int livello_retributivo)
    {
        if(livello_retributivo>6)
            livello_retributivo=6;
        if (livello_retributivo<1)
            livello_retributivo=1;
        this.livello_retributivo=livello_retributivo;
    }
    private int getMatricola()
    {
        return matricola;
    }
    private String getNominativo()
    {
        return nominativo;
    }
    private int getAnno_assunzione()
    {
        return anno_assunzione;
    }
    private int getLivello_retributivo()
    {
        return livello_retributivo;
    }
    public Impiegato(int matricola,String nominativo,int anno_assunzione,int livello_retributivo)
    {
        setMatricola(matricola);
        setNominativo(nominativo);
        setAnnoAssunzione(anno_assunzione);
        setLivelloretributivo(livello_retributivo);
    }
    public int calcolaStipendio()
    {
        return baseStipendio+(50*(2015-anno_assunzione))+25*livello_retributivo;
    }
    @Override
    public String toString()
    {
        dio
    }
}
