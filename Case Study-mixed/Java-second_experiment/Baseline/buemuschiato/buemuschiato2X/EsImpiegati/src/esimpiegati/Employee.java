/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esimpiegati;

import java.util.*;

/**
 *
 * @author a.spadaro
 */
public class Employee {
    //MAIN CLASS ATTRIBUTES
    private int code;
    private String name;
    private int year;
    private int lvl;
    //STATIC ATTS
    private static double BASE = 800;
    private static double ADDER = 25;
    
    
    public Employee() //IMPIEGATO PREDEFINITO, ANDREMO A SOSTITUIRE TUTTI I PARAMETRI TRAMITE INPUT.
    {
        this.code = 0;
        this.name = "";
        this.year = 1900;
        this.lvl = 1; 
    } //DOVREBBE ESSERE 825 COME STIPENDIO MINIMO
    
    
    
    
    
    //GETTER//
    public int getCode() {return this.code;}
    public String getName() {return this.name;}
    public int getYear() {return this.year;}
    public int getLvl() {return this.lvl;}
    
    //SETTER//
    public void setCode(int code) {this.code = code;}
    public void setName(String name) {this.name = name;}
    public void setYear(int year) {this.year = year;}
    public void setLvl(int lvl) 
    {
        if(lvl >= 1 && lvl <= 6)
            this.lvl = lvl;
        else if(lvl < 1)
            this.lvl = 1;
        else if(lvl > 6)
            this.lvl = 6;      
        
        //BO SE TIPO HAI UN LIVELLO SUPERFIGIO LA BASE DI PARTENZA VIENE RADDOPPIATA, MA NON FA PARTE DELL'ESERCIZIO, ERA SOLO PER DIMOSTRARE CHE LO STATIC SI PUO' CAMBIARE
        /*
         * if(lvl>=6)
         * Employee.BASE *= 2; 
         */
        
    }
    
    private double getSalary() //RACCOGLIAMO IL VALORE DELLO STIPENDIO DA IMMETTERE IN OUTPUT
    {
        GregorianCalendar gc = new GregorianCalendar();      
        return 800+((gc.get(Calendar.YEAR)-this.year)*50+(lvl*25)); //CALCOLO IMMEDIATO DELLO STIPENDIO  
    }

    @Override
    public String toString() {
        return  "Employee{" + "code: " + code + ", name: " + name + ", year: " + year + ", lvl: " + lvl + ", salary: " + getSalary() + " }" ;
    }
    
    
    
    
    
    
    
}
