/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esimpiegati;
import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;
/**
 *
 * @author a.spadaro
 */
public class EsImpiegati {
    
    

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner in = new Scanner(System.in); 
        Employee scemo = new Employee(); //nuovo dipendente oggetto scemo
        
        
        
        System.out.print("Inserisci il n° di matricola: ");
        int code = in.nextInt(); //in input glielo passi alla variabile
        scemo.setCode(code);  //modifica l'attributo dell'oggetto
        //stesso con gli altri
        System.out.print("Inserisci il nominativo dell'impiegato: ");
        String name = in.next();
        scemo.setName(name);
        
        System.out.print("Inserisci l'anno di assunzione: ");
        int year = in.nextInt();
        scemo.setYear(year);
        
        System.out.print("Inserisci il livello retributivo: ");
        int lvl = in.nextInt();
        scemo.setLvl(lvl);
        
        System.out.println(scemo.toString());
        
        
    }
    
}
