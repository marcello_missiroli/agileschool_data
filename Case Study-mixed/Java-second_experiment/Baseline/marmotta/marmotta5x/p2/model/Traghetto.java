/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;
import adt.*;
/**
 *
 * @author Cavalieri-Cere'
 */
public class Traghetto<T> extends Pila{
    double LGarage=50.0;
    boolean pieno=false;
    

    public Traghetto() {
    }
    
    
    
    //metodo sale auto
    public void saleAuto (Auto a){       
        if(a.getLunghezza()<=LGarage){
        this.push(a); 
        LGarage=LGarage-a.getLunghezza();
        }
        else{
            this.pieno=true;
        }
    }

    
    
    public boolean isPieno() {
        return pieno;
    }
    
    public int isDouble(String s){
        try {
            Integer.parseInt(s);
            return 0;
        }
        catch (Exception e) {
            try {
                Double.parseDouble(s);
                return 1;
            }
            catch (Exception ev){
                return -1;
            }
        }
    }
    public boolean comparatore(double n){
        boolean x;
        if(n>=0){
            x=true;
        }
        else{
            x=false;
        }
         return x;
    }
 
}
