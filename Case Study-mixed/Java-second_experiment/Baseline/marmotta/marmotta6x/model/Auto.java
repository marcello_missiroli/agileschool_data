/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Cavalieri-Cere'
 */
public class Auto {
    String targa;
    Double lunghezza;

    public Auto(String targa, Double lunghezza) {
        this.targa = targa;
        this.lunghezza = lunghezza;
    }

    public Double getLunghezza() {
        return lunghezza;
    }

    public String getTarga() {
        return targa;
    }

}
