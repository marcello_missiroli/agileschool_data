/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import adt.*;

/**
 *
 * @author Studente
 */
public class Ambulatorio <T> extends Coda{
    public Ambulatorio() {
        super();
    }
    public void entraPaz(T a){
        this.enQueue(a);    
    }
    public boolean esciPaz(){
        boolean ok;
        try{
        this.deQueue();
            ok=true;
        } 
        catch(Exception e){
            ok=false; 
        }
        return ok;
    }

    public T pazFronte(){
       T a;
       a=(T)this.getInfoNodo();
      return a;
      
    }
    
    
}
