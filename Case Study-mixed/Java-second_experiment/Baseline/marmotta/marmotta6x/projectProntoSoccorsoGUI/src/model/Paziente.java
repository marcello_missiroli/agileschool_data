/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Studente
 */
public class Paziente {
    String nomeCognome;
    String codice;

    public Paziente(String nomeCognome, String codice) {
        this.nomeCognome = nomeCognome;      
        this.codice = codice;
    }

    public String getCodice() {
        return codice;
    }

    public String getNomeCognome() {
        return nomeCognome;
    }
    
}
