/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pjimpiegato;

/**
 *
 * @author a.mercuriali
 */
public class Impegatone {
    private int matricola;
    private int anno_assunzione;
    private String nominativo;
    private int livello_retributivo;

    public Impegatone() 
    {
        matricola = 0;
        anno_assunzione = 0;
        nominativo = " ";
        livello_retributivo = 0;
    }

    public Impegatone(int matricola, int anno_assunzione, String nominativo) 
    {
        this.matricola = matricola;
        this.anno_assunzione = anno_assunzione;
        this.nominativo = nominativo;
    }

    public int getMatricola() {
        return matricola;
    }

    public void setMatricola(int matricola) {
        this.matricola = matricola;
    }

    public int getAnno_assunzione() {
        return anno_assunzione;
    }

    public void setAnno_assunzione(int anno_assunzione) {
        this.anno_assunzione = anno_assunzione;
    }

    public String getNominativo() {
        return nominativo;
    }

    public void setNominativo(String nominativo) {
        this.nominativo = nominativo;
    }

    public int getLivello_retributivo() {
        return livello_retributivo;
    }

    public void setLivello_retributivo(int livello_retributivo) {
        this.livello_retributivo = livello_retributivo;
    }
    
    
    
    
}
