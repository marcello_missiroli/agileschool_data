/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package impiegato.maiello.ficociello;

import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 *
 * @author p.maiello
 */
public class Impiegato {
    private int matricola;
    private String nominativo;
    private int anno_assunzione;
    private int livello_retributivo;
    private int stipendio;
    private int anzianità;
    
    
    public Impiegato() {
        matricola=0;
        nominativo="";
        anno_assunzione=0;
        livello_retributivo=0;
    }
    public Impiegato(int matricola, String nominativo, int anno_assunzione, int livello_retributivo) {
        this.matricola = matricola;
        this.nominativo = nominativo;
        this.anno_assunzione = anno_assunzione;
        this.livello_retributivo = livello_retributivo;
    }
    public Impiegato(Impiegato imp) {
        this.matricola = imp.matricola;
        this.nominativo = imp.nominativo;
        this.anno_assunzione = imp.anno_assunzione;
        this.livello_retributivo = imp.livello_retributivo;
    }

    public int getMatricola() {
        return matricola;
    }

    public String getNominativo() {
        return nominativo;
    }

    public int getAnno_assunzione() {
        return anno_assunzione;
    }

    public int getLivello_retributivo() {
        return livello_retributivo;
    }

    public void setMatricola(int matricola) {
        this.matricola = matricola;
    }

    public void setNominativo(String nominativo) {
        this.nominativo = nominativo;
    }

    public void setAnno_assunzione(int anno_assunzione) {
        this.anno_assunzione = anno_assunzione;
    }

    public void setLivello_retributivo(int livello_retributivo) {
        this.livello_retributivo = livello_retributivo;
    }
    
    public int calcolaAnzianità(){
        GregorianCalendar gc = new GregorianCalendar();
        int anno = gc.get(Calendar.YEAR);
        
        return anzianità=anno-anno_assunzione;
    }
    public int calcolastipendio(){
        
        return  stipendio=anzianità*50+livello_retributivo*25+800;
    }
    
    
    
}
