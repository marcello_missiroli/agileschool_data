package adt;
/**
 * Esempio semplificato di Nodo per lista semplice concatenata
 formata da nodi legati tra loro da un riferimento
 Contiene solo una stringa
 Implementa l'interfaccia Comparable anche se nel caso specifico
 non sarebbe indispensabili
 * @author peroni
 */
public class Nodo implements Comparable{
    /**
     * Rappresenta una versione semplificate dell'elemento contenuto nella lista
     */
    private String info;
    /**
     * Riferimento all'elemento successivo
     */
    private Nodo next;
    /**
     * Costruttore di default inizializza a null sia l'informazione che il
     * puntatore all'elemento successivo
     */
    public Nodo(){
        info="";
        next=null;
    }
    /**
     * Costruttore parametrico inizializza l'informazione e il riferimento all'
     * elemento successivo in base ai parametri ricevuti in ingresso
     * @param info Rappresentazione semplificata dell'informazione
     * @param next riferimento all'elemento successivo (null per l'ultimo)
     */
    public Nodo(String info,Nodo next){
        this.info=info;
        this.next=next;
    }
    /**
     * Restituisce l'elemento di informazione contenuto nel Nodo
     * @return informazione contenuta nel Nodo
     */
    public String getInfo(){
        return info;
    }
    /**
     * Restituisce il riferimento al Nodo successivo
     * @return riferimento al Nodo successivo (null se è l'ultimo)
     */
    public Nodo getNext(){
        return next;
    }
    /**
     * Modifica l'informazione in base al parametro di ingresso
     * @param info informazione da inserire nel Nodo
     */
    public void setInfo(String info){
        this.info=info;
    }
    /**
     * Modifica il riferimento al Nodo successivo in base al parametro di ingresso
     * @param next riferimento al Nodo successivo
     */
    public void setNext(Nodo next){
        this.next=next;
    }
    /**
     * scavalca il metodo di default restituendo una rappresentazione
 testuale del Nodo
     * @return rappresentazione testuale del Nodo
     */
    @Override
    public String toString(){
        return info;
    }
    /**
     * Implementa il metodo compareTo dell'interfaccia Comparable
 confronta due nodi secondo un criterio specifico relativo
 all'informazione contenuta
 Nel caso semplificato il confronto è banale perchè il Nodo contiene
 solo una stringa
     * @param o riferimento all'oggetto da confrontare
     * @return 1 se questo oggetto è maggiore dell'oggetto da confrontare
     * 0 se sono uguali, -1 se l'oggetto è minore
     */
    public int compareTo(Object o) {
        int ris=0;
        Nodo n=(Nodo) o;
        ris=this.getInfo().compareTo(n.getInfo());
        return ris;
    }
}
