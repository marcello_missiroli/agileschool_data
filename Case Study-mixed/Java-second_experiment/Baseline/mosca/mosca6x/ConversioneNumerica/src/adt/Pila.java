/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package adt;

/**
 *
 *   Esempio semplificato di pila, realizzata come lista semplice
 formata da nodi legati tra loro da un riferimento
 Il Nodo ha una struttura fissa e contiene solo una stringa
 * @author Cenacchi
 */
//implementazione pila con lista (push si aggiunge un elemento in testa, pop si elimina la testa)
public class Pila {
    private Nodo ultimo;
    public Pila(){
        //inizializzo la pila, il primo Nodo è null
        ultimo=null;

    }
    //metodi della pila
    public boolean isEmpty(){
        return ultimo==null;
    }
    public void push(String info){
        Nodo n=new Nodo();
        n.setInfo(info);
        n.setNext(ultimo);
        ultimo=n;
    }
    public void pop() throws Exception{
        //assert isEmpty() : "pop su pila vuota";
        if (this.isEmpty()) throw new Exception ("impossibile effettuare il pop perchè la pila è vuota");
        ultimo=ultimo.getNext();



    }
    public String top(){
        return ultimo.getInfo();
    }
}
