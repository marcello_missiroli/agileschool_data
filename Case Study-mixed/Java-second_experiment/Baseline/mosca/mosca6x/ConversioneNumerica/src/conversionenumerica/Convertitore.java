/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conversionenumerica;

import adt.*;
/**
 *
 * @author Studente
 */
public class Convertitore {
    int decimale;
    Pila p;

    public Convertitore(int decimale) {
        this.decimale = decimale;
    }
    public int conversione(){
        int risultato;
        int quoziente;
        int resto;
        while(decimale!=0){
            quoziente=decimale/2;
            resto=decimale%2;
            p.push(Integer.toString(resto));
            decimale=quoziente;            
        }
        risultato=decimale;
        return risultato;
    }
}
