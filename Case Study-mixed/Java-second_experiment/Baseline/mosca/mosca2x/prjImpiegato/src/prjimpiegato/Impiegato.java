/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package prjimpiegato;

import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 *
 * @author m.bellegati
 */
public class Impiegato {
    private int matricola;
    private String nominativo;
    private int anno_assunzione;
    private int livello_retributivo;
    private static int baseSipendio=800;
    
    public Impiegato(int matricola,String nominativo,int anno_assunzione,int livello_retributivo)
    {
        setMatricola(matricola);
        setNominativo(nominativo);
        setAnno_assunzione(anno_assunzione);
        setLivello_retributivo(livello_retributivo);
    }
    private void setMatricola(int m)
    {
        matricola=m;
    }
    private void setNominativo(String n)
    {
        nominativo=n;
    }
    private void setAnno_assunzione(int a)
    {
        anno_assunzione=a;
    }
    private void setLivello_retributivo(int l)
    {
        
          livello_retributivo=l;
    }
    public int getMatricola()
    {
        return matricola;
    }
    public String getNominativo()
    {
        return nominativo;
    }
    public int getAnno_assunzione()
    {
        return anno_assunzione;
    }
    public int getLivello_retributivo()
    {
        return livello_retributivo;
    }
    @Override
    public String toString()
    {
        return "l'impiegato n "+matricola+" chiamato "+nominativo+" ,ha iniziato a lavorare nella nostra PRESTIGIOSA azienda dall'anno "+anno_assunzione+" con un livello retributivo di: "+livello_retributivo;
    }
    public int Stipendio()
    {
        GregorianCalendar gc = new GregorianCalendar();
        int anno = gc.get(Calendar.YEAR);
        int calcolo=anno-anno_assunzione;
        int aggiunta=calcolo*50;
        int variabile=0;
        if(livello_retributivo<=0)
        {
            variabile=25;
        }
        if(livello_retributivo==1)
        {
            variabile=25;
        }
        if(livello_retributivo==2)
        {
            variabile=50;
        }
        if(livello_retributivo==3)
        {
            variabile=75;
        }
        if(livello_retributivo==4)
        {
            variabile=100;
        }
        if(livello_retributivo==5)
        {
            variabile=125;
        }
        if(livello_retributivo==6)
        {
            variabile=150;
        }
        if(livello_retributivo>6)
        {
            variabile=150;
        }
        return baseSipendio+variabile+aggiunta;
    }
    
    
    
 
            
            
            
            
            
            
            
            
            
            
            
            
            
}
