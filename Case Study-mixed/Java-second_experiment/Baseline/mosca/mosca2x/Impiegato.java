/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mainimpiegati;

/**
 *
 * @author paolo
 */
public class Impiegato
{
    public int Matricola;
    public String Nominativo;
    public int Anno_assunzione;
    public int Livello_retributivo;
    public static int Base=800;
    
    public Impiegato()
    {
        this.Matricola=0;
        this.Nominativo="";
        this.Anno_assunzione=0;
        this.Livello_retributivo=0;
    }
    
    public Impiegato(int Matricola, String Nominativo, int Anno_assunzione, int Livello_retributivo)
    {
        this.Matricola=Matricola;
        this.Nominativo=Nominativo;
        this.Anno_assunzione=Anno_assunzione;
        this.Livello_retributivo=Livello_retributivo;
    }

    public int getMatricola() 
    {
        return Matricola;
    }

    public void setMatricola(int Matricola) 
    {
        this.Matricola = Matricola;
    }

    public String getNominativo() 
    {
        return Nominativo;
    }

    public void setNominativo(String Nominativo) 
    {
        this.Nominativo = Nominativo;
    }

    public int getAnno_assunzione() 
    {
        return Anno_assunzione;
    }

    public void setAnno_assunzione(int Anno_assunzione) 
    {
        this.Anno_assunzione = Anno_assunzione;
    }

    public int getLivello_retributivo() 
    {
        return Livello_retributivo;
    }

    public void setLivello_retributivo(int Livello_retributivo) 
    {
        this.Livello_retributivo = Livello_retributivo;
    }

    public static void setBase(int Base) 
    {
        Impiegato.Base = Base;
    }
    
    public Impiegato Stipendio(Impiegato a, Impiegato l)
    {
        int xx = (50* a.getAnno_assunzione());
        int yy= (25*l.getLivello_retributivo());
        int b=Base+xx+yy;
        return b;
    }
    
    public String toString()
    {
        String s = "("+this.getMatricola()+";"+this.getNominativo()+";"+this.getAnno_assunzione()+";"+this.getLivello_retributivo()+")";
        return s;
    }
}


