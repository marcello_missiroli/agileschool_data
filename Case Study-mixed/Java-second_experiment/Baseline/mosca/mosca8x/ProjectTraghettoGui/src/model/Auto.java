/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

/**
 *
 * @author Miscio Francesco, Elvotti Federico
 */
public class Auto {
    String marca;
    
    int lunghAuto;

    public Auto(String marca, int lunghAuto) {
        this.marca = marca;
        this.lunghAuto = lunghAuto;
    }

    public String getTarga() {
        return marca;
    }

    public int getLunghAuto() {
        return lunghAuto;
    }

  
    
    
    
}
