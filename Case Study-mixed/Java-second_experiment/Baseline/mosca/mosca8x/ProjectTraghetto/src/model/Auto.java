/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

/**
 *
 * @author Miscio Francesco, Elvotti Federico
 */
public class Auto {
    String targa;
    
    int lunghAuto;

    public Auto(String targa, int lunghAuto) {
        this.targa = targa;
        this.lunghAuto = lunghAuto;
    }

    public String getTarga() {
        return targa;
    }

    public int getLunghAuto() {
        return lunghAuto;
    }

  
    
    
    
}
