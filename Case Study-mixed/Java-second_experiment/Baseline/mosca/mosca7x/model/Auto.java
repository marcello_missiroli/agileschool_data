/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Studente
 */
public class Auto {
    //attributi
    String targa;
    int lunghezzaMac;
   
    //costruttore
    public Auto(String targa, int lunghezzaMac) {
        this.targa = targa;
        this.lunghezzaMac = lunghezzaMac;
    }
    //metodo che restituisce la lunghezza della macchina
    public int getLunghezzaMac() {
        return lunghezzaMac;
    }
    //metodo che restituisce la targa della macchina
    public String getTarga() {
        return targa;
    }
}
