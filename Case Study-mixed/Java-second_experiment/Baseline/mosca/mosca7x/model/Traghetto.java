/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import adt.*;

/**
 *
 * @author Studente
 */
public class Traghetto{
    //attributi
    int lunghGar=50;
    Pila<Auto> p;
    int spazioDisp=lunghGar;
    boolean semaforo;
   
    //costruttore
    public Traghetto() {
    p = new Pila(); 
    semaforo= true;
    }
    
    //metodo per inserire un auto secondo il criterio dello spazio disponibile nel traghetto
    public void InsAuto(Auto a) {
        if(spazioDisp>=a.getLunghezzaMac()){
            spazioDisp=spazioDisp-a.getLunghezzaMac();
        p.push(a);
        }
        else{
            semaforo=false;
        }
    }
    //metodo per leggere un'auto presente nella pila(traghetto)
    public Auto leggiAuto(){
        Auto a = p.top();
        return a;
    }
    //metodo per estrarre un auto dalla pila(traghetto)
    public String estraiAuto(){
        String descrizione="";
        try{
        p.pop();
        }
        catch(Exception e){
            descrizione =e.getMessage();
        }
        return descrizione;
    }
    //metodo che restituisce lo stato del semaforo
    public boolean isSemaforo() {
        return semaforo;
    }
    //metodo che restituisce se lo stato della pila è vuota
    public boolean isEmpty(){
        return p.isEmpty();
    }
    }
   
    
