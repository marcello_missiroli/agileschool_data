package impiegato;

/**
 *
 * @author zambelli.marco
 */
public class Impiegato extends Persona{
    private int salario;
    public Impiegato(String n, String c, int e, int s) {
        super(n, c, e);
        salario = s;
    }
    
    public Impiegato() {
        super(" ", " ", 60);
        salario = 0;
    }
    
    public void setS(int s) {
        salario= s;
    }
    public int getS() {
        return salario;
    }
    public int aumentasal(int percentuale) {
        salario = salario + ((salario*percentuale)/100);
        return salario;
    }
    public void dettagliImpiegato() {
        System.out.println("dettagli:");
        System.out.println(super.dettagli());
        System.out.println(" salario: " + salario);
    }
}
