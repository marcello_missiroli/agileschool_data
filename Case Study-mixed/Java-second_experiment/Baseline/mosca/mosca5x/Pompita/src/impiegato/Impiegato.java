package impiegato;
public class Impiegato extends Persona {
    private int salario;
      public Impiegato() {
        super("","",30);
        salario = 0;
    }
    public Impiegato(String n, String c, int e, int s) {
        super(n, c, e);
        salario = s;
    }
    public void setS(int s){
        salario = s;
    }
    public int getS(){
        return salario;
    }
    public int aumentasalario (int percentuale) {
        salario = salario + (salario*percentuale)/100;
        return salario;
    }
    public void dettagli(){
            System.out.println("dettagli persona: ");
            System.out.println();
            System.out.println("nome: " + nome + "cognome: " + cognome + "età: " + eta);
            
        }
    }