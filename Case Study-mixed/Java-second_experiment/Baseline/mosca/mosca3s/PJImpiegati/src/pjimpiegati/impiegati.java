/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pjimpiegati;
import java.util.*;
/**
 *
 * @author s.gualtieri
 */
public class impiegati {
    
    private int matricola;
    private String nominativo;
    private int anno;
    private int livello;
    private final int base=800;
    private static int anzianita=50;
    private static int retribuito=25;
    
    public impiegati(int matricola, String nominativo, int anno, int livello)
    {
        this.matricola=matricola;
        this.nominativo=nominativo;
        this.anno=anno;
        this.livello=livello;
    }
    
    public impiegati()
    {
        matricola=0;
        nominativo=" ";
        anno=0;
        livello=0;
    }
    
    public void setAnno(int anno)
    {
        this.anno=anno;
    }
    
    public void setLivello(int livello)
    {
        this.livello=livello;
    }
    
    public int getMatricola()
    {
        return matricola;
    }
    
    public String getNominativo()
    {
        return nominativo;
    }
    
    public int getAnno()
    {
        return anno;
    }
    
    public int getLivello()
    {
        return livello;
    }
    
    public static void setRetribuito(int livello)
    {
        if(livello>1 && livello<6)
        {
            livello=retribuito;
        }
        else if(livello>6)
        {
            livello=6;
        }
        else if(livello<1)
        {
            livello=1;
        }
    }
    
    public static void setAnzianita(int anni)
    {
        anni=anzianita;
    }
    
    public int calcoloAnno()
    {
        GregorianCalendar gc = new GregorianCalendar();
        int oggi = gc.get(Calendar.YEAR);
        return oggi-anno;
    }
    
    public int stipendio()
    {
        return base + (anzianita * calcoloAnno()) + (retribuito * livello);
    }
    
    @Override
    public String toString()
    {
        return "impiegato:" + nominativo + "matricola: " + matricola + "anno di assunzione: " + anno + "livello retributivo: " + livello + "stipendio: " + stipendio();
    }
}
