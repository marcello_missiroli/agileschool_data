/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pjdistributore;

import java.util.Objects;

/**
 *
 * @author s.serpetti
 */
public class Snack {
    private int codice;
    private String nome;
    private String marca;

 public Snack() {
        this.codice = codice;
        this.nome = nome;
        this.marca = marca;
    }

    public Snack(int codice, String nome, String marca) {
        this.codice = 0;
        this.nome = "";
        this.marca = "";
    }

    public int getCodice() {
        return codice;
    }

    public void setCodice(int codice) {
        this.codice = codice;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    @Override
    public String toString() {
        return "Snack{" + "codice=" + codice + ", nome=" + nome + ", marca=" + marca + '}';
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Snack other = (Snack) obj;
        if (this.codice != other.codice) {
            return false;
        }
        if (!Objects.equals(this.nome, other.nome)) {
            return false;
        }
        if (!Objects.equals(this.marca, other.marca)) {
            return false;
        }
        return true;
    }
  
}
