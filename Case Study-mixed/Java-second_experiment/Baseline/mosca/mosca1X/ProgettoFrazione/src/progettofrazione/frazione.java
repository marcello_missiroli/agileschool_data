/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package progettofrazione;

/**
 *
 * @author m.digani
 */
public class frazione {
    private int num;
    private int den;
    
    public frazione()
    {
       this.num = 0;
       this.den = 1;
    }
       
    public frazione (int num , int den)
    {
        this.num = num;
        if(den==0)
        this.den=1;
        else
        this.den = den;
    }
    
    public frazione(frazione F){
        this.num=F.getNum();
        this.den=F.getDen();
    }

    public int getNum() {
        return num;
    }

    public int getDen() {
        return den;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public void setDen(int den) {
        this.den = den;
    }
     
    @Override
     public String toString()
             {
                 
                 return this.num+"/"+this.den;
             }
     
     public frazione somma(frazione f)
    {
        frazione r;
        int a=den*f.den;
        int b=num*f.den+ f.num*den;

        r = new frazione(b, a);
        r.semplificazione();
        return r;
    }
     
     public frazione sottrazione(frazione f)
    {
        frazione r;
        int a=den*f.den;
        int b=num*f.den- f.num*den;

        r = new frazione(b, a);
        r.semplificazione();
        return r;
    }
     
     public frazione divisione(frazione f)
    {
        frazione r;
        int a=den*f.num;
        int b=num*f.den;

        r = new frazione(b, a);
        r.semplificazione();
        return r;
    }
     
     public frazione moltiplicazione(frazione f)
    {
        frazione r;
        int a=den*f.den;
        int b=num*f.num;

        r = new frazione(b, a);
        r.semplificazione();
        return r;
    }

    private int Euclide(int a, int b)
    {
        int r;
        r = a % b;           
        while(r != 0)         
        {
            a = b;
            b = r;
            r = a % b;
        }
        return b;
    }
     
     private void semplificazione()
     {
         int s;
         s= Euclide(num,den);
         num=num/s;
         den=den/s;
     }
    
}
