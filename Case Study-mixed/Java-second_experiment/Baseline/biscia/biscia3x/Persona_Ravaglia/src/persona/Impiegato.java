/*
 * 
 */
package persona;

/**
 *
 * @author ravaglia.alessandro
 */
public class Impiegato extends Persona{
    private float salario;

    /**
     *
     * @return
     */
    public String dettagli1() {
            String re;
            re = this.getNome() + "  " + this.getCognome() + "  " + this.getAnni() + "  " + salario;
            return re;
        }

    /**
     *
     * @param p
     */
    public void aumentaSalario(float p) {
        salario += (salario * p) / 10;
    }

    /**
     *
     * @param a
     */
    public void setSalario(float a) {
        salario = a;
    }
}
