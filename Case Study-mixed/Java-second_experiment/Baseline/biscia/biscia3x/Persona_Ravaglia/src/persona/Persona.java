/*
 * 
 */
package persona;

/**
 *
 * @author ravaglia.alessandro
 */
public class Persona {

    /**
     *
     */
    protected String nome;

    /**
     *
     */
    protected String cognome;

    /**
     *
     */
    protected int anni;

    /**
     *
     * @param nome
     * @param cognome
     * @param anni
     */
    public Persona(String nome, String cognome, int anni) {
            this.nome = nome;
            this.cognome = cognome;
            this.anni = anni;
        }

    /**
     *
     */
    public Persona() {
            nome = "";
            cognome = "";
            anni = 0; 
        }

    /**
     *
     * @return
     */
    public String getNome() {
            return nome;
        }

    /**
     *
     * @return
     */
    public String getCognome() {
            return cognome;
        }

    /**
     *
     * @return
     */
    public int getAnni() {
            return anni;
        }

    /**
     *
     * @return
     */
    public String dettagli() {
            return nome + "  " + cognome + "  " + anni;
        }
}
