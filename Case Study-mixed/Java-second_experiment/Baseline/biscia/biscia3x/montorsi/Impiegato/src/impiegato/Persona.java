package impiegato;

public class Persona {
    protected String nome;
    protected String cognome;
    protected int età;
    protected Persona (String n, String c, int e){
        nome = n;
        cognome = c;
        età = e;
    }
    protected Persona(){
        nome = " ";
        cognome = " ";
        età = 20;
    }
    public void SetNome(String n) {
        nome = n;
    }
    public void SetCognome(String c){
        cognome = c;
    }
    
    public void SetEtà(int e){
        età = e;
    }
    public String GetNome(){
        return nome;
    }
    public String GetCognome(){
        return cognome;
    }
    public int GetEtà(){
        return età;
    }
    public void Dettagli(){
        System.out.println("Nome: " + nome);
        System.out.println("Cognome: " + cognome);
        System.out.println("Età: " + età);
    }
}
