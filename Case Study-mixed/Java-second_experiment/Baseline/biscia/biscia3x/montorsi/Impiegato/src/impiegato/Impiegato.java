package impiegato;

public class Impiegato extends Persona{
    private int salario;
    public Impiegato(String n, String c, int e, int s) {
        super(n, c, e);
        salario = s;
    }
    public Impiegato(){
        super(" ", " ", 20);
        salario = 0; 
    }
    public void SetSalario(int s){
        salario = s;
    }
    public int GetSalario(){
        return salario;
    }
    public int AumentaSalario(int s, int p){
        System.out.print("Aumento salario: ");
        System.out.println(salario = s + (s * p / 100));
        return 0;
    } 
    public void Dettagli(){
        System.out.println("Nome: " + nome);
        System.out.println("Cognome: " + cognome);
        System.out.println("Età: " + età);
        System.out.println("Salario: " + salario);
    }
}