/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package frazione;

/**
 *
 * @author l.camara
 */
public class frazion {
    private int num;
    private int den;
    boolean valida=true;    
    
    public frazion()
    {
        this.num=0;
        this.den=1;
    }
    
    public frazion (int num, int den)
    {
        this.num=num;
       if(this.den==0)
       {
           valida=true;
       }
       else
       {
           valida=false;
       }
        this.den=den;
        
    }
    public frazion( int b)
    {
        num=b;
        den=1;
    }
    public frazion( frazion f)
    {
        this.num=f.num;
        this.den=f.den;            
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public int getDen() {
        return den;
    }

    public void setDen(int den) {
        this.den = den;
    }
    public frazion somma(frazion f){
    
        frazion r;
        int a=den*f.den;
        int b=num*f.den+ f.num*den;
        r = new frazion(b, a);
        r.semplificazione();
        return r;
    }
    public frazion sottrazione(frazion f){
    
        frazion r;
        int a=den*f.den;
        int b=num*f.den - f.num*den;
        r = new frazion(b, a);
        r.semplificazione();
        return r;
    }
    public frazion moltiplicazione(frazion f){
    
        frazion r;
        int a=den*f.den;
        int b=num*f.num; 
        r = new frazion(b, a);
        r.semplificazione();
        return r;
    }
    public frazion divisione(frazion f){
    
        frazion r;
        int a=den*f.num;
        int b=num*f.den;
        r = new frazion(b, a);
        r.semplificazione();
        return r;
    }
    private int Euclide(int a, int b)  //il massimo comune divisore
   {
        int r;
        r = a % b;            
      while(r != 0)          
       {
          a = b;
          b = r;
          r = a % b;
       }
         return b;
    }
     private void semplificazione(){
         int mcd=Euclide(num,den); //mcd con euclide
         num/=mcd;
         den/=mcd;
     
      }
     
    @Override
     public String toString ()
     {
         return "" +num+ "/"+den;
     }
     public boolean equals (frazion t){
         if(t.getNum() == t.getDen())
         {
             return true;
         }
         else
         {
             return false;
         }
     }
         
    
}

