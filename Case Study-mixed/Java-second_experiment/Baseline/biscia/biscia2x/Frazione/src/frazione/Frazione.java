/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package frazione;

import java.util.Scanner;

/**
 *
 * @author l.camara
 */
public class Frazione {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner in = new Scanner(System.in);
        frazion f1,f2;
        int n1,d1,n2,d2;
        System.out.println("Inserisci il numeratore:");
        n1=in.nextInt();
        System.out.println("Inserisci il denominatore:");
        d1=in.nextInt();
        f1 = new frazion(n1,d1);
        System.out.println(f1.toString());
        System.out.println("Inserisci il numeratore:");
        n2=in.nextInt();
        System.out.println("Inserisci il denominatore:");
        d2=in.nextInt();
        f2 = new frazion(n2,d2);
        System.out.println(f2.toString());
        System.out.println("ecco la somma tra 2 frazione:\t"+f1.somma(f2));
        System.out.println("ecco la sottrazione tra 2 frazione:\t"+f1.sottrazione(f2));
        System.out.println("ecco la moltiplicazione tra 2 frazione:\t"+f1.moltiplicazione(f2));
        System.out.println("ecco la divisione tra 2 frazione:\t"+f1.divisione(f2));
        
        
    }
    
}
