 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esercizio_frazioni_scialfa_monari;

/**
 *
 * @author i.scialfa
 */
public class Frazione {
 
private int numeratore;
private int denominatore;
   
public Frazione (int numeratore,int denominatore)
{
    setnumeratore( numeratore);
    setdenominatore(denominatore);
    semplifica();
}
public Frazione (int numeratore)
{
    setnumeratore(numeratore);
    setdenominatore(1);
    
}
public Frazione (Frazione F)
{
    setnumeratore( F.getnumeratore());
    setdenominatore(F.getdenominatore());
    semplifica();
}

private void setnumeratore (int numeratore)
{
    this.numeratore=numeratore;
}
private void setdenominatore(int denominatore)
{
    if (denominatore!=0)
    {
        this.denominatore=denominatore;
    }
    else
    {
        this.denominatore=1;
    }
}
public int getnumeratore()
{
    return this.numeratore;
}
public int getdenominatore()
{
    return this.denominatore;
}
private int mcd(int a, int b)
{
    int resto;
    resto=a%b;
    while(resto!=0)
    {
        a=b;
        b=resto;
        resto=a%b;
    }
    return b;
}
private void semplifica()
{
    int mc;
    mc=mcd(this.numeratore,this.denominatore);
    setnumeratore(this.numeratore/mc);
    setdenominatore(this.denominatore/mc);
}
public Frazione somma(Frazione F)
{
    Frazione s;
    s= new Frazione((F.getnumeratore()*this.denominatore)+(this.numeratore*F.getdenominatore()),F.getdenominatore()*this.getdenominatore());
    return s;
}
public Frazione sottrazione(Frazione F)
{
    Frazione s;
    s= new Frazione((F.getnumeratore()*this.denominatore)-(this.numeratore*F.getdenominatore()),F.getdenominatore()*this.getdenominatore());
    return s;
}
public Frazione moltiplicazione(Frazione F)
{
    Frazione s;
    s= new Frazione((F.getnumeratore()*this.denominatore)*(this.numeratore*F.getdenominatore()),F.getdenominatore()*this.getdenominatore());
    return s;
}
public Frazione divisione(Frazione F)
{
    Frazione s;
    s= new Frazione((F.getnumeratore()*this.denominatore)/(this.numeratore*F.getdenominatore()),F.getdenominatore()*this.getdenominatore());
    return s;
}
}
