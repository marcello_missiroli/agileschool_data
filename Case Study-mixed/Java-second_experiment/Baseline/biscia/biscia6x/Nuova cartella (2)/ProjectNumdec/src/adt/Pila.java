/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package adt;

/**
 *
 * @author Dal Pesco Riccardo, Donati Nicolò, Martinelli Lorenzo
 */

//implementazione pila con lista (push si aggiunge un elemento in testa, pop si elimina la testa)
public class Pila {
    private nodo ultimo;
    public Pila(){
        //inizializzo la pila, il primo nodo è null
        ultimo=null;

    }
    //metodi della pila
    public boolean isEmpty(){
        return ultimo==null;
    }
    public void push(int info){
        nodo n=new nodo();
        n.setInfo(info);
        n.setNext(ultimo);
        ultimo=n;
    }
    public void pop() throws Exception{
        //assert isEmpty() : "pop su pila vuota";
        if (this.isEmpty()) throw new Exception ("la pila è vuota");
        ultimo=ultimo.getNext();



    }
    public int top(){
        return ultimo.getInfo();
    }
}
