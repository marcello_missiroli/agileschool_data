/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import adt.Pila;
import java.util.*;


/**
 *
 * @author Dal Pesco Riccardo, Donati Nicolò, Martinelli Lorenzo
 */
public class NumeroDecimale {

    public NumeroDecimale() {
    }
    
    
    public String numeroBinario(int numero){
        int resto = 0;
        String numeroBinario="";
        int valoreBinario;
        Pila p=new Pila();
        while(numero!=0){
            resto=numero%2;
            numero=numero/2;
            p.push(resto);
        }      
        try{
            while (!p.isEmpty()){
                valoreBinario=p.top();
                p.pop();
                numeroBinario=numeroBinario+Integer.toString(valoreBinario);        
            }
        }        
        catch(Exception e){
            System.out.println("Error "+e.getMessage());
        }
        return numeroBinario;
    }    


}
