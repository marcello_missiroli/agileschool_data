<?php
	function controllaCredenziali ($utente="", $pass="", $ar=Array()) {
		if (!$utente || !$pass) return false;
		if (count($ar) == 0) return false;
		if ($ar["username"] !== $utente) return false;
		if ($ar["password"] !== $pass) return false;
		return true;
	}
	
	function stampaPost ($ar=Array()) {
		if (count($ar) == 0) return false;
		$post = "
		<div class='post'>
			<div class='titolo'>
			".$ar["titolo"]."
			</div>
			<div class='contenuto'>
			".$ar["contenuto"]."
			</div>
			<div class='footer'>
				<div class='author'> By ".$ar["username"]."</div><div class='date'>".$ar["data"]."</div>
			</div>
		</div>
		";
		
		return $post;
	}
	
	function visualizzaDati ($utente="", $pass="", $type="") {
		if (!$utente || !$pass || !$type) return false;
		$html = "
		<div class='credenziali'>
			Nome utente: {$utente} <a href='modifica.php?update=username'>Modifica</a><br>
			Password: {$pass} <a href='modifica.php?update=password'>Modifica</a><br>
			Tipo account {$type}<br>";
		if ($type != "admin") {
			$html.="	<a href='modifica.php?update=delete'>Elimina profilo</a>
			</div>
			";
		}
		else {
			$html .= "</div>";
		}
		
		return $html;
	}
	
	mysql_connect("localhost","root","ubuntu") or die ("Connessione al database non riuscita: ".mysql_error());
	
	mysql_select_db ("agile") or die ("Selezione database non riuscita: ".mysql_error());
	
?>
