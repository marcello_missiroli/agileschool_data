<?php
	require_once('database.php');
	session_start();
	session_destroy();
	if (is_admin()){	
		header('location: admin.php?error=Logout%20Effettuato%20con%20Successo');
		exit();
	} 
	header('location: user.php?error=Logout%20Effettuato%20con%20Successo');
	exit();
?>
