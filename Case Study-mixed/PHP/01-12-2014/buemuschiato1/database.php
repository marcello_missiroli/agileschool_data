<?php
	session_start();
	
	function openDb(){
		
		$host = $GLOBALS['db_host']; 
		$user = $GLOBALS['db_user'];
		$pass = $GLOBALS['db_password'];
		$db = $GLOBALS['db_database'];
		
		mysql_connect($host,$user,$pass) or die(mysql_error());
		
		mysql_select_db($db);	
		
	}
	
	function closeDb(){
		
		mysql_close();
	
	} 
	
	function set_user_data($data){
		$_SESSION['ID'] = $data['ID'];
		$_SESSION['username'] = $data['username'];
		$_SESSION['type'] = $data['type'];
		
	}
	
	function clear_data(){
		session_destroy();
	}
	
	function already_login(){
		if(isset($_SESSION['username'])){
			return true;
		}
		return false;
	}
	
	function is_admin(){
		if(isset($_SESSION['type'])){
			if($_SESSION['type'] == 'A'){
				return true;
			}
			return false;
		}
	}
?>
