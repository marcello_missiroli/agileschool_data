<?php

	require_once('tools.php');
	require_once('database.php');

	session_start();

	$html=genericHTMLHeader();
	$html.=genericHTMLMessage("Crea un nuovo post:");
	$html.=genericHTMLHomeRedirect();
	$html.=postHTMLBody();

	if(isset($_REQUEST['submit'])) {
		if(newPost($_SESSION['userid'], $_REQUEST['title'], $_REQUEST['content'])) {
			$html.=postHTMLSuccess();
			header('location: index.php');
		} else $html.=postHTMLFailure();
	}

	$html.=closeHTML();

	echo $html;


?>