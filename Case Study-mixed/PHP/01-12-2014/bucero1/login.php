<?php

	require_once('tools.php');
	require_once('database.php');

	session_start();

	$html=genericHTMLHeader();

	$html.=genericHTMLMessage("Accedi come utente:");
	$html.=genericHTMLLogin("login.php");
	$html.=genericHTMLHomeRedirect();

	if(isset($_REQUEST['submit'])) {
		$check=explode(" ", checkUser($_REQUEST['username'], $_REQUEST['password'], 'user'));
		if (($check[0]!="invalid")) {
			$_SESSION['username']=$_REQUEST['username'];
			$_SESSION['type']='user';
			$_SESSION['userid']=$check[1];
			$html.=genericHTMLLoginSuccess();
		} else {
			$html.=genericHTMLLoginFailure();
			session_destroy();
		}
	}

	$html.=closeHTML();

	echo $html;

?>