<?php

	require_once('tools.php');
	require_once('database.php');

	session_start();

	$html=genericHTMLHeader();
	$html.=genericHTMLMessage("Profilo");
	$html.=profileHTMLBody($_SESSION['username']);
	$html.=genericHTMLHomeRedirect();

	if(isset($_REQUEST['submit'])) {
		if(!isset($_REQUEST['delete'])) {
			if(updateUser($_SESSION['userid'], $_REQUEST['username'], $_REQUEST['password'])) {
				$html.=profileHTMLSuccess();
				$_SESSION['username']=$_REQUEST['username'];
			} else $html.=profileHTMLFailure();
		} else {
			if(deleteUser($_SESSION['userid'])) {
				$html.=profileHTMLSuccess();
			} else $html.=profileHTMLFailure();
		}
	}

	$html.=closeHTML();

	echo $html;

?>