<?php

	require_once('database.php');

	function genericHTMLHeader() {
		return "
			<html>
				<head>
					<link href='style/style.css' rel='stylesheet' type='text/css'>
				</head>
				<body>
		";
	}

	function closeHTML() {
		return "
				</body>
			</html>
		";
	}

	function indexHTMLBody($uil, $gul) {
		$ret= "
					<p>Benvenuto!</p></br>
					<a href='login.php'>Accedi</a>
					<a href='admin.php'>Amministra</a>
		";
		if ($gul) {
			$ret.="
					<a href='index.php?cmd=logout'>Slogga</a>
			";
		}
		if ($uil) {
			$ret.="
					<a href='post.php'>Post</a>
					<a href='profilo.php'>Profilo</a>
			";
			$ret.=getPosts();
		}
		return $ret;
	}

	function genericHTMLMessage($msg) {
		return "
					<p>$msg</p>
		";
	}

	function genericHTMLHomeRedirect() {
		return "
					<a href='index.php'>Torna alla home</a>
		";
	}

	function genericHTMLLogin($red) {
		return "
					<form action='$red' method='POST'>
						<input class='fleft' type='text' name='username' placeholder='Username'> 
						<input type='text' name='password' placeholder='Password'>
						<input class='clear-float' type='submit' name='submit' value='Accedi'>
					</form>
		";
	}

	function genericHTMLLoginSuccess() {
		return "
					<span>Login effettuato!</span></br>
					<a href='index.php?cmd=logout'>Logout</a>
		";
	}

	function genericHTMLLoginFailure() {
		return "
					<span>Credenziali errate</span>
		";
	}

	function adminHTMLRedirect() {
		return "
					<span>Permesso negato, <a href='index.php'>torna alla home</a></span>
		";
	}

	function postHTMLBody() {
		return "
			<form action='post.php' method='POST'>
				<input type='text' name='title' placeholder='Titolo'></br>
				<input type='textarea' name='content' placeholder='Contenuto' size='50'></br>
				<input type='submit' name='submit' value='Invia post'>
			</form>
		";
	}

	function postHTMLSuccess() {
		return "
			<p>Post inserito con successo</p>
		";
	}

	function postHTMLFailure() {
		return "
			<p>Ops! Qualcosa &egrave; andato storto</p>
		";
	}

	function profileHTMLBody($session_username) {
		$return= "
			<form action='profilo.php' method='POST'>
				<input type='text' name='username' value='{$_SESSION['username']}'>
				<input type='text' name='password' placeholder='Nuova password'>
		";
		if($_SESSION['username']!='admin') {
			$return.="
				<input type='checkbox' name='delete'>Eliminami
			";
		};
		$return.="
				<input type='submit' name='submit' value='Modifica dati'>
			</form>
		";
		return $return;
	}

	function profileHTMLSuccess() {
		return "
			<p>Profilo modificato</p>
		";
	}

	function profileHTMLFailure() {
		return "
			<p>Ops! Qualcosa &egrave; andato storto</p>
		";
	}

?>