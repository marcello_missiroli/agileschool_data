<?php

	require_once('tools.php');

	session_start();

	$user_is_logged = false;
	$generic_user_logged = false;

	if(isset($_REQUEST['cmd'])) {
		if($_REQUEST['cmd']=='logout') {
			session_destroy();
			header('location: index.php');
		}
	}

	$html=genericHTMLHeader();

	if (isset($_SESSION['username'])) {
		$html.="<span>Benvenuto {$_SESSION['username']}</span>";
	}

	if (isset($_SESSION['type'])) {
		if ($_SESSION['type']=='user') {
			$user_is_logged = true;
		}
		if (($_SESSION['type']=='user')||($_SESSION['type']=='admin')) {
			$generic_user_logged = true;
		}
	}

	$html.=indexHTMLBody($user_is_logged, $generic_user_logged);
	$html.=closeHTML();

	echo $html;

?>