<?php

	require_once('tools.php');
	require_once('database.php');

	session_start();

	$html=genericHTMLHeader();
	$html.=genericHTMLMessage("Accedi come amministratore:");
	$html.=genericHTMLLogin("admin.php");

	if(isset($_REQUEST['submit'])) {
		$check=explode(" ", checkUser($_REQUEST['username'], $_REQUEST['password'], 'user'));
		if (($check[0]!="invalid")&&($check[0]!="match_error")) {
			$_SESSION['username']=$_REQUEST['username'];
			$_SESSION['type']='admin';
			$_SESSION['userid']=$check[1];
			$html.=genericHTMLLoginSuccess();
		} else if ($check[0]=="match_error") {
			$html.=adminHTMLRedirect();
			session_destroy();
		} else {
			$html.=genericHTMLLoginFailure();
			session_destroy();
		}
	}

	$html.=closeHTML();

	echo $html;

?>