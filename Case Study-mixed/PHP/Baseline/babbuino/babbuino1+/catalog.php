<?php
	
	$table = array(
	"0" => array("cod_prod" => "SH01VX", "nome" => "Villaggio exclusive Sharm 5s", "prezzo" => "800 euro"),
	"1" => array("cod_prod" => "ZN02VD", "nome" => "Villaggio diving Zanzibar 4s", "prezzo" => "1.100 euro"),
	"2" => array("cod_prod" => "ML03VB", "nome" => "Villaggio bungalow Maldive 5s", "prezzo" => "1.500 euro"));

	function printTable($tab){
		
		$keys = array_keys($tab[0]);
		
		echo "<table border = 1>";
		
		echo "<tr>";
		
		foreach($keys as $key){
			echo "<th>$key</th>";
		}
		
		echo"</tr>";
		
		
		
		
		foreach($tab as $row){
			echo "<tr>";
			foreach($row as $cell){
				echo "<td>$cell</td>";
			}
			echo "</tr>";
		}
		echo "</table>";
	}
	
	printTable($table);

?>
