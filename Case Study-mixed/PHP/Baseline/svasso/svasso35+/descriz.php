
<html>
	<head>
		<title>DESCRIZIONE ELEMENTO</title>
		
		<style type="text/css">
       

	body {
		background: #909090;
	}

	body, input, select, textarea {
		color: #00CCFF;
	
	}

	a {
		-moz-transition: color 0.2s ease-in-out, border-bottom-color 0.2s ease-in-out;
		-webkit-transition: color 0.2s ease-in-out, border-bottom-color 0.2s ease-in-out;
		-o-transition: color 0.2s ease-in-out, border-bottom-color 0.2s ease-in-out;
		-ms-transition: color 0.2s ease-in-out, border-bottom-color 0.2s ease-in-out;
		transition: color 0.2s ease-in-out, border-bottom-color 0.2s ease-in-out;
		border-bottom: dotted 1px;
		color: #0066FF;
		text-decoration: none;
	}

		a:hover {
			border-bottom-color: transparent;
		}

	strong, b {
		color: #646464;
		font-weight: 400;
	}

	em, i {
		font-style: italic;
	}

	p {
		margin: 0 0 2em 0;
	}

	h1, h2, h3, h4, h5, h6 {
		color: #646464;
		font-weight: 300;
		line-height: 1em;
		margin: 0 0 0.5em 0;
	}

		h1 a, h2 a, h3 a, h4 a, h5 a, h6 a {
			color: #000000;
			text-decoration: none;
		}

	h2 {
	    color:#000000;
		font-size: 2.25em;
		line-height: 1.5em;
		letter-spacing: -0.035em;
	}

	h3 {
		font-size: 1.75em;
		line-height: 1.5em;
		letter-spacing: -0.025em;
	}

	h4 {
		font-size: 1.1em;
		line-height: 1.5em;
		letter-spacing: 0;
	}

	h5 {
		font-size: 0.9em;
		line-height: 1.5em;
		letter-spacing: 0;
	}

	h6 {
		font-size: 0.7em;
		line-height: 1.5em;
		letter-spacing: 0;
	}

	sub {
		font-size: 0.8em;
		position: relative;
		top: 0.5em;
	}

	sup {
		font-size: 0.8em;
		position: relative;
		top: -0.5em;
	}

	hr {
		border: 0;
		border-bottom: solid 2px #e5e5e5;
		margin: 2em 0;
	}

		hr.major {
			margin: 3em 0;
		}

	blockquote {
		border-left: solid 4px #e5e5e5;
		font-style: italic;
		margin: 0 0 2em 0;
		padding: 0.5em 0 0.5em 2em;
	}

	pre {
		-webkit-overflow-scrolling: touch;
		background:#000000;
		border-radius: 6px;
		border: solid 1px #e5e5e5;
		font-family: monospace;
		font-size: 0.9em;
		line-height: 1.75em;
		margin: 0 0 2em 0;
		overflow-x: auto;
		padding: 1em 1.5em;
	}

	code {
		background: #000000;
		border-radius: 6px;
		border: solid 1px #e5e5e5;
		font-family: monospace;
		font-size: 0.9em;
		margin: 0 0.25em;
		padding: 0.25em 0.65em;
	}

	.align-left {
		text-align: left;
	}

	.align-center {
		text-align: center;
	}

	.align-right {
		text-align: right;
	}

/* Section/Article */

	section.special, article.special {
		text-align: center;
	}

	header p {
		color: #999;
		position: relative;
		margin: 0 0 1.5em 0;
		font-style: italic;
	}

	header h2 + p {
		font-size: 1.25em;
		margin-top: -1em;
		line-height: 1.5em;
	}

	header h3 + p {
		font-size: 1.1em;
		margin-top: -0.85em;
		line-height: 1.5em;
	}

	header h4 + p,
	header h5 + p,
	header h6 + p {
		font-size: 0.8em;
		margin-top: -0.5em;
		line-height: 1.5em;
	}

	header.major {
		padding: 1em 0;
		text-align: center;
	}

		header.major h2 {
			margin: 0;
		}

		header.major p {
			display: inline-block;
			border-top: solid 2px #FFFFCC;
			color: #FFFFCC;
			margin: 1.5em 0 0 0;
			padding: 1.5em 0 0 0;
			font-style: normal;
		}



/* Box */

	.box {
		background: #B2B2B2;
		border-radius: 6px;
		box-shadow: 0 2px 0 0 #CCFF66;
		margin: 0 0 2em 0;
		padding: 3em;
	}

		.box > :last-child {
			margin-bottom: 0;
		}

		.box.alt {
			background: none !important;
			border-radius: 0 !important;
			box-shadow: #CCFF66!important;
			margin: 0 0 2em 0;
			padding: 0 !important;
		}

		.box.features .features-row {   
		
			position: relative;
		}

			.box.features .features-row:after {
				clear: both;
				content: '';
				display: block;
			}

			.box.features .features-row section {
				float: left;
				padding: 3em;
				width: 50%;
			}

				.box.features .features-row section :last-child {
					margin-bottom: 0;
				}

				.box.features .features-row section:nth-child(2n) {
					padding-right: 0;
				}

					.box.features .features-row section:nth-child(2n):before {
						background:#CCFF66;
						content: '';
						display: block;
						height: 100%;
						margin-left: -3em;
						position: absolute;
						top: 0;
						width: 2px;
					}

				.box.features .features-row section:nth-child(2n-1) {
					padding-left: 0;
				}

			.box.features .features-row:first-child {
				border-top: 0;
			}

				.box.features .features-row:first-child section {
					padding-top: 0;
				}

			.box.features .features-row:last-child {
				padding-bottom: 0;
			}

				.box.features .features-row:last-child section {
					padding-bottom: 0;
				}

		.box.special {
			text-align: center;
		}

		.box .image.featured {
			border-radius: 0;
			display: block;
			margin: 3em 0 3em -3em;
			position: relative;
			width: calc(100% + 6em);
		}

			.box .image.featured img {
				border-radius: 0;
				display: block;
				width: 100%;
			}

			.box .image.featured:first-child {
				border-radius: 6px 6px 0 0;
				margin-bottom: 3em;
				margin-top: -3em;
			}

				.box .image.featured:first-child img {
					border-radius: 6px 6px 0 0;
				}

			.box .image.featured:last-child {
				border-radius: 0 0 6px 6px;
				margin-bottom: -3em;
				margin-top: 3em;
			}

				.box .image.featured:last-child img {
					border-radius: 0 0 6px 6px;
				}


/* List */

	ol {
		list-style: decimal;
		margin: 0 0 2em 0;
		padding-left: 1.25em;
	}

		ol li {
			padding-left: 0.25em;
		}

	ul {
		list-style: disc;
		margin: 0 0 2em 0;
		padding-left: 1em;
	}

		ul li {
			padding-left: 0.5em;
		}

		ul.alt {
			list-style: none;
			padding-left: 0;
		}

			ul.alt li {
				border-top: solid 1px #e5e5e5;
				padding: 0.5em 0;
			}

				ul.alt li:first-child {
					border-top: 0;
					padding-top: 0;
				}

		ul.icons {
			cursor: default;
			list-style: none;
			padding-left: 0;
		}

			ul.icons li {
				display: inline-block;
				padding: 0 1.25em 0 0;
			}

				ul.icons li:last-child {
					padding-right: 0;
				}

				ul.icons li .icon {
					color: inherit;
				}

					ul.icons li .icon:before {
						font-size: 1.75em;
					} 

		ul.actions {
		    color:#000000;
			cursor: default;
			list-style: none;
			padding-left: 0;
		}

			ul.actions li {
				display: inline-block;
				padding: 0 1em 0 0;
				vertical-align: middle;
			}

				ul.actions li:last-child {
					padding-right: 0;
				}

			ul.actions.small li {
				padding: 0 0.5em 0 0;
			}

			ul.actions.vertical li {
				display: block;
				padding: 1em 0 0 0;
			}

				ul.actions.vertical li:first-child {
					padding-top: 0;
				}

				ul.actions.vertical li > * {
					margin-bottom: 0;
				}

			ul.actions.vertical.small li {
				padding: 0.5em 0 0 0;
			}

				ul.actions.vertical.small li:first-child {
					padding-top: 0;
				}

			ul.actions.fit {
				display: table;
				margin-left: -1em;
				padding: 0;
				table-layout: fixed;
				width: calc(100% + 1em);
			}

				ul.actions.fit li {
					display: table-cell;
					padding: 0 0 0 1em;
				}

					ul.actions.fit li > * {
						margin-bottom: 0;
					}

				ul.actions.fit.small {
					margin-left: -0.5em;
					width: calc(100% + 0.5em);
				}

					ul.actions.fit.small li {
						padding: 0 0 0 0.5em;
					}

	dl {
		margin: 0 0 2em 0;
	}

/* Table */

	.table-wrapper {
		-webkit-overflow-scrolling: touch;
		overflow-x: auto;
	}

	table {
		margin: 0 0 2em 0;
		width: 100%;
	}

		table tbody tr {
			border: solid 1px #e5e5e5;
			border-left: 0;
			border-right: 0;
		}

			table tbody tr:nth-child(2n + 1) {
				background-color:#909090;
			}

		table td {
			padding: 0.75em 0.75em;
		}

		table th {
			color: #646464;
			font-size: 0.9em;
			font-weight: 300;
			padding: 0 0.75em 0.75em 0.75em;
			text-align: left;
		}

		table thead {
			border-bottom: solid 2px #e5e5e5;
		}

		table tfoot {
			border-top: solid 2px #e5e5e5;
		}

		table.alt {
			border-collapse: separate;
		}

			table.alt tbody tr td {
				border: solid 1px #e5e5e5;
				border-left-width: 0;
				border-top-width: 0;
			}

				table.alt tbody tr td:first-child {
					border-left-width: 1px;
				}

			table.alt tbody tr:first-child td {
				border-top-width: 1px;
			}

			table.alt thead {
				border-bottom: 0;
			}

			table.alt tfoot {
				border-top: 0;
			}


/* Header */

	#skel-layers-wrapper {
		padding-top: 3em;
	}

	body.landing #skel-layers-wrapper {
		padding-top: 0;
	}
	@-moz-keyframes reveal-header { 0% { top: -5em; } 100% { top: 0; } }
	@-webkit-keyframes reveal-header { 0% { top: -5em; } 100% { top: 0; } }
	@-o-keyframes reveal-header { 0% { top: -5em; } 100% { top: 0; } }
	@-ms-keyframes reveal-header { 0% { top: -5em; } 100% { top: 0; } }
	@keyframes reveal-header { 0% { top: -5em; } 100% { top: 0; } }

	#header {
		background: #444;
		color: #bbb;
		cursor: default;
		height: 3.25em;
		left: 0;
		line-height: 3.25em;
		position: fixed;
		top: 0;
		width: 100%;
		z-index: 10000;
	}

		#header h1 {
			color:#000000;
			height: inherit;
			left: 1.25em;
			line-height: inherit;
			margin: 0;
			padding: 0;
			position: absolute;
			top: 0;
		}

			#header h1 a {
				color: #000000;
				font-weight: 400;
				border: 0;
			}

		#header nav {
			height: inherit;
			line-height: inherit;
			position: absolute;
			right: 0.75em;
			top: 0;
			vertical-align: middle;
		}

			#header nav > ul {
				list-style: none;
				margin: 0;
				padding-left: 0;
			}

				#header nav > ul > li {
					display: inline-block;
					padding-left: 0;
				}

					#header nav > ul > li > ul {
						display: none;
					}

					#header nav > ul > li a {
						display: inline-block;
						height: 2em;
						line-height: 1.95em;
						padding: 0 1em;
						border-radius: 6px;
					}

					#header nav > ul > li a:not(.button) {
						color: #000000;
						display: inline-block;
						text-decoration: none;
						border: 0;
					}

						#header nav > ul > li a:not(.button).icon:before {
							color: #000000;
							margin-right: 0.5em;
						}

					#header nav > ul > li:first-child {
						margin-left: 0;
					}

					#header nav > ul > li.active a:not(.button) {
						background-color: #000000;
					}

					#header nav > ul > li .button {
						margin: 0 0 0 0.5em;
						position: relative;
					}

		#header input[type="submit"],
		#header input[type="reset"],
		#header input[type="button"],
		#header .button {
			background-color: transparent;
			box-shadow: inset 0 0 0 2px #999;
			color: #000000;
		}

			#header input[type="submit"]:hover,
			#header input[type="reset"]:hover,
			#header input[type="button"]:hover,
			#header .button:hover {
				background-color: rgba(153, 153, 153, 0.25);
			}

			#header input[type="submit"]:active,
			#header input[type="reset"]:active,
			#header input[type="button"]:active,
			#header .button:active {
				background-color: rgba(153, 153, 153, 0.5);
			}

		#header .container {
			position: relative;
		}

			#header .container h1 {
				left: 0;
			}

			#header .container nav {
				right: 0;
			}

		#header.reveal {
			-moz-animation: reveal-header 0.5s;
			-webkit-animation: reveal-header 0.5s;
			-o-animation: reveal-header 0.5s;
			-ms-animation: reveal-header 0.5s;
			animation: reveal-header 0.5s;
		}

		#header.alt {
			-moz-animation: none;
			-webkit-animation: none;
			-o-animation: none;
			-ms-animation: none;
			animation: none;
			background: none;
			color: #66FFFF;
			position: absolute;
		}

			#header.alt nav > ul > li a:not(.button).icon:before {
				color: #66FFFF;
			}

			#header.alt nav > ul > li.active a:not(.button) {
				background-color: rgba(255, 255, 255, 0.2);
			}

			#header.alt input[type="submit"],
			#header.alt input[type="reset"],
			#header.alt input[type="button"],
			#header.alt .button {
				box-shadow: inset 0 0 0 2px rgba(255, 255, 255, 0.5);
			}

				#header.alt input[type="submit"]:hover,
				#header.alt input[type="reset"]:hover,
				#header.alt input[type="button"]:hover,
				#header.alt .button:hover {
					background-color: rgba(255, 255, 255, 0.1);
				}

				#header.alt input[type="submit"]:active,
				#header.alt input[type="reset"]:active,
				#header.alt input[type="button"]:active,
				#header.alt .button:active {
					background-color: rgba(255, 255, 255, 0.2);
				}

	
	
/* Main */

	#main {
		padding: 4em 0;
	}

		#main > header {
			text-align: center;
			margin: 0 0 3em 0;
		}

			#main > header h2 {
				font-size: 2.75em;
				margin: 0;
			}

			#main > header p {
				border-top: solid 2px #e5e5e5;
				color: #777;
				display: inline-block;
				font-style: normal;
				margin: 1em 0 0 0;
				padding: 1em 0 1.25em 0;
			}

	body.landing #main {
		margin-top: -14em;
	}


        
        
       
        </style>
	</head>
	<body>

		<!-- Header -->
			<header id="header" class="alt">
				<nav id="nav">
					<ul>
						<li><a href="http://danu23.altervista.org">Home</a></li>
						<li>
							<a href="" >Pagine</a>
							<ul>
								<li><a href="http://danu23.altervista.org/accedi/accedi.php"><font color=#000000>Accedi</font></a></li>
								<li><a href="http://danu23.altervista.org/word.html"><font color=#000000>Word</font></a></li>
								<li><a href="http://danu23.altervista.org/parchi.php"><font color=#000000>Visualizza</font></a></li>
		
							</ul>
						</li>
					</ul>
				</nav>
			</header>
			<section id="main" class="container">
				
                <form method="post" action="descrizioneelemento.php">
                 <?php
echo "<table border=1> <br>
 <center><font color=#0066CC size=5 face=arial>ECCO LA DESCRIZIONE DEL ELEMENTO CHE HAI SCETO...</font></center>
<br>";

  $connessione = mysql_connect('localhost', 'danu23', '', '')
  or die('Connessione non riuscita: ' . mysql_error());
  mysql_select_db('my_danu23', $connessione) or die('Errore...');
  //lettura
  $result = mysql_query("SELECT elemento,descrizione FROM t_elementi WHERE id_elemento=".$_POST['elemento'].";");
  while ($row = mysql_fetch_array($result)) 
  {
     echo"<tr><td>".$row['descrizione']."</td></tr>";
     echo"<tr><td><center><img src ='parchinaturali/img/".$_POST["elemento"].".jpg'></td></tr></center>";
   // printf("elemento: %s descrizione: %s <br>",  $row["elemento"], $row["descrizione"]);
   }
   
   
    echo "</table>";
    mysql_free_result($result); //libero le variabili 
    mysql_close($connessione);
    ?> 
			</form>
	</body>
</html>

