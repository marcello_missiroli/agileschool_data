<?php

	$carrello = array(
		
		array(
			"cod_prod"=>"SH01VX",
			"nome"=>"Villaggio exclusive Sharm 5s",
			"prezzo"=>800
		),
		
		array(
			"cod_prod"=>"ZN02VD",
			"nome"=>"Villaggio diving Zanzibar 4s",
			"prezzo"=>1100
		),
		
		array(
			"cod_prod"=>"ML03VB",
			"nome"=>"Villaggio bungalow Maldive 5s",
			"prezzo"=>1500
		)
	);
	function creaTabella($array){
		echo "<table border=''>";
		echo "<tr>";
		$chiavi = array_keys($array[0]);
		foreach($chiavi as $elem){
			
			echo "<td><b>$elem</b></td>";
		}
		echo "</tr>";
		
		foreach($array as $elem){
			echo "<tr>";
			foreach($elem as $v){
				echo "<td>$v</td>";
			}
			echo "</tr>";
		}
		
		echo "</table>";
	}
	
	creaTabella($carrello);
?>
