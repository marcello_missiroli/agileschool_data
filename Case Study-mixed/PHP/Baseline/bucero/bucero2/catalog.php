<?php
	$prodotti = array(
		array("cod_prod" => "SH01VX","nome" => "Villaggio exclusive Sharm 5s","prezzo" => 800 ),
		array("cod_prod" => "ZN02VD","nome" => "Villaggio diving Zanzibar 4s","prezzo" => 1100),
		array("cod_prod" => "ML03VB","nome" => "Villaggio bungalow Maldive 5s","prezzo" => 1500)
	);
	
	function stampa_tabella ($arr) {
		$result = "";
		for ($i = 0; $i<count($arr); $i++) {
			$result .= "
				<tr>
					<td>{$arr[$i]["cod_prod"]}</td>
					<td>{$arr[$i]["nome"]}</td>
					<td>{$arr[$i]["prezzo"]}</td>
				</tr>
			";
		}
		return $result;
	}
	
	$html = "
	<html>
		<head>
			<title>Array</title>
			<style>
				td {
					border : 1px solid black;
					}
			</style>
		</head>
		<body>
			<table>
				<thead>
					<td>cod_prod</td>
					<td>nome</td>
					<td>prezzo</td>
				</thead>
				".stampa_tabella($prodotti)."
			</table>
		</body>
	</html>
	";
	
	echo $html;

?>
