<?php

	$products = array(
			"cod_prod"=>array("SH01VX","SH01VD"),
			"nome"=>"Villaggio exlusive Sharm 5s",
			"prezzo"=>"800 euro",
		);

	function createTable($elem) {
		$tmp="
			<table border=1>
				<thead>
					<tr>
		";
		$fieldsNames = array_keys($elem);
		for($i = 0; $i < count($fieldsNames); $i++) {
			$tmp.="
				<th>{$fieldsNames[$i]}</th>
			";
		}
		$tmp.="
					</tr>
				</thead>
				<tbody>
					<tr>
		";
		foreach ($elem as $key => $value) {
			foreach($value as $sValue) {
				$tmp.="
						<td>$sValue</td>
				";
			}
		}
		$tmp.="
					</tr>
				</tbody>
			</table>
		";

		return $tmp;
	}

	$html="
		<html>
			<head>
			</head>
			<body>
	";

	$html.=createTable($products);

	$html.="
		</html>
	";

	echo $html;

?>

