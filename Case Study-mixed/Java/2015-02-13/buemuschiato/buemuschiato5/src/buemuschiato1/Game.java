/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package buemuschiato1;

/**
 *
 * @author i.cotti
 */
public class Game {
String giocatore1;
String giocatore2;
int punteggio1=0;
int punteggio2=0;
boolean v1=false;
boolean v2=false;
int vittoria=0;
    public Game(String s1,String s2) //instanza i giocatori
    {
        this.giocatore1=s1;             
        this.giocatore2=s2;
    }
    public String getPunteggio() // restituisce i punteggi come stringa
    {
        String punt="";
        punt=convertiPunteggio(punteggio1,1);
        punt+=", ";
        punt+=convertiPunteggio(punteggio2,2);
        if(vittoria==1){
            punt=giocatore1 + " vince";
        }
        else if(vittoria==2){
            punt= giocatore2 + " vince";
        }
        return punt;
    }
    public String convertiPunteggio(int punt,int quale) //converte il punteggio da int a stringa
    {
        if(punt==0)
        {
            return "zero";
        }
        else if(punt==1){
            return "quindici";
        }
        else if(punt==2){
            return "trenta";
        }
        else if (punt==3)
        {
          return "quaranta";  
        }
        else if(punt>3)  //calcolo parità/vittoria/vantaggio
        {
            if(quale==1 && punteggio2>=3)  //punto giocatore 1
            {
                if(!v1 && !v2)
                {
                    v1=true;
                    return "vantaggio";
                }
                else if( v1 && !v2) 
                {
                    vittoria=1;
                    return "vittoria";
                }
                else if(!v1 && v2)
                {
                    v2=false;
                    return "pareggio";
                }
                
            }
            else if(quale==2 && punteggio1>=3) //punto giocatore 2
            {
                if(!v2 && !v1)
                {
                    v2=true;
                    return "vantaggio";
                }
                else if( v2 && !v1)
                {
                    vittoria=2;
                    return "vittoria";
                }
                else if(!v2 && v1)
                {
                    v1=false;
                    return "pareggio";
                }
                else if(quale==1 && punteggio2<3){ //secondo controllo vittoria
                    vittoria=1;
                    return "vittoria";
                    
                }
                else if(quale==2 && punteggio1<3){ // secondo controllo vittoria
                    vittoria=2;
                    return "vittoria";
                }
                
            }
        }
        return null;
    }
    public void puntoPer(String gioc){ //aggiunge un punto
        
        if(gioc.equalsIgnoreCase(giocatore1)){
            punteggio1+=1;
        }
        else if(gioc.equalsIgnoreCase(giocatore2)){
            punteggio2+=1;
        }
    }

}
