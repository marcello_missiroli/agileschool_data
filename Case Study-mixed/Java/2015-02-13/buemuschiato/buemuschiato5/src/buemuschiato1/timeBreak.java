/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package buemuschiato1;

/**
 *
 * @author i.cotti
 */
public class timeBreak {
    
String giocatore1;
String giocatore2;
int punteggio1=0;
int punteggio2=0;
boolean v1=false;
boolean v2=false;
int vittoria=0;
    public timeBreak(String s1,String s2) //instanza i giocatori
    {
        this.giocatore1=s1;             
        this.giocatore2=s2;
    }
    public String getPunteggio() // restituisce i punteggi come stringa
    {
        String punt="";
        punt=convertiPunteggio(punteggio1,1);
        punt+=", ";
        punt+=convertiPunteggio(punteggio2,2);
        if(vittoria==1){
            punt=giocatore1 + " vince";
        }
        else if(vittoria==2){
            punt= giocatore2 + " vince";
        }
        return punt;
    }
    public String convertiPunteggio(int punt,int quale) //converte il punteggio da int a stringa
    {
        if(punt>=7)
        {
            vittoria=quale;
        }
        else if(punteggio1-punteggio2>=2)
        {
            vittoria=1;
        }
        else if( punteggio2-punteggio1>=2)
            vittoria=2;
        return null;
    }
    public void puntoPer(String gioc){ //aggiunge un punto
        
        if(gioc.equalsIgnoreCase(giocatore1)){
            punteggio1+=1;
        }
        else if(gioc.equalsIgnoreCase(giocatore2)){
            punteggio2+=1;
        }
    }
}
