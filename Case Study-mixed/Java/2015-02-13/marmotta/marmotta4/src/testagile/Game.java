/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package testagile;

/**
 *
 * @author l.venturi
 */
class Game {

    String pippo;
    String pluto;
    int pippoP;
    int plutoP;
    boolean vpl,vpi,p;
    
    Game(String pippo, String pluto) {
       setPippo(pippo);
       setPluto(pluto);
       pippoP=0;
       plutoP=0;
       vpl=false;
       vpi=false;
       p=false;
    }

    Object getPunteggio() {
        if(pippoP==0 && plutoP==0)
            return "zero, zero";
        if(pippoP==0 && plutoP==15)
            return "zero, quindici";
        if(pippoP==40 && plutoP==0)
            return "quaranta, zero";
        if(pippoP==30 && plutoP==15)
            return "trenta, quindici";
        if(vpl==true)
            return "vantaggio Pluto";
        if(vpi==true)
            return "vantaggio Pippo";
        if(plutoP==40 && pippoP==40)
            return "parità ";

        return true;
    }

    void puntoPer(String s) {
        if(s=="Pluto"){
            if(plutoP==40 && pippoP==40){
                vpl=true;
                vpi=false;
            }else
            if(plutoP==40)
                vpl=true;
            else
                if(plutoP==30)
                plutoP=40;
                else 
                    plutoP=plutoP+15;
            

        }
        if(s=="Pippo"){ 
             if(plutoP==40 && pippoP==40){
                vpi=true;
                vpl=false;
             }else
            if(pippoP==40)
                vpi=true;
            else
                if(pippoP==30)
                pippoP=40;
                else 
                    pippoP+=15;
            if(plutoP==40 && pippoP==40)
                p=false;
        }
        if(pippoP==40||plutoP==40)
            p=true;
    }

    private void setPippo(String p) {
        pippo=p;
    }

    private void setPluto(String pl) {
        pluto=pl;
    }
    
}
