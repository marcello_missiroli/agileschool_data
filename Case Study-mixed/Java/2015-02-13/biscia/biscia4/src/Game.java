/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author s.walz
 */
public class Game {
    
    protected String punteggio; 
    protected int contaPippo=0;
    protected int contaPluto=0;
    protected String punteggioPluto = ", zero"; //inizializzata in questo modo per sintassi
    protected String punteggioPippo = "zero";
    
    
    Game(String pippo, String pluto) {
        
        punteggio = punteggioPippo+punteggioPluto;
       
    }

    Object getPunteggio() {
        return punteggio;
    }

    void puntoPer(String pluto) {
       
        if(pluto == "Pippo") {
            // Se è la prima volta che entra nel if
            if (contaPippo==0) {
                punteggioPippo="quindici"; //Aumenta il punteggio di pippo
                contaPippo++; //contatore che serve per controllare i punteggi
            }
            else if (contaPippo==1) {
               punteggioPippo="trenta";
               contaPippo++;
            }
            else if(contaPippo==2) {
                punteggioPippo = "quaranta";
                contaPippo++;
            }
            else
                contaPippo++;
        }
        if (pluto=="Pluto") {
            if (contaPluto==0) {
                punteggioPluto=", quindici";
                contaPluto++;
            }
            else if (contaPluto==1) {
               punteggioPluto=", trenta";
               contaPluto++;
            }
            else if (contaPluto==2){
                punteggioPluto=", quaranta";
                contaPluto++;
            }
            else
                contaPluto++;
        }
        

        if(contaPippo==contaPluto && contaPippo>2 && contaPluto>2){
            punteggio="parita";
        }
        else if (contaPippo==3 && contaPluto<2){
            punteggio="Pippo vince";
        }
        else if (contaPluto==3 && contaPippo<2){
            punteggio="Pluto vince";
        }
        else if(contaPippo > 3 && contaPippo > contaPluto)
            punteggio = "vantaggio Pippo";
        else if (contaPluto > 3 && contaPluto > contaPippo) 
            punteggio = "vantaggio Pluto";
    
        else
            punteggio = punteggioPippo + punteggioPluto;    

    }   
}
