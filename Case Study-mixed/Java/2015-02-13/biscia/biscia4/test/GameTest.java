/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author s.walz
 */
public class GameTest {
    
    public GameTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    Game game;

    @Before
    public void perTuttiITest() {
        game = new Game("Pippo", "Pluto");
    }

    @After
    public void tearDown() {
    }
    
    @Test
    public void zeroDeveEssereLaDescrzionePerIlPunteggio0() {
        assertEquals(game.getPunteggio(), "zero, zero");
    }
    
    @Test
    public void quindiciDeveEssereIlDescrittorePerIlPunteggio1() {
        game.puntoPer("Pluto");
        assertEquals(game.getPunteggio(), "zero, quindici");
    }

    @Test
    public void trentaDeveEssereIlDescrittorePerIlPunteggio2() {
        game.puntoPer("Pippo");
        game.puntoPer("Pippo");
        game.puntoPer("Pluto");
        assertEquals(game.getPunteggio(), "trenta, quindici");
    }
    
    
    @Test
    public void quarantaSDeveEssereIlDescrittorePerIlPunteggio3() {
        game.puntoPer("Pippo");
        game.puntoPer("Pippo");
        game.puntoPer("Pippo");
        assertEquals(game.getPunteggio(), "quaranta, zero");
    }
    
    
    
    @Test
    public void vantaggioDeveEssereIlDescrittorePerIlPunteggioQuandoEntrmbiHannoFatto3PuntiEUnGiocatoreHaUnPuntoDiVantaggio() {
        game.puntoPer("Pippo");
        game.puntoPer("Pippo");
        game.puntoPer("Pippo");
        game.puntoPer("Pluto");
        game.puntoPer("Pluto");
        game.puntoPer("Pluto");
        game.puntoPer("Pluto");
        assertEquals(game.getPunteggio(), "vantaggio Pluto");
    }

        @Test
    public void paritaDeveEssereIlDescrittorePerIlPunteggioQuandoEntrmbiHannoFatto3PuntiEIPunteggiSonoUguali() {
        game.puntoPer("Pippo");
        game.puntoPer("Pippo");
        game.puntoPer("Pippo");
        game.puntoPer("Pluto");
        game.puntoPer("Pluto");
        game.puntoPer("Pluto");
        assertEquals(game.getPunteggio(), "parita");
        game.puntoPer("Pippo");
        assertFalse(game.getPunteggio().equals("parita"));
        game.puntoPer("Pluto");
        assertEquals(game.getPunteggio(), "parita");
    }

        @Test
    public void ilGameDeveEssereVintoDalPrimoGiocatoreCheAbbiAlmenoQuattroPuntiEDuePuntiDiVantaggio() {
        game.puntoPer("Pippo");
        game.puntoPer("Pippo");
        game.puntoPer("Pippo");
        game.puntoPer("Pippo");
        game.puntoPer("Pluto");
        game.puntoPer("Pluto");
        game.puntoPer("Pluto");
        assertFalse(game.getPunteggio().equals("Pippo vince"));
        assertFalse(game.getPunteggio().equals("Pluto vince"));
        game.puntoPer("Pippo");
        assertEquals(game.getPunteggio(), "Pippo vince");
    }
    
    @Test
    public void testSomeMethod() {
        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");    
    }
    
}
