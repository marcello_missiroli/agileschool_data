/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package babbuino;

/**
 *
 * @author l.bertoldi
 */
public class Game {
    public String giocatore1;
    public String giocatore2;
    public int punteggio1;
    public int punteggio2;
    
    public Game(String g1,String g2){
        this.giocatore1=g1;
        this.giocatore2=g2;
        this.punteggio1=0;
        this.punteggio2=0;
    }
    
    
    //Trasformiamo il punteggio in parole, yay
    public String getPunteggio(){
        String punteggio="";
        
        switch (punteggio1){
            case  0: punteggio="zero, "; 
                break;
            case 1: punteggio="quindici, ";
                break;
            case 2:punteggio="trenta, ";
                break;
            case 3:punteggio="quaranta, ";
                break;
            case 4:punteggio="vantaggio "+this.giocatore1;
                break;
            }
        if(punteggio1!=4){
            switch (punteggio2){
                case  0: punteggio+="zero"; 
                    break;
                case 1: punteggio+="quindici";
                    break;
                case 2:punteggio+="trenta";
                    break;
                case 3:punteggio+="quaranta";
                    break;
                case 4:punteggio="vantaggio "+this.giocatore2;
                    break;
            }
        }
        if(punteggio1==4&&punteggio2<=2)
            punteggio=giocatore1+" vince";
        if(punteggio2==4&&punteggio1<=2)
            punteggio=giocatore2+" vince";
        if(punteggio1==5&&punteggio2<=3)
            punteggio=giocatore1+" vince";
        if(punteggio2==5&&punteggio1<=3)
            punteggio=giocatore2+" vince";
        
        if(punteggio1==3 && punteggio2==3)
            punteggio="parità";
        
        return punteggio;
    }
    
    //Diamo un punto in piu' al giocatore C:
    public void puntoPer(String g){
        if(g.equalsIgnoreCase(giocatore1)){
            if(punteggio2!=4)
                punteggio1++;
            else
                punteggio2--;
        }
        else{
            if(punteggio1!=4)
                punteggio2++;
            else
                punteggio1--;
        }
    }
}
