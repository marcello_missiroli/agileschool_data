/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package bucero3;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author v.kumar
 */
public class GameTest {
    private Game game;
    
    public GameTest() {
        Game game;
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void perTuttiITest() {
        game = new Game("Pippo", "Pluto");}
    
    @After
    public void tearDown() {
    }

    @Test
    public void zeroDeveEssereLaDescrzionePerIlPunteggio0() {
        assertEquals(game.getPunteggio(), "zero, zero");
    }
    
    //PIPPO SE VINCE SARA' A 15 E VERRA' COMUNICATO "15 A 0"
    @Test
    public void quindiciDeveEssereIlDescrittorePerIlPunteggio1() {
        game.puntoPer("Pluto");
        assertEquals(game.getPunteggio(), "zero, quindici");
    }
    
    //PIPPO SE VINCE SARA' A 30 E VERRA' COMUNICATO "30 A 0"
    @Test
    public void trentaDeveEssereIlDescrittorePerIlPunteggio2() {
        game.puntoPer("Pippo");
        game.puntoPer("Pippo");
        game.puntoPer("Pluto");
        assertEquals(game.getPunteggio(), "trenta, quindici");
    }
    @Test
    //PIPPO SE VINCE SARA' A 40 E VERRA' COMUNICATO "40 A 0"
     public void quarantaSDeveEssereIlDescrittorePerIlPunteggio3() {
        game.puntoPer("Pippo");game.puntoPer("Pippo");game.puntoPer("Pippo");
        assertEquals(game.getPunteggio(), "quaranta, zero");
    }
     
      @Test
    public void paritàDeveEssereIlDescrittorePerIlPunteggioQuandoEntrmbiHannoFatto3PuntiEIPunteggiSonoUguali() {
        game.puntoPer("Pippo");game.puntoPer("Pippo");game.puntoPer("Pippo");
        game.puntoPer("Pluto");game.puntoPer("Pluto");game.puntoPer("Pluto");
        assertEquals(game.getPunteggio(), "parità");
        game.puntoPer("Pippo");
        assertFalse(game.getPunteggio().equals("parità"));
        game.puntoPer("Pluto");
        assertEquals(game.getPunteggio(), "parità");
    }



  


        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    
   
    }

    


    
    
    

    

