/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package progettotennis;

/**
 *
 * @author e.domingo
 */
public class Game {
    private String player1;
    private String player2;
    private String punteggioPlayer1;
    private String punteggioPlayer2;
    private int cPippo;
    private int cPluto;

    public Game(String s,String s2){
        player1 = s;
        player2 = s2;
        this.punteggioPlayer1 = "zero";
        this.punteggioPlayer2 = "zero";
        //cPippo = contatore vantaggio Player1
        //cPluto = contatore vantaggio Player2
        cPippo=0;
        cPluto=0;
    }

    public String getPunteggio() {
        //ritorna i punteggi separati da una virgola
        if(this.punteggioPlayer1.equals("vantaggio "+player1) || cPippo == 2){
            //Se pippo è in vantaggio di 2  ed è in vantaggio ritorna la vittoria.
            //Oppure ritorna il vantaggio di uno o dell'altro
            return this.punteggioPlayer1;   
        }else if(this.punteggioPlayer2.equals("vantaggio " + player2) || cPluto == 2){
            //Se pluto è in vantaggio di 2  ed è in vantaggio ritorna la vittoria.
            return this.punteggioPlayer2;
        }
        //Se sono tutte e due a quaranta, ritorna parita
        else if(this.punteggioPlayer2.equals(this.punteggioPlayer1) && this.punteggioPlayer1.equals("quaranta")){
            return "parita";
        }
        //Altrimenti ritorna il punteggio normale
        else
            return this.punteggioPlayer1 + ", " + this.punteggioPlayer2;
    }

    void puntoPer(String pluto) {
        //assegna un punto al partecipante 
        if(pluto.equals(player1))
        {
            this.punteggioPlayer1 = this.trasforma(player1);
        }
        if(pluto.equals(player2))
        {
            this.punteggioPlayer2 = this.trasforma(player2);
        }
    }
    private String trasforma(String pluto){
        //assegna al punteggio uno step in più
        if(pluto.equals(player1)){
            if(this.punteggioPlayer1.equals("zero"))
            {
                this.punteggioPlayer1 = "quindici";
            }else if(this.punteggioPlayer1.equals("quindici")){
                this.punteggioPlayer1 = "trenta";
            }else if(this.punteggioPlayer1.equals("trenta"))
            {
                this.punteggioPlayer1 = "quaranta";
            }
            else if(this.punteggioPlayer1.equals("quaranta")){
                this.punteggioPlayer2 = "vantaggio "+player1;
                //Aumenta il vantaggio di Player1
                cPippo++;
            }
            else if(this.punteggioPlayer2.equals("vantaggio "+player2)){
                this.punteggioPlayer1 = "quaranta";
                //Resetta il vantaggio dell'avversario.
                cPippo = 0;
            }
            if (this.punteggioPlayer1.equals("quaranta") && cPippo == 2){
                //Vittoria di Player1
                punteggioPlayer1= player1+" vince";
            }
            return punteggioPlayer1;
        }
        if(pluto.equals(player2)){
            if(this.punteggioPlayer2.equals("zero"))
            {
                this.punteggioPlayer2 = "quindici";
            }else if(this.punteggioPlayer2.equals("quindici")){
                this.punteggioPlayer2 = "trenta";
            }else if(this.punteggioPlayer2.equals("trenta"))
            {
                this.punteggioPlayer2 = "quaranta";
            }
            else if(this.punteggioPlayer2.equals("quaranta")){
                this.punteggioPlayer2 = "vantaggio "+player2;
                //Aumenta il vantaggio di Player2
                cPluto++;
            }
            else if(this.punteggioPlayer2.equals("vantaggio "+player1)){
                this.punteggioPlayer2 = "quaranta";
                //Resetta il vantaggio dell'avversario.
                cPluto = 0;
            }
            if (this.punteggioPlayer1.equals("quaranta") && cPluto == 2){
                //Vittoria di Player2
                punteggioPlayer2= player2+" vince";
            }  
            return punteggioPlayer2;
        }
        
    return null;        
    }
}
