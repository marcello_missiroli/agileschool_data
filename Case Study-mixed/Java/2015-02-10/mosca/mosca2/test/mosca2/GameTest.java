/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mosca2;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author s.gualtieri
 */
public class GameTest {
    private Game game;
    
    public GameTest() {
        Game game;
    }
    
    @Before
    public void perTuttiITest() {
    game = new Game("Pippo", "Pluto");}
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testSomeMethod() {
        // TODO review the generated test code and remove the default call to fail.
        
    }
    @Test
    public void zeroDeveEssereLaDescrzionePerIlPunteggio0()
    {
        assertEquals(game.getPunteggio(), "zero, zero");
    }

    @Test
    public void quindiciDeveEssereIlDescrittorePerIlPunteggio1()
    {
        game.puntoPer("Pluto");
        assertEquals(game.getPunteggio(), "zero, quindici");
    }
    
    @Test
    public void trentaDeveEssereIlDescrittorePerIlPunteggio2() 
    {
        game.puntoPer("Pippo");
        game.puntoPer("Pippo");
        game.puntoPer("Pluto");
        assertEquals(game.getPunteggio(), "trenta, quindici");
    }
}
