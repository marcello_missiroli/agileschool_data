/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author a.ba
 */

package mosca1bis;

public class Game {
private String pippo;
private String pluto;
private int punti_pippo=0;
private int punti_pluto=0;
    Game(String pippo, String pluto) {
        this.pippo=pippo;
        this.pluto=pluto;
    }

    Object getPunteggio() {
        if(punti_pippo==0 && punti_pluto==0)
            return "zero, zero";
        else if(punti_pippo==0 && punti_pluto==15)
            return "zero, quindici";
        else if(punti_pippo==30 && punti_pluto==15)
            return "trenta, quindici";
        else if(punti_pippo==40 && punti_pluto==0)
            return "quaranta, zero";
        else
            return "";
    }

    void puntoPer(String nome) {
        if(nome=="Pluto")
            punti_pluto+=15;
        else
            punti_pippo+=15;    
    }
    
}
