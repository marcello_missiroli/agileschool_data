/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package javagame;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author s.legname
 */
public class GameTest 
{
    private Game game;
    
    public GameTest() 
    {
        Game game;
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testSomeMethod() {
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of perTuttiITest method, of class Game.
     */
    
    
    

        @Before
        public void perTuttiITest() 
        {
            game = new Game("Pippo", "Pluto");
        }

    
    
    
    @Test
    public void zeroDeveEssereLaDescrzionePerIlPunteggio0() 
    {
        
        
        assertEquals(game.getPunteggio(), "zero, zero");
    }

    
    @Test
    public void quindiciDeveEssereIlDescrittorePerIlPunteggio1() 
    {
        
        game.puntoPer("Pluto");
        assertEquals(game.getPunteggio(), "zero, quindici");
    }
    
    @Test
    public void trentaDeveEssereIlDescrittorePerIlPunteggio2() 
    {
        
        game.puntoPer("Pippo");
        game.puntoPer("Pippo");
        game.puntoPer("Pluto");
        assertEquals(game.getPunteggio(), "trenta, quindici");
    }
    
    @Test
    public void quarantaSDeveEssereIlDescrittorePerIlPunteggio3() {
            
        game.puntoPer("Pippo");game.puntoPer("Pippo");game.puntoPer("Pippo");
        assertEquals(game.getPunteggio(), "quaranta, zero");
    }
    @Test
    public void vantaggioDeveEssereIlDescrittorePerIlPunteggioQuandoEntrmbiHannoFatto3PuntiEUnGiocatoreHaUnPuntoDiVantaggio() {
                   
        game.puntoPer("Pippo");game.puntoPer("Pippo");game.puntoPer("Pippo");
        game.puntoPer("Pluto");game.puntoPer("Pluto");game.puntoPer("Pluto");game.puntoPer("Pluto");
        assertEquals(game.getPunteggio(), "vantaggio Pluto");
    }
    
//    @Test
//    public void paritàDeveEssereIlDescrittorePerIlPunteggioQuandoEntrmbiHannoFatto3PuntiEIPunteggiSonoUguali() {
//        game = new Game("parità");
//        game.puntoPer("Pippo");
//        game.puntoPer("Pippo");
//        game.puntoPer("Pippo");
//        game.puntoPer("Pluto");
//        game.puntoPer("Pluto");
//        game.puntoPer("Pluto");
//        assertEquals(game.getPunteggio(), "parità");
//        game.puntoPer("Pippo");
//        assertFalse(game.getPunteggio().equals("parità"));
//        game.puntoPer("Pluto");
//        assertEquals(game.getPunteggio(), "parità");
//    }



    



   




    
}

