/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Me
 */
public class Rettangolo {
    private int base;
    private int altezza;
    
    public Rettangolo(int base, int altezza){
        this.altezza=altezza;
        this.base=base;
    }
    public Rettangolo(){
        this.base=2;
        this.altezza=1;
    }

    public int getBase() {
        return base;
    }

    public int getAltezza() {
        return altezza;
    }

    public void setBase(int base) {
        this.base = base;
    }

    public void setAltezza(int altezza) {
        this.altezza = altezza;
    }
    
    public int perimetro(){
        return this.altezza+this.altezza+this.base+this.base;
    }
    
    public int area(){
        return this.base*this.altezza;
    }
    
    @Override
    public String toString(){
        if(this.altezza==this.base){
            return "è un quadrato!";
        }
        else{
            return "non è un quadrato!";
        }
    }
      
}
