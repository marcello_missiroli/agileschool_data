/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author m.bruschi
 */
import java.util.Random;

public class Dado {
    private int face;
    public Dado(int face)
    {
        SetFace(face);
    }
    public Dado()
    {
        face = 6;
    }
    public Dado(String Nome)
    {
        Random m = new Random();
        face = m.nextInt();
    }
    public int GetFace(){return face;}
    private int SetFace(int p) {return face = p;}
    
    public int LanciaDado()
    {
        int e;
        Random c = new Random();
        e = c.nextInt(face)+1; //+1 poichè il random parte da 0.
        return e;
    }
}
