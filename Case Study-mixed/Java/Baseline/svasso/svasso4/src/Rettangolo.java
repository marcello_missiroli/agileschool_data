/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package progettorettangolo;

/**
 *
 * @author Massi
 */
public class Rettangolo {
    private double base;
    private double altezza;
    private double x;
    private double y;
    
    
public Rettangolo(){
    this.base=5;
    this.altezza=9;
}

public Rettangolo(double base,double altezza){
    this.base=base;
    this.altezza=altezza;
}

    public double getBase() {
        return base;
    }

    public void setBase(double base) {
        this.base = base;
    }

    public double getAltezza() {
        return altezza;
    }

    public void setAltezza(double altezza) {
        this.altezza = altezza;
    }

    public double Perimetro(){
        return (double)(this.base+this.altezza)*2;
        
    }
    
    public double Area(){
        return (double)this.altezza*this.base;
    }
    
    @Override
    public String toString(){
        if(this.base==this.altezza){
            return (" e' un quadrato");}
        else{
            return(" non e' un quadrato");
        }
        
    }
   
   
}
