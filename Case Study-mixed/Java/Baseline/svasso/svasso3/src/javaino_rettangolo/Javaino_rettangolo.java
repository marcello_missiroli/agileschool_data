/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* IL COMPITO DI QUESTO PROGRAMMA è QUELLO DI FARE INSRIRE DA TASTIERA DUE VALORI: RISPETTIVAMENTE BASE E ALTEZZA, IL PROGRAMMA IN SEGUITO DOVRA ANDARE A 
CALCOLARE IL PERIMETRO E LA AREA E STAMPARE IL RISULTATO SU SCHERMO, SUCCESSIVAMENTE DIRE ALL'UTENTE SE è UN RETTANGOLO O UN QUADRATO.
PROGRAMMA REALIZZATO SA SEBASTIAN SENDEROWSKI! 4A INFO  15/11/2014
*/
package javaino_rettangolo;
import java.util.*;

/**
 *
 * @author sebastian senderowski
 */
public class Javaino_rettangolo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) 
    {
        int base,altezza; // dichiaro le variabili
         
        Scanner in = new Scanner(System.in);
     
        
        
        System.out.println("inserisci il valore della base!\n"); // l'utente inserisce il valore della base
        base=in.nextInt();
        
        
        System.out.println("inserisci il valore della altezza!\n"); // l'utente inserisce il valore dell' altezza
        altezza=in.nextInt();
        
        rettangolo primo=new rettangolo(base,altezza); // instanzio l'oggetto denominato <<primo>>
        
        System.out.println("il perimetro è:"+primo.perimetro());  // richiamo del metodo perimetro() in un output di stampa
        
        System.out.println("L'area del rettangolo è:"+primo.area());  // richiamo del metodo area() in un output di stampa
        
        
        primo.rettangoloquadrato();   // richiamo del metodo rettangoloquadrato() senza un system.out.println perche l'output è gia presente nel metodo.
        // TODO code application logic here
    }
    
}
