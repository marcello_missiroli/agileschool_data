/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaino_rettangolo;

/* IL COMPITO DI QUESTO PROGRAMMA è QUELLO DI FARE INSRIRE DA TASTIERA DUE VALORI: RISPETTIVAMENTE BASE E ALTEZZA, IL PROGRAMMA IN SEGUITO DOVRA ANDARE A 
CALCOLARE IL PERIMETRO E LA AREA E STAMPARE IL RISULTATO SU SCHERMO, SUCCESSIVAMENTE DIRE ALL'UTENTE SE è UN RETTANGOLO O UN QUADRATO.
PROGRAMMA REALIZZATO SA SEBASTIAN SENDEROWSKI! 4A INFO  15/11/2014
*/

/**
 *
 * @author sebastian
 */
public class rettangolo 
{
    private int base;    // variabili private della classe
    private int altezza;  // variabili private della classe

   
    
    public rettangolo(int base, int altezza)   // costruttore rettangolo con passati per parametri base e altezza.
    {
        this.base = base;
        this.altezza = altezza;
    }

    

    

    public int getBase()    // get base
    {
        return base;
    }

    public int getAltezza()   // get altezza
    {
        return altezza;
    }

    public void setAltezza(int altezza)   // set altezza
    {
        this.altezza = altezza;
    }

    public void setBase(int base)    // set base 
    {
        this.base = base;
    }
    
    
    public int perimetro()     // metodo perimetro
    {
         
        return (base+altezza)*2;    // return con calcolo del perimetro
       
    }
    
    public int area()     // metodo area 
    {
       int area;    // dichiarazione variabile area locale al metodo
       area=base*altezza;       // calcolo area
       return  area;     
    }
    
    public int rettangoloquadrato()   // metodo confronto : controllo se la figura geometrica è un rettangolo oppure un quadrato
    {
        int vero=0;  // creo una variabile locale di tipo intero
        if (base!=altezza){    // controllo i lati con if
        System.out.println("è un rettangolo NON UN QUADRATO!");
            vero=0;}     // se i due valori sono diversi assegno alla variabile il valore 0  quindi è un rettangolo
        
        
        else    // oppure
        System.out.println("è un quadrato NON UN RETTANGOLO!");  // output su schermo 
        vero=1;     // assegno alla varabile il valore 1 quindi i due valori sono uguale e si tratta evidentemente di un quadrato
        return vero;   // valore di ritorno
    }
}
