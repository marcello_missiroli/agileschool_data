/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author a.nava
 */
public class Persona {
    
    private int eta;
    private String nome;
    private String sesso;
    private String professione;
    
    public Persona(int eta, String nome, String sesso, String professione) {
    
        setEta(eta);
        setNome(nome);
        setSesso(sesso);
        setProfessione(professione);
        
    }
    public int getEta()
    {
        return eta;
    }
    public void setEta(int e)
    {
        eta=e;
    }
    public String getNome()
    {
        return nome;
    }
    public void setNome(String n)
    {
        nome=n;
    }
    public String getSesso()
    {
        return sesso;
    }
    public void setSesso(String s)
    {
        sesso=s;
    }
    public String getProfessione()
    {
        return professione;
    }
    public void setProfessione(String p )
    {
        professione=p;
    }
    public void Chisei()
    {
        System.out.println("Sono una persona di nome: " + nome + "," + "Sesso:" + sesso + "," + "Eta': " + eta + "," + "Professione:" + professione);
    }
}
