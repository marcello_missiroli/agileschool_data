/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classe_rettangolo;

/**
 *
 * @author Sam
 */
public class Rettangolo 
{
    private int l1;
    private int l2;
    
    public Rettangolo()
    {
        this.l1 = 4;
        this.l2 = 6;
    }
    
    public Rettangolo(int l1, int l2)
    {
        this.l1 = l1;
        this.l2 = l2;
    }

    public int getL1() 
    {
        return l1;
    }

    public void setL1(int l1) 
    {
        this.l1 = l1;
    }

    public int getL2() 
    {
        return l2;
    }

    public void setL2(int l2) 
    {
        this.l2 = l2;
    }
    
    public int Perimetro ()
    {
        return ((l1*2)+(l2*2));
    }
    
    public int Area()
    {
        return (l1*l2);
    }
    
    public String Controllo()
    {
        if (l1==l2)
        {
            return "E' un quadrato (l1 = "+this.getL1()+", l2 = "+this.getL2()+")";
        }
        else
        {
            return "E' un rettangolo (l1 = "+this.getL1()+", l2 = "+this.getL2()+")";
        }
    }

}
