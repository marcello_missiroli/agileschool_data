/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classe_rettangolo;
import java.util.*;
/**
 *
 * @author Sam
 */
public class Classe_Rettangolo 
{

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) 
    {
        Scanner in = new Scanner(System.in);
        int b;
        int a;
        System.out.println("RETTANGOLO 1\n\n");
        System.out.println("Inserisci la base del triangolo");
        b = in.nextInt();
        System.out.println("Inserisci l'altezza del triangolo");
        a = in.nextInt();
        Rettangolo r1 = new Rettangolo(b, a);
        String s = r1.Controllo();
        System.out.println(s);
        int perimetro = r1.Perimetro();
        System.out.println("Il perimetro vale : "+perimetro);
        int area = r1.Area();
        System.out.println("L'area vale : "+area);
        // TODO code application logic here
    }
    
}
