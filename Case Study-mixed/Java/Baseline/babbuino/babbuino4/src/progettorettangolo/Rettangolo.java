/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package progettorettangolo;

/**
 *
 * @author Emanuele Domingo
 */
public class Rettangolo {
    private int b;
    private int a;
    
    public Rettangolo()
    {
        this.a = 0;
        this.b = 0;
    }
    
    public Rettangolo(int a,int b)
    {
        this.b = a;
        this.a = b;
    }
    public double getBase() 
    {
        return b;
    }

    public void setBase(int a) 
    {
        this.b = a;
    }

    public double getAltezza() 
    {
        return a;
    }

    public void setAltezza(int a) 
    {
        this.a = a;
    }
    
    public int Perimetro()
    {
        return (this.b*2)+(this.a*2);
    }
    
    public int Area()
    {
        return (this.b*this.a);
    }
    
    public boolean RicRettangolo()
    {
        if(this.b == this.a) return false;
        else return true;
    }
}
