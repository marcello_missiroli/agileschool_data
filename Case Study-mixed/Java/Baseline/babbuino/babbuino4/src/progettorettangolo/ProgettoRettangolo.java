/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package progettorettangolo;
import java.util.Scanner;
/**
 *
 * @author Emanuele Domingo
 */
public class ProgettoRettangolo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int x,y;
        System.out.print("Inserisci l'altezza: ");
        x = in.nextInt();
        System.out.println("");
        System.out.print("Inserisci la base: ");
        y = in.nextInt();
        System.out.println("");
        Rettangolo rett = new Rettangolo(x,y);
        System.out.println("Il perimetro è: "+rett.Perimetro());
        System.out.println("");
        System.out.println("L'area è: "+rett.Area());
        System.out.println("");
        if(rett.RicRettangolo() == true)
        {
                System.out.println("E' un rettangolo");
        }
        else
        {
            System.out.println("E' un quadrato");
        }
    } 
}
