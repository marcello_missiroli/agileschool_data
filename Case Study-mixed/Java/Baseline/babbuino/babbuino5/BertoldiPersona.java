/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ciaomondo;

/**
 *
 * @author l.bertoldi
 */
public class Persona
{
    private int eta;
    private String nome;
    private String sesso;
    private String professione;
    public Persona (int eta,String nome,String sesso,String professione){
    setEta (eta);
    setNome (nome);
    setSesso (sesso);
    setProfessione (professione);
    }
    public int getEta () {return eta;}
    private void setEta (int e) {eta=e;}
    public String getNome () {return nome;}
    private void setNome (String n) {nome=n;}
    public String getSesso () {return sesso;}
    private void setSesso (String s) {sesso=s;}
    public String getProfessione() {return professione;}
    private void setProfessione(String p) {professione=p;}
    public String getChisei(){return "Mi chiamo "+nome+", ho "+eta+" anni, sono un "+sesso+" e faccio parte degli "+professione;}
}

