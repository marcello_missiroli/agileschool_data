/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esercizio_rettangolo;

import java.util.Scanner;

/**
 *
 * @author Alison
 */
public class Esercizio_Rettangolo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner in= new Scanner(System.in);
        int b,h;
        System.out.println("Inserisci l' altezza del rettangolo");
        h=in.nextInt();
        System.out.println("Inserisci la base del rettangolo");
        b=in.nextInt();
        Rettangolo r=new Rettangolo(b,h);//creazione oggetto
        System.out.println("Il perimetro e':"+ r.perimetro());
        System.out.println("L' area e':"+ r.area());
        if(r.verifica())//Se il metodo booleano "verifica" ritorna "true" verrà stampato che e' un quadrato
        System.out.println("E' un quadrato poiche altezza ("+r.getAltezza()+") e base ("+r.getBase()+") sono uguali");
        System.out.println("Modifica altezza: ");
        h=in.nextInt();
        r.setAltezza(h);// modifico l' altezza precedente dell' oggetto con quella inserita adesso
        System.out.println("Modifica base");
        b=in.nextInt();
        r.setBase(b);
        System.out.println("Il perimetro e':"+ r.perimetro());
        System.out.println("L' area e':"+ r.area());
        if(r.verifica())
        System.out.println("E' un quadrato poiche altezza ("+r.getAltezza()+") e base ("+r.getBase()+") sono uguali");
        
    }
    
}
