/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esercizio_rettangolo;

/**
 *
 * @author Alison
 */
public class Rettangolo {
    private int base;
    private int altezza;
    
    public Rettangolo()//Costruttore di default
    {
        this.base=0;
        this.altezza=0;
        
    }
    public Rettangolo ( int base ,int altezza)//Cotruttore rettangolo con due parametri
    {
      this.altezza=altezza;
      this.base=base;
    }
    public int getAltezza()//Metodo per ottendere l' altezza
    {
        return this.altezza;
    }
    public int getBase()// metodo per ottenere la base
    {
        return this.base;
    }
    public void setAltezza(int altezza)//Metodo per modificare l' altezza
    {
        this.altezza=altezza;
    }
    public void setBase(int base)// metodo  per modificare la base
    {
        this.base=base;
    }
    public int perimetro()//Metodo per calcolare il perimetro
    {
        return ((this.base*2)+(this.altezza*2));
    }
    public int area()//Metodo per calcolare l' area
    {
        return this.base*this.altezza;
    }
    public boolean verifica()// metodo booleano per verificare se e' un quadrato o meno
    {
        if(base==altezza)
            return true;
        else
            return false;
    }
}

