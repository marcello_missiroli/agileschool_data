/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package programma_rettangolo;
import java.util.Scanner;
/**
 *
 * @author SONY
 */
public class Programma_rettangolo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        Scanner in = new Scanner(System.in);
        
        int base, altezza;
        System.out.println("Inserisci la lunghezza della base:");
        base = in.nextInt();
        System.out.println("Inserisci la lunghezza dell'altezza:");
        altezza = in.nextInt();
        
        rettangolo r=new rettangolo(base,altezza);
        System.out.println("Il perimetro del rettangolo e':"+r.perimetro());
        System.out.println("L'area del rettangolo e':"+r.area());
        System.out.println(""+r.toString());
    }
    
}
