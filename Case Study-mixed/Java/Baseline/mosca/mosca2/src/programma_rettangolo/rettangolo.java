/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package programma_rettangolo;

/**
 *
 * @author SONY
 */
public class rettangolo {
    private int base;
    private int altezza;
    
    public rettangolo( int base, int altezza)   //COSTRUTTORE CON DUE PARAMETRI
    {
        this.base=base;
        this.altezza=altezza;
    }
    
    public rettangolo()     //COSTRUTTORE SENZA PARAMETRI
    {
        this.base=0;
        this.altezza=0;
    }
    
    public int perimetro()  //METODO CHE RESTITUISCE IL PERIMETRO
    {
        int p=((base*2)+(altezza*2));
        return p;
    }
    
    public int area()    //METODO CHE RESTITUISCE L'AREA
    {
        int a=(base*altezza);
        return a;
    }
    
    public String toString()   //METODO CHE MI DICE SE E' QUADRATO O NO 
    {
        String s;
        if(this.base==this.altezza)
        {
            s = ("E' un quadrato");
        }
        else
        {
            s = ("Non e' un quadrato");
        }
        return s;
    }
}
