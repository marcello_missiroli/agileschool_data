package rettangolo_murlano;
import java.util.Scanner;
public class Rettangolo_Murlano 
{
    public static void main(String[] args) 
    {
        Scanner in = new Scanner (System.in); /*Inizializzo lo scanner nella variabile in*/
        ClasseRettangolo r1; /*Definisco un oggetto di tipo ClasseRettangolo chiamato r1*/
        System.out.print("Benvenuto\n1)Crea una figura di default - 2)Crea una figura manualmente\nScelta: ");
        int scelta = in.nextInt(); /*Definisco una variabile che contiene la decisione dell'utente*/
        switch(scelta) /*Utilizzo lo switch per eseguire ciò che l'utente desidera*/
        {
            case 1:
                r1 = new ClasseRettangolo(); /*Istanzio un nuovo oggetto r1 con il costruttore default*/
                System.out.println("------------------------------------------------------");
                System.out.println("Altezza: " + r1.getAltezza());
                System.out.println("Base: " + r1.getBase());
                System.out.println(r1.toString());
                System.out.println("Il suo perimetro è " + r1.perimetro());
                System.out.println("La sua area è " + r1.area());
                System.out.println("------------------------------------------------------");
                break;
            case 2:
                System.out.print("Inserisci l'altezza: ");
                int altezza = in.nextInt();
                System.out.print("Inserisci la base: ");
                int base = in.nextInt();
                r1 = new ClasseRettangolo(altezza, base); /*Istanzio un nuovo oggetto r1 con il costruttore che utilizza Altezza e Base, appena letti*/
                System.out.println("------------------------------------------------------");
                System.out.println("Altezza: " + r1.getAltezza());
                System.out.println("Base: " + r1.getBase());
                System.out.println(r1.toString());
                System.out.println("Il suo perimetro è " + r1.perimetro());
                System.out.println("La sua area è " + r1.area());
                System.out.println("------------------------------------------------------");
                break;
            default:
                System.out.println("La scelta inserita non è valida!");
        }
        /*RICHIAMO I METODI SETTER (MODIFICATORI) ALL'INTERNO DI UN COMMENTO PERCHè NON VI è MOTIVO INSERIRLI NEL MAIN PER FARLI ESEGUIRE   
        System.out.print("Inserisci la nuova altezza: ");
        int altezza = in.nextInt();
        System.out.print("Inserisci la nuova base: ");
        int base = in.nextInt();
        r1 = new ClasseRettangolo();
        r1.setAltezza(altezza);
        r1.setBase(base);
        System.out.println("------------------------------------------------------");
        System.out.println("Altezza: " + r1.getAltezza());
        System.out.println("Base: " + r1.getBase());
        System.out.println(r1.toString());
        System.out.println("Il suo perimetro è " + r1.perimetro());
        System.out.println("La sua area è " + r1.area());
        System.out.println("------------------------------------------------------");*/
    }
}
