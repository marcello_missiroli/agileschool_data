package rettangolo_murlano;
public class ClasseRettangolo {
    /*Attributi privati della classe*/
    private int altezza;
    private int base;
    /*Costruttori*/
    public ClasseRettangolo() /*Costruttore default che assegna base e altezza = a zero*/
    {
        this.altezza = 0;
        this.base = 0;
    }
    public ClasseRettangolo(int altezza, int base) /*Costruttore con due parametri, Altezza e Base*/
    {
        this.altezza = altezza;
        this.base = base;
    }
    /*Metodi Setter*/
    public void setAltezza(int altezza) /*Setter che modifica l'altezza del rettangolo da cui è invocato*/
    {
        this.altezza = altezza;
    }
    public void setBase(int base)
    {
        this.base = base;
    }
    /*Funzione Getter*/
    public int getAltezza() /*Getter che restituisce il valore di Altezza del rettangolo da cui è invocato*/
    {
        return this.altezza;
    }
    public int getBase() /*Getter che restituisce il valore di Base del rettangolo da cui è invocato*/
    {
        return this.base;
    }
    /*Funzione che restituisce il perimetro del rettangolo*/
    public int perimetro()
    {
        return ( this.altezza * 2 ) + ( this.base * 2);
    }
    /*Funzione che mi restituisce l'area del rettangolo*/
    public int area()
    {
        return ( this.altezza * this.base);
    }
    /*Metodo che restituisce se il rettangolo è un quadrato oppure non lo è*/
    @Override
    public String toString()
    {
        if (this.altezza == 0 && this.base == 0)
            return "Questa figura non è nè un rettangolo nè un quadrato!";
        else if( this.altezza == this.base)
            return "Questa figura geometrica è un quadrato!";
        else
            return "Questa figura geometrica è un rettangolo!";
    }
}
