/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author m.casu
 */
public class ClassPersona {
    private int Eta;
    private String Nome;
    private String Sesso;
    public ClassPersona(int Eta,String Nome,String Sesso)
    {
       setEta(Eta);
       setNome(Nome);
       setSesso(Sesso);
       Eta=0;
       Nome="";
       Sesso="";
    }
     protected void setEta(int e)
        {
            Eta=e;
        }
     public int getEta()
     {
         return Eta;
     }
     protected void setNome(String n)
     {
         Nome=n;
     }
     public String getNome()
     {
         return Nome;
     }
     protected void setSesso(String s)
     {
         Sesso=s;
     }
     private String getSesso()
     {
         return Sesso;
     }
     public String ChiSei()
     {
         String Sei;
         Sei="mi chiamo" + Nome + "sono"+ Sesso + "e ho" + Eta + "anni";
         return Sei;
     }
     
}
