/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mainrettangolo;

/**
 *
 * @author Giancarlo
 */
public class rettangolo {
    private int base;
    private int altezza;
    
    public rettangolo(){
        this.base=0;
        this.altezza=0;
    }
    public rettangolo(int b,int a){
        this.base=b;
        this.altezza=a;
    }

    public void setBase(int base) {
        this.base = base;
    }

    public void setAltezza(int altezza) {
        this.altezza = altezza;
    }

    public void modBase(int b){
        this.base=b;
    }

    public void modAltezza(int a){
        this.altezza=a;
    }
    public double perimetro(){
        return (base*2+altezza*2);
    }
    public double area(){
        return (base*altezza);
    }
    @Override
    public String toString(){
        if(base==altezza)
        return "e' un quadrato!";
        else
        return "e' un rettangolo";
    }
}
