/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mainrettangolo;

import java.util.Scanner;

/**
 *
 * @author Giancarlo
 */
public class MainRettangolo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner in =new Scanner(System.in);
        
        System.out.println("Inserisci la base:");
        int base=in.nextInt();
        System.out.println("Inserisci la altezza:");
        int altezza=in.nextInt();
        rettangolo rt1=new rettangolo(base,altezza);
        System.out.println(rt1.perimetro());
        System.out.println(rt1.area());
        System.out.println(rt1.toString());
    }
    
}
