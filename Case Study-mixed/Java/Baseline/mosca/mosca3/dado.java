/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author t.garuti
 */
import java.util.*;
public class dado {

    private int numeroFacce;
    
    /**
     *
     * @param numeroFacce
     */
    public dado(int numeroFacce){
         setNumeroFacce(numeroFacce);
         
    }
    public dado(){
        numeroFacce=6;
    }
    private int getNumeroFacce(){
        return numeroFacce;
    }
    private void setNumeroFacce(int numFacce) {
        numeroFacce=numFacce;
    }
    public int lancioDado() {
        int casual;
        Random caso=new Random();
        casual=caso.nextInt(numeroFacce)+1;
        if(casual==0)
            casual++;
        
        return casual;
        
        
    }
}
