/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main_rettangolo;

/**
 *
 * @author Vip
 */
public class Rettangolo {
    //CREO GLI ATTRIBUTI base E altezza DI TIPO INTERO
    private int base;
    private int altezza;
    //QUESTO E' UN COSTRUTTORE DI DEFAULT CHE NON RICHIEDE NESSUN PARAMETRO,PERO' I VALORI DI base E altezza SARANNO 0
    public Rettangolo()
    {
        this.base=0;
        this.altezza=0;
    }
    //QUESTO COSTRUTTORE INVECE RICHIEDE DUE PARAMETRI, IN QUESTO CASO SONO DI TIPO INTERO E DARANNO IL VALORE AGLI ATTRIBUTI
    public Rettangolo(int base, int altezza)
    {
        this.base=base;
        this.altezza=altezza;
    }
    //E' UN METODO SELETTORE CHE CHIEDE IL VALORE DELLA BASE
    public int getBase()
    {
        return this.base;
    }
    //E' UN METODO SELETTORE CHE CHIEDE IL VALORE DELL'ALTEZZA
    public int getAltezza()
    {
        return this.altezza;
    }
    //E' UN METODO MODIFICATORE CHE CAMBIA IL VALORE DELLA BASE, RICHIEDENDO UN VALORE IN PARAMETRO
    public void setBase(int base)
    {
        this.base=base;
    }
    //E' UN METODO MODIFICATORE CHE CAMBIA IL VALORE DELLA ALTEZZA, RICHIEDENDO UN VALORE IN PARAMETRO
    public void setAltezza(int altezza)
    {
        this.altezza=altezza;
    }
    //QUESTO E' UN METODO CHE RITORNA IL VALORE DEL PERIMETRO E NON RICHIEDE PARAMETRI 
     public int CalcolaPerimetro ()
    {
        return (getBase()*2)+(getAltezza()*2);
    }
    //QUESTO E' UN METODO CHE RITORNA IL VALORE DELL'AREA E NON RICHIEDE PARAMETRI
    public int CalcolaArea ()
    {
        return getBase()*getAltezza();
    }
    //QUESTO E' UN METODO CHE RITORNA IL VALORE true O false IN BASE ALLA CONGRUENZA DEI LATI E NON RICHIEDE PARAMETRI
    public boolean VerificaQuadrato()
    {
        if(this.altezza==this.base)
            return true;
        else return false;
    }
    //QUESTO E' IL METODO toString() CHE RITORNA LA FRASE CON I VALORI DELLA BASE E DELL'ALTEZZA CONTENENTI NELL'ISTANZA E NON RICHIEDE PARAMETRI
    public String toString()
    {
        return "\nBASE : "+getBase()+"\nALTEZZA : "+getAltezza()+"\n";
    }
}
