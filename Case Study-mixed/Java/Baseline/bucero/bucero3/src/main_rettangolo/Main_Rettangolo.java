/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main_rettangolo;
import java.util.Scanner;
/**
 *
 * @author Vip
 */
public class Main_Rettangolo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        //VIENE CREATO UN OGGETTO DI TIPO Scanner CHIAMATO input
        Scanner input=new Scanner(System.in);
        //INPUT DELLA base E DELL'altezza 
        System.out.println("INPUT BASE E ALTEZZA DEL RETTANGOLO");
        int base=input.nextInt();
        int altezza=input.nextInt();
        //QUI AVVIENE LA CREAZIONE DELL'OGGETTO rettangolo DI TIPO Rettangolo 
        Rettangolo rettangolo=new Rettangolo(base,altezza);
        //RICHIAMO IL METODO toString() E LA STAMPO
        System.out.println(rettangolo.toString());
        //RICHIAMO IL METODO CalcolaPerimetro() E LO STAMPO
        System.out.println("PERIMETRO : "+rettangolo.CalcolaPerimetro());
        //RICHIAMO IL METODO CalcolaArea() E LO STAMPO
        System.out.println("AREA : "+rettangolo.CalcolaArea());
        //RICHIAMO IL METODO VerificaQuadrato() NELL'IF CHIEDENDO SE QUESTO RETTANGOLO E' UN QUADRATO
        if(rettangolo.VerificaQuadrato())
            System.out.println("E' UN QUADRATO");
        else
            System.out.println("NON E' UN QUADRATO");
    }
    
}
