/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classerettangolo;
import java.util.Scanner;
/**
 *
 * @author dajeroma96
 */
public class ClasseRettangolo 
{

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) 
    {
        // TODO code application logic here
        Scanner in=new Scanner(System.in);
        System.out.println("Inserisci la base: ");
        int base=in.nextInt();
        System.out.println("Inserisci l'altezza: ");
        int altezza=in.nextInt();
        Rettangolo r = new Rettangolo(base, altezza);
        System.out.println("Il perimetro e': "+r.perimetro());
        System.out.println("L'area e': "+r.area());
        System.out.println(""+r.toString());
    }  
}
