/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classerettangolo;

/**
 *
 * @author dajeroma96
 */
public class Rettangolo 
{
    private int base;
    private int altezza;
    
    public Rettangolo()     //Costruttore senza parametri
    {
        this.base=0;
        this.altezza=0;
    }
    
    public Rettangolo(int base, int altezza)     //Costruttore con 2 parametri
    {
        this.base=base;
        this.altezza=altezza;
    }   

    public int getBase()          //Selettori    
    {
        return base;
    }
    
    public int getAltezza() 
    {
        return altezza;
    }

    public void setBase(int base)      //Modificatori
    {
        this.base = base;
    }

    public void setAltezza(int altezza) 
    {
        this.altezza = altezza;
    }

    public int perimetro()          //Calcolo del perimetro
    {
        int perimetro;
        perimetro = (base*2)+(altezza*2);
        return perimetro;
    }
    
    public int area()               //Calcolo dell'area
    {
        int area;
        area = base*altezza;
        return area;
    }
    
    public boolean equals()        //Metodo per vedere se e' un quadrato
    {
         if(base==altezza)
         {   
             return true;
         }else
         {
             return false;
         }
    }
   
    @Override
    public String toString()            //Metodo toString
    { 
         String msg = "";
         if 
             (equals() == true) msg = "E' un quadrato!";
         else 
             msg = "Non e' un quadrato!";
         return msg;
    }
}
