/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mainrettangolo;

/**
 *
 * @author EJ
 */
public class Rettangolo {
    private int base;
    private int altezza;
   
    public Rettangolo()
    {
        this.altezza=0;
        this.base=0;
    }
    
    public Rettangolo(int base, int altezza)
    {
        this.base=base;
        this.altezza=altezza;
    }

    public int getBase() {
        return base;
    }

    public void setBase(int base) {
        this.base = base;
    }

    public int getAltezza() {
        return altezza;
    }

    public void setAltezza(int altezza) {
        this.altezza = altezza;
    }
    
    public int Perimetro()   //Restituisce il perimetro della figura
    {
        int perimetro = (this.base*2)+(this.altezza*2);
        return perimetro;
    }
    
    public int Area() //Restituisce l'area della figura
    {
        return this.base*this.altezza;
    }
    
    public boolean equals() //Controlla se il rettangolo è un quadrato
    {
        if (base==altezza)
        return true;
        else
        return false;
    }
   
    @Override
     public String toString() { 
     String msg = "";
     if (equals() == true ) msg = "E' UN QUADRATO!";  //Restituisce il contenuto testuale della Classe;
     return msg;
   }

}
