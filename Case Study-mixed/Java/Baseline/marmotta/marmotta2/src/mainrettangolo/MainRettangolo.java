/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mainrettangolo;
import java.util.Scanner;
/**
 *
 * @author EJ
 */
public class MainRettangolo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner in=new Scanner(System.in);
        int base;
        System.out.println("Inserire la base: ");
        base=in.nextInt();
        int altezza;
        System.out.println("Inserire l'altezza: ");
        altezza=in.nextInt();
        Rettangolo R = new Rettangolo(base, altezza);
        System.out.println("Il Perimetro è: "+ R.Perimetro());
        System.out.println("L'area è: "+ R.Area());
        System.out.println(""+ R.toString());
    }
    
}
