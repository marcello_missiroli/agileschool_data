/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rettangolo;

/**
 *
 * @author Yuri Petrillo
 */
public class rett {
    private int base;
    private int altezza;
    private double x;
    private double y;
    
    
public rett(){
    this.base=0;
    this.altezza=0;
}

public rett(int base,int altezza){
    this.base=base;
    this.altezza=altezza;
}

    public double getBase() {
        return base;
    }

    public void setBase(int base) {
        this.base = base;
    }

    public double getAltezza() {
        return altezza;
    }

    public void setAltezza(int altezza) {
        this.altezza = altezza;
    }

    public double Perimetro(){
        return (this.base+this.altezza)*2;
        
    }
    
    public double Area(){
        return this.altezza*this.base;
    }
    
    @Override
    public String toString(){
        if(this.base==this.altezza){
            return (" E' un quadrato!!!\n");}
        else{
            return(" Non e' un quadrato!!!\n");
        }
        
    }
   
   
}

