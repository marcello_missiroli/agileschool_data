/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author l.venturi
 */
import java.util.Random;
public class Dadi {
    private  int facce;
    public  Dadi(int facce)
    {
       SetFacce(facce);
    }
    public  Dadi()
    {
         facce=6;
    }
    private int SetFacce(int f)
    {
        return facce=f;
    }
    public int GetFacce()
    {
        return facce;
    }
    public int LanciaDadi()
    {
        int d;
       Random c=new Random();
       d=c.nextInt(facce+1);
       return d;         
    }
    
}
