/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author g.guiduzzi
 */
public class Persona
{
        private int eta;
        private String nome;
        private String sesso;
        private String prof;
        
        //costruttore
        public Persona(int eta, String nome, String sesso, String prof)
        {
            setEta(eta);
            setNome(nome);
            setSesso(sesso);
            setProf(prof);
        }
        
        //get e set
        public int getEta()
        {
            return eta;
        }
        
        public String getNome()
        {
            return nome;       
        }
        
        public String getSesso()
        {
            return sesso;       
        }
        
        public String getProf()
        {
            return prof;       
        }
        
        private void setEta(int age)
        {
            eta=age;
        }
        
        private void setSesso(String SeeS)
        {
            sesso=SeeS;
        }
        
        private void setNome(String NooN)
        {
            nome=NooN;
        }
        
        private void setProf(String plof)
        {
            prof=plof;
        }
        
        public String chiSei(Persona SuuS)
        {
            String s=("Sono una persona di nome "+SuuS.getNome()+", ho "+SuuS.getEta()+" anni, sono "+SuuS.getSesso()+" e la mia professione e' "+SuuS.getProf()+".");
            return s;
        }
}
