/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author s.galavotti
 */
public class persona {
    private int eta;
    private String nome;
    private String sesso;
    private String professione;
    
    public persona(int eta, String nome, String sesso, String professione)
    {
       seteta(eta); 
       setnome(nome);
       setsesso(sesso);
       setprofessione(professione);
    }
    
    public void chisei() {System.out.println("mi chiamo " + getnome()+", sono un " + getsesso()+" ho "+ geteta()+" anni e sono uno "+getprofessione() );}
    public int geteta() {return eta;}
    private void seteta(int e) {eta=e;}
    public String getnome() {return nome;}
    private void setnome(String n) {nome=n;}
    public String getsesso() {return sesso;}
    private void setsesso(String s) {sesso=s;}
    public String getprofessione() {return professione;}
    private void setprofessione(String p) {professione=p;}
    
}
