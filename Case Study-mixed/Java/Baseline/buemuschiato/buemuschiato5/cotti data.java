
/**
 *
 * @author i.cotti
 */
public class data {
    private int giorni;
    private int mesi;
    private int anni;
    data(int g,int m,int a) //costruttore 
    {
        setGiorni(g);
        setMesi(m);
        setAnni(a);
    }
    data(data d) //costruttore di copia
    {
        giorni=d.giorni;
        mesi=d.mesi;
        anni=d.anni;
    }
    // setters
    private void setGiorni(int gih) 
    {
        if( gih>0 && gih<=31)
            giorni=gih;
        else giorni=1;
    }
    private void setMesi(int mih)
    {
        if(mih>=1 && mih<=12)
            mesi=mih;
        else mesi=1;
    }
    private void setAnni(int Anih)
    {
        if(Anih>=0)
            anni=Anih;
        else anni=1900;
    }
    //ritorna una stringa in formato ggmmaaaa o mmggaaaa
    public String getGGMMAAAA()
    {
        String faf="";
        faf+=giorni+"/"+mesi+"/"+anni;
        return faf;
    }
    public String getMMGGAAAA()
    {
        String faf="";
        faf+=mesi+"/"+giorni+"/"+anni;
        return faf;
    }
    //ritorna il numero di giorni/mesi/anni dell'oggetto
    private int numeroGiorni()
    {
        int sas=giorni;
        if(mesi==11 || mesi==4 || mesi==6 || mesi==9)
            sas+=30;
        else if(mesi==2)
            sas+=28;
        else
            sas+=31;
        sas+=anni*365;
        return sas;
    }
    private int numeroMesi()
    {
        int sas=mesi;
        sas+=anni*12;
        return sas;
    }
    private int numeroAnni()
    {
        return anni;
    }
    //getters
    public int getAnni()
    {
        return anni;
    }
    public int getMesi()
    {
        return mesi;
    }
    public int getGiorni()
    {
        return giorni;
    }
    // differenza giorni/mesi/anni tra l'oggetto e un'altro o tre variabili rappresentanti giorni,mesi e anni
    public int diffGiorni(int g,int m,int a)
    {
        int fef=this.numeroGiorni();
        int sas=g;
        if(m==11 || m==4 || m==6 || m==9)
            sas+=30;
        else if(m==2)
            sas+=28;
        else
            sas+=31;
        sas+=a*365;
        return fef-sas;
    }
    public int diffGiorni(data joj)
    {
        return this.numeroGiorni()-joj.numeroGiorni();
    }
    public int diffMesi(int m,int a)
    {
        int fef=this.numeroMesi();
        int sas=mesi;
        sas+=anni*12;
        return fef-sas;
    }
    public int diffMesi(data joj)
    {
        return this.numeroMesi()-joj.numeroMesi();
    }
    public int diffAnni(int a)
    {
        return this.numeroAnni()-a;
    }
    public int diffAnni(data joj)
    {
        return this.numeroAnni()-joj.numeroAnni();
    }
    // ritorna vero se la data dell'oggetto è maggiore della data inserita
    public boolean successivo(int g,int m,int a)
    {
        int fef=this.numeroGiorni();
        int sas=g;
        if(m==11 || m==4 || m==6 || m==9)
            sas+=30;
        else if(m==2)
            sas+=28;
        else
            sas+=31;
        sas+=a*365;
        if(fef-sas>0)
            return true;
        else
            return false;
    }
    
    public boolean successivo(data joj)
    {
        if(this.numeroGiorni()-joj.numeroGiorni()>0)
            return true;
        else
            return false;
    }
    //ritorna vero se la data dell'oggetto e la data inserita sono uguali
    public boolean dataEquals(int g,int m,int a)
    {
        int fef=this.numeroGiorni();
        int sas=g;
        if(m==11 || m==4 || m==6 || m==9)
            sas+=30;
        else if(m==2)
            sas+=28;
        else
            sas+=31;
        sas+=a*365;
        if(fef==sas)
            return true;
        else
            return false;
    }
    public boolean susEquals(data joj)
    {
        if(this.numeroGiorni()==joj.numeroGiorni())
            return true;
        else
            return false;
    }
    
}
