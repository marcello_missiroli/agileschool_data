/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javino_rettangolo;

/**
 *
 * @author Andrea
 */
public class rettangolo {
    private int base;
    private int altezza;
    
    public rettangolo()     //costruttore senza parametri
    {
        this.base=0;
        this.altezza=0;
    }
    public rettangolo(int base,int altezza)
    {
        this.base=base;
        this.altezza=altezza;
    }

    public int getBase() {
        return base;
    }

    public void setBase(int base) {
        this.base = base;
    }

    public int getAltezza() {
        return altezza;
    }

    public void setAltezza(int altezza) {
        this.altezza = altezza;
    }
    
    public int perimetro()
    {
        return ((this.base*2)+(this.altezza*2));
    }
    public int area()
    {
        return this.base*this.altezza;
    }
    public String quadrato()
    {
        if (this.base==0 && this.altezza==0)
            return"Non qualificabile";
        else if (this.base==this.altezza)
             return"E' un quadrato";
             else
             return"E' un rettangolo";
    }
}
