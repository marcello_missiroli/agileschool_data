/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mainrettangolo;
import java.util.*;
/**
 *
 * @author Arman Arnautovic
 */
public class Rettangolo {
    // dichiarazioni attributi privati di tipo int per definire i lati del Rettangolo
    private int base;
    private int altezza;
    
     //METODI COSTRUTTORI 
    //THIS: fa riferimento agli attributi per evitare ambiguità
    
public Rettangolo (){
    //costruttore di default, senza parametri e con gli attributi assegnati a un valore
    //nullo
    this.base=0;
    this.altezza=0;
}   
public Rettangolo (int base, int altezza){
    //costruttore con due parametri che vengono assegnati rispettivamente alla base e all'altezza
    this.base=base;
    this.altezza=altezza;
}

 //METODI (FUNZIONI E PROCEDURE)
    
    //METODI GETTER E SETTER

    public int getBase() {
        //metodo getter ( o selettore ) che serve per visualizzare 
        //il valore degli attributi ( base ) 
        return base;
    }

    public void setBase(int base) {
        //metodo setter ( o modificatore ) che serve per modificare
        //il valore degli attributi ( base )
        this.base = base;
    }

    public int getAltezza() {
         //metodo getter ( o selettore ) che serve per visualizzare 
        //il valore degli attributi ( altezza )
        return altezza;
    }

    public void setAltezza(int altezza) {
        //metodo setter ( o modificatore ) che serve per modificare
        //il valore degli attributi ( altezza )
        this.altezza = altezza;
    }

    //METODO toString
    @Override
    public String toString (){
        // il metodo toString restituisce una variabile stringa che ci serve per la restituire 
        //in formato testuale il contenuto informativo dell'oggetto Rettangolo
        String S;
        if(base==altezza){
            //se l'altezza è di pari valore della base mi restituisce una variabile stringa 
            //che dovrebbe far capire all'utente che i dati del rettangolo sono equivalenti
            //a quelli di un quadrato generico
            S="\nBase: "+base+"\nAltezza: "+altezza+"\nE'un quadrato!";
            //S="E'un quadrato!";
        }
        else{
            S="\nBase: "+base+"\nAltezza: "+altezza;
        }
        return S;
    }
    
    //METODI ALGORITMICI
    
    public int Perimetro (){
        //metodo funzione che serve per calcolare il perimetro del rettangolo
        //usando i due attributi che lo caratterizzano, per poi farci i calcoli
        int p=((base*2)+(altezza*2));//assegno il valore del calcolo a una variabile int
        return p;//resituisce la variabile int
    }
    
    public int Area (){
        //metodo funzione che serve per calcolare l'area del rettangolo
        //usando i due attributi che lo caratterizzano, per poi farci i calcoli
        int a=(base*altezza); //assegno il valore del calcolo a una variabile int
        return a;//resituisce la variabile int
    }
}
