/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mainrettangolo;
import java.util.Scanner;
/**
 *
 * @author Arman Arnautovic
 */
public class MainRettangolo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner in=new Scanner(System.in);//riga di codice che permette l'input tramite la classe Scanner
        int base, altezza;//dichiaro due variabili di tipo int relative alla base e all'altezza
        Rettangolo Default = new Rettangolo();//intstanza di un oggetto relativo al costruttore di default
        //lo assegno a una varaibile di tipo Rettangolo (Default)
        System.out.println("\nQuesto è il triangolo Default  :"+Default.toString());
        //stampo il risultato richiamando il metodo toString
        System.out.println("\nInserisci Base: ");
        base=in.nextInt();
        //input della base che caratterizza l'instanza del secondo oggetto di tipo Rettangolo
        System.out.println("\nInserisci Altezza: ");
        altezza=in.nextInt();
         //input dell'area che caratterizza l'instanza del secondo oggetto di tipo Rettangolo
        Rettangolo BA = new Rettangolo(base,altezza);//intstanza di un oggetto relativo al costruttore con due parametri
        //lo assegno a una varaibile di tipo Rettangolo(BA)
        System.out.println(BA.toString());//stampo il risultato richiamando il metodo toString
        int Perimetro =BA.Perimetro();//richiamo il metodo che calcola il perimetro fra la base e l'altezza
        //assegnangdolo a una variabile di tipo int (Perimetro), svolgendo i calcoli sui dati relativi all'istanza BA
        System.out.println("Questo è il perimetro del rettangolo: "+Perimetro);
        int Area =BA.Area();//richiamo il metodo che calcola l'area fra la base e l'altezza
        //assegnangdolo a una variabile di tipo int (Area), svolgendo i calcoli sui dati relativi all'istanza BA
        System.out.println("Questa è l'area del rettangolo: "+Area);
    }
    
}
