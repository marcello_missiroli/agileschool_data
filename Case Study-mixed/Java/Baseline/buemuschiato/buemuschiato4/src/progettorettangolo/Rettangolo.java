/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package progettorettangolo;

/**
 *
 * @author Matteo
 */
public class Rettangolo {
    private int base;
    private int altezza;
    
    public Rettangolo()//costruttore senza parametri
    {
        this.base = 0;
        this.altezza = 0;
    }
    
    public Rettangolo(int base,int altezza)// costruttore con 2 parametri
    {
        this.base = base;
        this.altezza = altezza;
    }
    //GETTER e SETTER
    public int getBase() 
    {
        return base;
    }

    public void setBase(int base) 
    {
        this.base = base;
    }

    public int getAltezza() 
    {
        return altezza;
    }

    public void setAltezza(int altezza) 
    {
        this.altezza = altezza;
    }
    
    public int Perimetro()//metodo per calcolare il perimetro
    {
        int p;
        return p = (this.base * 2) + (this.altezza * 2);
    }
    
    public int Area()//metodo per calcolare l'area
    {
        int a;
        return a = (this.base * this.altezza);
    }
    
    public boolean VeroFalso()//metodo per verificare se la figura geometrica è un rettangolo o un quadrato
    {
        if(this.base == this.altezza)
            return true;
        else return false;
    }
}
