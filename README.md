DATA REPOSITORY
===============
for "Learning Agile Software Development in High School:an Investigation"
-------------------------------------------------------------------------


Data related to the experiment:

In folder "Experiment - quantitative" you will find the results related to the actual experiment along with Subject Selection data, the protocol used and the instruction materials.

In folder "Case Study" you will find the actual code produced during the experiment, along with the sonarcube analysis output.

In folder "Surverys" you will find the questions and the results of both pupil feedback and teacher survey.
