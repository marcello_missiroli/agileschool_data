PROTOCOLLO FOLDER
==================
Folder "Planning" contains the information given to teacher and pupils. In particular, Project outline, User stories, Presentations used for instruction and legal documents for school admininistration. 

Everything is in Italian.



SCORING CARD FOLDER
===================
This folder contains the performance evaluation of each pair of solo programmer.

Following is the codename-symbol association for groups:

PAIRS:
------

* Babbuino (GG)
* Buemuschiato (GA)
* Biscia (GP)
* Marmotta (AA)
* Mosca (AP)
* Svasso(PP)

SOLOS:
------
* Bucero (G)
* Muflone (A)
* Surmolotto (P)

If a +1 appears on the score, it indicates the extra point awarded by the class teacher. 


