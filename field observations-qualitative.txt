Second experiment field notes
Class MO4A16
The class seems more tense than other. One element is a special need person, recent immigrat with linguistic problems and possible autistic tendencies; normally has a support teacher but not today. Two students had already done the experience the year before (rejected), but state they don't remember anything from it. 
Class VG4C16
Due to absences, we had an uneven number of pupils. One was therefore paired to the teacher, who also had no previous knowledge of Agile. Their interaction showed that the teacher got the lead, often relegating the student to a secondary role. 
Class BO4D16
The lab was unavailable, so the pupils worked on their laptops with the inevitable last-minute problems. This probably influenced the overall results, which were lower that the average. 
Due to absences, we had an uneven number of pupils. One was therefore paired to the laboratory assistaning teacher, who also had no previous knowledge of Agile. Their interaction showed however cooperation on even terms. A girl was a special need student, that reacted excessively to stress for the entire duration of the experience. A second student was certified as an asperger-type, but he had apparently no problem in relating with his friends. 
One student was finalist in the Italian Competition in Informatics. His pair showed a strong tendency to overcode, and his companion seemed unable to express his point of view.
One group lost completely his job due to an HD failure. 

